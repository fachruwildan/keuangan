<?php
namespace app\components;

use yii\helpers\Url;
use app\models\HutangPiutang;
use app\models\Pengajuan;
use yii\db\Expression;
class Notif {
    // public static function create($url, $params, $to_user, $title, $description = "-"){
    //     $has_fcm_token = FcmToken::find()->where(['user_id' => $to_user])->select(['fcm_token'])->all();
    //     if($has_fcm_token){
    //         $prepare_token = [];
    //         foreach($has_fcm_token as $row) array_push($prepare_token, $row->fcm_token);
    //         SendFcm::exec($prepare_token, $title);
    //     }
    //     $notification = new notification();
    //     $notification->controller = $url;
    //     $notification->params = json_encode($params);
    //     $notification->to_user = $to_user;
    //     $notification->title = $title;
    //     $notification->description = $description;
    //     $notification->save();
    // }
    

    public static function notifList($listHtml =true){
        $notifList = [];
        $notifListPengajuan = [];
        $totalCount = 0;

        $user = \Yii::$app->user->identity;
        $belum_dilihat = 0;

        $notif = HutangPiutang::find()
        ->where(['>=', 'tanggal_tempo', new Expression('DATE_SUB(NOW(), INTERVAL 3 DAY)')])
        ->andWhere(['status'=>0])->orderBy(['id' => SORT_ASC])->limit(5)->all();
        $pengajuan = Pengajuan::find()
        ->where(['id_user' => $user->id])
        ->orWhere(['or',
        ['status'=>1],
        ['status'=>2],
        ['status'=>3]
    ])->orderBy(['id' => SORT_ASC])->limit(5)->all();
        $count = count($notif) + count($pengajuan);
        if($count > 0){
            $label = '<span class="label label-danger pull-right">'.$count.'</span>';
            foreach($notif as $n){
                array_push($notifList,
                    '<li>
                        <a href="'. Url::to(["/hutang-piutang/$n->id"]).'">
                            <i class="fa fa-users text-aqua"></i>'.$n->kelompok .' Perusahaan '.$n->perusahaan.'
                            <span class="label label-danger pull-right"></span>
                        </a>
                    </li>');
            }
            foreach($pengajuan as $a){
                array_push($notifListPengajuan,
                    '<li>
                        <a href="'. Url::to(["/pengajuan/$a->id"]).'">
                            <i class="fa fa-envelope text-aqua"></i>'.' Pengajuan dengan Tujuan '.$a->tujuan.'
                            <span class="label label-danger pull-right"></span>
                        </a>
                    </li>');
            }
        }

        if($listHtml)
        echo '
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning">'.$count.'</span>
            </a>
            <ul class="dropdown-menu">
                <li class="header">Kamu Memiliki '.$count.' notifications</li>
                <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                        '.implode('',$notifList).'
                        '.implode('',$notifListPengajuan).'
                    </ul>
                </li>
            </ul>
        ';
        else echo $totalCount;
    }
    public static function notifListPengajuanDirektur($listHtml =true){
        $notifListPengajuan = [];
        $totalCount = 0;

        $user = \Yii::$app->user->identity;
        $belum_dilihat = 0;

        $pengajuan = Pengajuan::find()
        ->where(['status' =>2])    
        ->orderBy(['id' => SORT_ASC])->limit(5)->all();
        $count = count($pengajuan);
        if($count > 0){
            $label = '<span class="label label-danger pull-right">'.$count.'</span>';
            foreach($pengajuan as $a){
                array_push($notifListPengajuan,
                    '<li>
                        <a href="'. Url::to(["/pengajuan/$a->id"]).'">
                            <i class="fa fa-envelope text-aqua"></i>'.' Pengajuan dengan Tujuan '.$a->tujuan.'
                            <span class="label label-danger pull-right"></span>
                        </a>
                    </li>');
            }
        }

        if($listHtml)
        echo '
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning">'.$count.'</span>
            </a>
            <ul class="dropdown-menu">
                <li class="header">Kamu Memiliki '.$count.' notifications</li>
                <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                        '.implode('',$notifListPengajuan).'
                    </ul>
                </li>
                <li class="footer"><a href="'. Url::to(["/pengajuan"]).'">View all</a></li>
            </ul>
        ';
        else echo $totalCount;
    }
    public static function notifListPengajuanDirekturUtama($listHtml =true){
        $notifListPengajuan = [];
        $totalCount = 0;

        $user = \Yii::$app->user->identity;
        $belum_dilihat = 0;

        $pengajuan = Pengajuan::find()
        ->where(['status' =>3])    
        ->orderBy(['id' => SORT_ASC])->limit(5)->all();
        $count = count($pengajuan);
        if($count > 0){
            $label = '<span class="label label-danger pull-right">'.$count.'</span>';
            foreach($pengajuan as $a){
                array_push($notifListPengajuan,
                    '<li>
                        <a href="'. Url::to(["/pengajuan/$a->id"]).'">
                            <i class="fa fa-envelope text-aqua"></i>'.' Pengajuan dengan Tujuan '.$a->tujuan.'
                            <span class="label label-danger pull-right"></span>
                        </a>
                    </li>');
            }
        }

        if($listHtml)
        echo '
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning">'.$count.'</span>
            </a>
            <ul class="dropdown-menu">
                <li class="header">Kamu Memiliki '.$count.' notifications</li>
                <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                        '.implode('',$notifListPengajuan).'
                    </ul>
                </li>
                <li class="footer"><a href="'. Url::to(["/pengajuan"]).'">View all</a></li>
            </ul>
        ';
        else echo $totalCount;
    }
}