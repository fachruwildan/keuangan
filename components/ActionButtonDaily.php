<?php
/**
 * Created by PhpStorm.
 * User: feb
 * Date: 30/05/16
 * Time: 00.14
 */

namespace app\components;

use yii\helpers\Html;

class ActionButtonDaily
{
    public static function getButtons()
    {
        $btn = [
            'view' => function ($url, $model, $key) {
                return Html::a("<i class='fa fa-eye'></i>", ["view", "id"=>$model->id], ["class"=>"btn btn-success", "title"=>"Lihat Data"]);
            },
            'update' => function ($url, $model, $key) {
                if($model->status == 0){
                    return Html::a("<i class='fa fa-pencil'></i>", ["update", "id"=>$model->id], ["class"=>"btn btn-warning", "title"=>"Edit Data"]);
                }
            },
            'delete' => function ($url, $model, $key) {
                if($model->status == 0){
                    return Html::a("<i class='fa fa-trash'></i>", ["delete", "id"=>$model->id], [
                        "class"=>"btn btn-danger",
                        "title"=>"Hapus Data",
                        "data-confirm" => "Apakah Anda yakin ingin menghapus data ini ?",
                        //"data-method" => "GET"
                    ]);
                }
               
            },
        ];
        $template = '{view} {update} {delete}';
        $width = "120px";


        return [
            'class' => 'yii\grid\ActionColumn',
            'template' => $template,
            'buttons' => $btn,
            'contentOptions' => ['nowrap'=>'nowrap', 'style'=>"text-align:center"]
        ];
    }
    
        public static function getStatus()
        {
            return [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{status} {note}',
                'buttons' => [
                    'note' => function ($url, $model, $key) {
                        if($model->note == null){
                            return Html::a("<i class='fa fa-pencil'></i>", ["note", "id"=>$model->id], [
                              "class"=>"btn btn-warning",
                              "title"=>"Note",
                              "data-confirm" => "Apakah Anda akan menambahkan catatan ?",
                          ]);
                          }else{
                            return Html::a("<i class='fa fa-pencil'></i>", ["update-note", "id"=>$model->id], [
                                "class"=>"btn btn-warning",
                                "title"=>"Note",
                                "data-confirm" => "Apakah Anda akan mengedit catatan ?",
                            ]); 
                          }
                    },
                    'update' => function ($url, $model, $key) {
                            return Html::a("<i class='fa fa-pencil'></i>", ["update", "id"=>$model->id], ["class"=>"btn btn-warning", "title"=>"Edit Data"]);
                        },
                    'status' => function ($url, $model, $key) {
                        if($model->status == 0){
                            return Html::a("<i class='fa fa-check'></i>", ["status", "id"=>$model->id], [
                                "class"=>"btn btn-success",
                                "title"=>"Confirm Daily Schedule",
                                "data-confirm" => "Apakah Anda yakin ingin mengerjakan ini ?",
                                //"data-method" => "GET"
                            ]);
                        }
                        
                    },
    
                ],
                'contentOptions' => ['nowrap'=>'nowrap', 'style'=>'text-align:center;width:140px']
            ];
        }
}