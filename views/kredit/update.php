<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\Kredit $model
*/

$this->title = 'Kredit ' . $model->id_kredit . ', ' . 'Edit';
$this->params['breadcrumbs'][] = ['label' => 'Kredit', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id_kredit, 'url' => ['view', 'id_kredit' => $model->id_kredit]];
$this->params['breadcrumbs'][] = 'Edit';
?>

<p>
    <?= Html::a('<i class="fa fa-eye-open"></i> Lihat', ['view', 'id_kredit' => $model->id_kredit], ['class' => 'btn btn-default']) ?>
</p>

<?php echo $this->render('_form', [
'model' => $model,
]); ?>
