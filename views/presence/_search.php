<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var app\models\search\PresenceSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="presence-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'qr_code') ?>

		<?= $form->field($model, 'user_id') ?>

		<?= $form->field($model, 'name') ?>

		<?= $form->field($model, 'temperature') ?>

		<?php // echo $form->field($model, 'photo') ?>

		<?php // echo $form->field($model, 'is_guest') ?>

		<?php // echo $form->field($model, 'is_late') ?>

		<?php // echo $form->field($model, 'date') ?>

		<?php // echo $form->field($model, 'action') ?>

		<?php // echo $form->field($model, 'time_in') ?>

		<?php // echo $form->field($model, 'time_out') ?>

		<?php // echo $form->field($model, 'time_late') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
