<?php

use app\components\CustomForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\MasterPresence $model
 * @var yii\widgets\ActiveForm $form
 */

?>

<div class="box box-info">
    <div class="box-body">
        <?php $form = ActiveForm::begin([
            'id' => 'MasterPresence',
            'layout' => 'horizontal',
            'enableClientValidation' => true,
            'errorSummaryCssClass' => 'error-summary alert alert-error',
        ]);
        ?>
        <?=$form->field($model, 'time_in_min', CustomForm::COLUMN_VERTICAL_2)->textInput(['type' => 'time',])?>
        <?=$form->field($model, 'time_in_max', CustomForm::COLUMN_VERTICAL_2)->textInput(['type' => 'time',])?>
        <?=$form->field($model, 'time_out_min', CustomForm::COLUMN_VERTICAL_2)->textInput(['type' => 'time',])?>
        <?=$form->field($model, 'time_out_max', CustomForm::COLUMN_VERTICAL_2)->textInput(['type' => 'time',])?>
        <hr />
        <?php echo $form->errorSummary($model); ?>
        <div class="row">
            <div class="col-md-offset-3 col-md-10">
                <?=Html::submitButton('<i class="fa fa-save"></i> Simpan', ['class' => 'btn btn-success']);?>
                <?=Html::a('<i class="fa fa-chevron-left"></i> Kembali', ['index'], ['class' => 'btn btn-default'])?>
            </div>
        </div>

        <?php ActiveForm::end();?>

    </div>
</div>