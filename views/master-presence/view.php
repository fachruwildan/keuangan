<?php

use dmstr\bootstrap\Tabs;
use dmstr\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\MasterPresence $model
 */

$this->title = 'Master Presence';
$this->params['breadcrumbs'][] = ['label' => 'Master Presence', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string) "Detail", 'url' => ['view', 'id' => $model->id]];
// $this->params['breadcrumbs'][] = 'View';
?>
<div class="giiant-crud master-presence-view">

    <!-- menu buttons -->
    <p class='pull-left'>
        <?php echo Html::a('<span class="glyphicon glyphicon-pencil"></span> ' . 'Edit', ['update', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
        <?php // echo Html::a('<span class="glyphicon glyphicon-plus"></span> ' . 'Tambah Baru', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <p class="pull-right">
        <?php // echo Html::a('<span class="glyphicon glyphicon-list"></span> ' . 'Daftar Master Presence', ['index'], ['class'=>'btn btn-default']) ?>
    </p>

    <div class="clearfix"></div>

    <!-- flash message -->
    <?php if (\Yii::$app->session->getFlash('deleteError') !== null): ?>
        <span class="alert alert-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <?=\Yii::$app->session->getFlash('deleteError')?>
        </span>
    <?php endif;?>

    <div class="box box-info">
        <div class="box-body">
            <?php $this->beginBlock('app\models\MasterPresence');?>

            <?=DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'attribute' => 'time_in_min',
                        'value' => function($model){
                            return $model->time_in_min;
                        }
                    ],
                    [
                        'attribute' => 'time_in_max',
                        'value' => function($model){
                            return $model->time_in_max;
                        }
                    ],
                    [
                        'attribute' => 'time_out_min',
                        'value' => function($model){
                            return $model->time_out_min;
                        }
                    ],
                    [
                        'attribute' => 'time_out_max',
                        'value' => function($model){
                            return $model->time_out_max;
                        }
                    ],
                    // 'created_at',
                    // 'updated_at',
                ],
            ]);?>

            <hr/>

            <?php // echo Html::a('<span class="glyphicon glyphicon-trash"></span> ' . 'Delete', ['delete', 'id' => $model->id],
            // [
            //     'class' => 'btn btn-danger',
            //     'data-confirm' => '' . 'Are you sure to delete this item?' . '',
            //     'data-method' => 'post',
            // ]);?>
            <?php $this->endBlock();?>



            <?=Tabs::widget(
                [
                    'id' => 'relation-tabs',
                    'encodeLabels' => false,
                    'items' => [[
                        'label' => '<b class=""># Pengaturan Waktu</b>',
                        'content' => $this->blocks['app\models\MasterPresence'],
                        'active' => true,
                    ]],
                ]
            );
            ?>
        </div>
    </div>
</div>
