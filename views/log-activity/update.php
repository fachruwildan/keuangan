<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\LogActivity $model
*/

$this->title = 'Log Activity ' . $model->name . ', ' . 'Edit';
$this->params['breadcrumbs'][] = ['label' => 'Log Activity', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->name, 'url' => ['view', 'id_log' => $model->id_log]];
$this->params['breadcrumbs'][] = 'Edit';
?>

<p>
    <?= Html::a('<i class="fa fa-eye-open"></i> Lihat', ['view', 'id_log' => $model->id_log], ['class' => 'btn btn-default']) ?>
</p>

<?php echo $this->render('_form', [
'model' => $model,
]); ?>
