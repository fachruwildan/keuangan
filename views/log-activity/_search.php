<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var app\models\search\LogActivitySearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="log-activity-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id_log') ?>

		<?= $form->field($model, 'name') ?>

		<?= $form->field($model, 'debit') ?>

		<?= $form->field($model, 'kredit') ?>

		<?= $form->field($model, 'tanggal') ?>

		<?php // echo $form->field($model, 'keterangan') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
