<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\Perusahaan $model
*/

$this->title = 'Perusahaan ' . $model->id_perusahaan . ', ' . 'Edit';
$this->params['breadcrumbs'][] = ['label' => 'Perusahaan', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id_perusahaan, 'url' => ['view', 'id_perusahaan' => $model->id_perusahaan]];
$this->params['breadcrumbs'][] = 'Edit';
?>

<p>
    <?= Html::a('<i class="fa fa-eye-open"></i> Lihat', ['view', 'id_perusahaan' => $model->id_perusahaan], ['class' => 'btn btn-default']) ?>
</p>

<?php echo $this->render('_form', [
'model' => $model,
]); ?>
