<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\Datepicker;

/**
* @var yii\web\View $this
* @var app\models\search\DailyScheduleSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="daily-schedule-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

<?=
 $form->field($searchModel, 'date')->widget(\kartik\datecontrol\DateControl::class, [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Pilih Tanggal',
                'autoclose' => true
            ]
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
