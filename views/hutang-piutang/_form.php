<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use kartik\date\DatePicker;
/**
* @var yii\web\View $this
* @var app\models\HutangPiutang $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="box box-info">
    <div class="box-body">
        <?php $form = ActiveForm::begin([
        'id' => 'HutangPiutang',
        'layout' => 'horizontal',
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error'
        ]
        );
        ?>
			<?= $form->field($model, 'perusahaan')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'kelompok')->dropDownList(['Piutang'=> 'Piutang','Hutang'=> 'Hutang'],['prompt'=>'Hutang / Piutang ']) ?>
            <?=$form->field($model, 'nominal')->widget(\kartik\money\MaskMoney::classname(), [
     'value' => 1000,
     'pluginOptions' => [
         'prefix' => 'Rp ',
         'precision' => 0
    ]
]); ?>
			<?= $form->field($model, 'tanggal_tempo')->widget(DatePicker::classname(), [
    'options' => ['placeholder' => 'Pilih Tanggal Jatuh Tempo'],
    'readonly' => true,
    'pluginOptions' => [
        'format' => 'yyyy-m-d',
        'autocomplete' => "off",
    ],
]);?>      <hr/>
        <?php echo $form->errorSummary($model); ?>
        <div class="row">
            <div class="col-md-offset-3 col-md-10">
                <?=  Html::submitButton('<i class="fa fa-save"></i> Simpan', ['class' => 'btn btn-success']); ?>
                <?=  Html::a('<i class="fa fa-chevron-left"></i> Kembali', ['index'], ['class' => 'btn btn-default']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>