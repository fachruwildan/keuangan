<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var app\models\search\HutangPiutang $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="hutang-piutang-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'id_user') ?>

		<?= $form->field($model, 'perusahaan') ?>

		<?= $form->field($model, 'kelompok') ?>

		<?= $form->field($model, 'piutang') ?>

		<?php // echo $form->field($model, 'hutang') ?>

		<?php // echo $form->field($model, 'tanggal_tempo') ?>

		<?php // echo $form->field($model, 'created_at') ?>

		<?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
