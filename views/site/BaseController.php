<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Kecamatan;
use App\Models\Banner;
use App\Models\Desa;
use App\Models\Polygon;
use App\Models\EventPohon;
use App\Models\EventPohonFoto;
use App\Http\Resources\EventPohonCollection;
use App\Http\Resources\KecamatanCollection;
use App\Http\Resources\DesaCollection;
use App\Components\DetikCrawl;
use App\components\IG;
use App\Components\JatimTimesCrawl;
use App\Components\JawaPosCrawl;
use App\Components\MalangPostCrawl;
use App\Http\Resources\BannerCollection;
use App\Http\Resources\DesaResource;
use App\Models\News;
use Illuminate\Support\Facades\Validator;

class BaseController extends Controller
{
    use \App\Traits\UploadTrait;

    public function kecamatan(){
        return new KecamatanCollection(Kecamatan::paginate());
    }

    public function desa($id){
        $kecamatan = Kecamatan::find($id);
        if(!$kecamatan) return response()->json(['status' => 'fail', 'message' => 'Data tidak ditemukan' ]);
        return new DesaCollection(Desa::where(['kecamatan_id' => $kecamatan->id])->paginate());
    }
    
    public function sortFunction( $a, $b ) {
        return strtotime($a["date"]) < strtotime($b["date"]);
    }

    public function terkini(Request $request){
        $response = (new MalangPostCrawl((int)$request->page))->response();

        $response_length = count($response) - 1;
        if($response_length == -1){
            $article_db = News::whereBetween('date', [date("Y-m-d", strtotime("-4 days")), date("Y-m-d")])->get();
        }else{
            $first_article = date("Y-m-d", strtotime($response[0]['date']));
            $last_article = date("Y-m-d", strtotime($response[$response_length]['date']));
    
            if ((int)$request->page == 0 or (int)$request->page == 1){
                $first_article = date("Y-m-d");
            }
            $article_db = News::whereBetween('date', [$last_article, $first_article])->get();    
        }

        $list_article_db = [];

        foreach($article_db as $article){
            array_push($list_article_db, [
                "url" => url("api/self-detail-terkini/{$article->id}"),
                "image" => url("storage/{$article->image}"),
                "date" => date("d F Y", strtotime($article->date)),
                "title" => $article->title
            ]);
        }

        $response = array_merge($response, $list_article_db);
        usort($response, [$this, "sortFunction"]);
        if(count($response)) return response()->json([ 'status' => 'success', 'message' => 'Berhasil mendapatkan data', 'data' => $response ]);
        return response()->json([ 'status' => 'fail', 'message' => 'Data sudah habis' ]);
    }

    public function selfDetailTerkini($id){
        $model = News::where(['id' => $id])->first();
        if($model == []) return response()->json([ 'status' => 'fail', 'message' => 'Gagal mendapatkan data.']);

        $image = url("storage/$model->image");
        return response()->json([
            'status' => 'success',
            "message" => "Data berhasil didapatkan",
            "data" => [
                "url" => $model->url,
                "image" => $model->image,
                "title" => $model->title,
                "content" => "<p><img src='{$image}' class='img img-responsive'></p>$model->content"
            ]
        ]);
    }

    public function detailTerkini(){
        if(isset($_GET['link'])){
            $page = $_GET['link'];
        }else{
            return response()->json(['status' => 'fail', 'message' => 'link does not exist']);
        } 
        
        if($page == null) return response()->json(['status' => 'fail', 'message' => "Data tidak ditemukan"]);

        $response = MalangPostCrawl::detail($page);
        if($response) return response()->json([ 'status' => 'success', 'message' => 'Berhasil mendapatkan data', 'data' => $response ]);
        return response()->json([ 'status' => 'fail', 'message' => 'Terjadi kesalahan' ]);

    }

    // public function insert(Request $request){

    //     $desa = Desa::find($request->id);
    //     $desa->latitude = $request->latitude;
    //     $desa->longitude = $request->longitude;
    //     $desa->save();

    //     return response()->json($desa);
    // }

    public function polygon(){
        $desa = Desa::get();

        $output = [
            "status" => "success",
            "data" => []
        ];
        
        foreach($desa as $index => $value){
            $res = Polygon::where(['wilayah_desa_id' => $value->id])->get();
            $output["data"][$index] = [
                "id" => $value->id,
                "name" => $value->name,
                "marker" => [
                    "lng" => $value->longitude,
                    "lat" => $value->latitude,
                ]
            ];
            foreach($res as $lines){
                $output["data"][$index]["coordinates"][] = [
                    "lat" => $lines->latitude,
                    "lng" => $lines->longitude
                ];
            }
        }

        return response()->json($output);
    }

    public function desaAll(){
        return new DesaCollection(Desa::get());
    }

    public function desaShow($id){
        $desa = Desa::find($id);

        if(!$desa) return response()->json(['status' => 'fail', 'message' => 'Desa tidak ditemukan']);
        return response()->json([
            'status' => 'success',
            'message' => "Data berhasil didapatkan",
            "data" => new DesaResource($desa)
        ]);
    }

    public function banner(){
        return new BannerCollection(Banner::get());
    }
    
    public function isQRActive(){
        $setting = \App\Models\Settings::where(['id' => 1])->first();
        if($setting == []){
            return response()->json([
                "status" => "fail",
                "message" => "data tidak ditemukan" 
            ]);
        }
        return response()->json([
            "status" => "success",
            "message" => "Data berhasil didapatkan",
            "data" => $setting->is_qr_active
        ]);
    }

    public function getIg($name){
        return IG::get($name);
        return response()->json(
            json_decode(
                '
[{"display_url":"https:\/\/scontent-sin6-3.cdninstagram.com\/v\/t51.2885-15\/e35\/s1080x1080\/151046565_456360065779029_
    8349705681354464556_n.jpg?_nc_ht=scontent-sin6-3.cdninstagram.com&_nc_cat=109&_nc_ohc=3K8Hn0tncoUAX-n9VOx&tp=1&oh=
    f8f9ff2874b496a791300bb5e13d0666&oe=60590AE8","is_video":false,"captions":"[H-2 Menuju The Next Trip SS]\n-\nNggak
     kerasa sudah h-2 menuju The Next Trip SS nih! Tapi sebelum itu, list starter pack dong! Kira kira apa aja yang perlu
      dibawa supaya Arun Trek dan Fun Bike nya aman dan nyaman?\ud83e\udd29 Absen juga dong di kolom komentar siapa
       yang udah daftar The Next Trip SS Hari Minggu besok?\ud83d\udc47\u2728\n-\n#amongtanifoundation #batutracking19 
       #wisatabatu #explorebatu #wisatabatumalang #songgoriti #selectamalang"},{"display_url":"https:\/\/scontent-sin6-
        2.cdninstagram.com\/v\/t51.2885-15\/e35\/150705693_849692802539957_5923614524210904058_n.jpg?_nc_ht=scontent-
        sin6-2.cdninstagram.com&_nc_cat=105&_nc_ohc=m0ADTymq6EkAX-_3vmC&tp=1&oh=3227bdc65fa284a21a9c025ccd285521&oe=
        60339DAC","is_video":true,"captions":"[H-3 MENUJU THE NEXT TRIP SS]\n-\nMau ngingetin kalau 3 Hari lagi menuju 
        The Next Trip SS! \ud83e\udd73Siapa yang sudah daftar?\ud83d\ude1dAbsen dulu di komentar yuk!\ud83d\udc47\n-\n#
        amongtanifoundation #batutracking19 #songgoriti #wisatabatu #explorebatu #selectamalang","video_url":"https:\/\/
        scontent-sin6-3.cdninstagram.com\/v\/t50.2886-16\/150712316_1051728415307337_7857392956789883404_n.mp4?_nc_ht=
        scontent-sin6-3.cdninstagram.com&_nc_cat=109&_nc_ohc=8WFlA7-9mXAAX_VjlJC&oe=6033C9A9&oh=9ae5984209dc64aae41498e
        b3cf0b793"},{"display_url":"https:\/\/scontent-sin6-2.cdninstagram.com\/v\/t51.2885-15\/e35\/150787098_37807723
            78628132_7805773377375066083_n.jpg?_nc_ht=scontent-sin6-2.cdninstagram.com&_nc_cat=108&_nc_ohc=kjKZXRTIpt4A
            X95z4f0&tp=1&oh=c53256c67c6af72cba549530fa9c4869&oe=60337593","is_video":true,"captions":"Apa itu Batu Track
            ing-19? Fitur di Aplikasi Batu Tracking-19 itu Apa Aja ya? Kalau mau download Batu Tracking 19 itu Dimana?\n
            -\nBuat yang masih bingung apa sih Batu Tracking 19 itu? Tonton video ini sampai habis ya! \ud83e\udd29\ud83
            e\udd29\n\n-\n#amongtanifoundation #batutracking19 #wisatabatumalang #explorebatu #wisatabatu","video_url":"https:\/\/scontent-sin6-3.cdninstagram.com\/v\/t50.2886-16\/150507455_3954982647896037_1299989885732307985_n.mp4?_nc_ht=scontent-sin6-3.cdninstagram.com&_nc_cat=109&_nc_ohc=xu5_cDHcI-8AX-EBrsD&oe=6033AD8B&oh=14b7c39c37be3fa31c4710759538648d"},{"display_url":"https:\/\/scontent-sin6-1.cdninstagram.com\/v\/t51.2885-15\/e35\/149937778_270464121142771_3514235947607887718_n.jpg?_nc_ht=scontent-sin6-1.cdninstagram.com&_nc_cat=107&_nc_ohc=nkKmFWE_Xk4AX_ESsUK&tp=1&oh=4db044bdb56e6fbfa505966060dc2945&oe=6033C0CA","is_video":true,"captions":"[Modal Rp 50.000 aja, kulineran berdua di Alun Alun Batu bisa dapat apa aja ya?]\n-\nMasih edisi valentine, nih\ud83e\udd29 Nggak afdol wisata ke Batu kalau nggak mampir di alun alun Batu, betul apa betul? Modal Rp 50.000 versi Mimin kayak gini, kalau kamu kayak gimana? Tag pasangan kamu di kolom komentar ya!\ud83d\ude0d\n-\n#batutracking19 #wisatabatu #explorebatumalang #amongtanifoundation #alunalunwisatabatu","video_url":"https:\/\/scontent-sin6-2.cdninstagram.com\/v\/t50.2886-16\/149334606_411467533485711_4767047554158610564_n.mp4?_nc_ht=scontent-sin6-2.cdninstagram.com&_nc_cat=103&_nc_ohc=ajW6WGqwPjMAX-43jAf&oe=60339397&oh=a64b465cac67278f72f446164c8b9cf3"},{"display_url":"https:\/\/scontent-sin6-1.cdninstagram.com\/v\/t51.2885-15\/e35\/s1080x1080\/149128245_437886530978077_8497754963286153417_n.jpg?_nc_ht=scontent-sin6-1.cdninstagram.com&_nc_cat=101&_nc_ohc=o3cbbjFCEiUAX9trmPL&tp=1&oh=78cab71fc60201f791c48ec21594f23c&oe=605C8B00","is_video":false,"captions":"[H-1 Menuju Hari...?]\n-\nAda yang tahu besok hari apa?\ud83d\ude0c Yup, Hari Valentine sekaligus Hari Minggu! Merayakan valentine pas banget di Dekleine Batu! Suasana yang romantis, hawa sejuk, dan pemandangan alam yang cantik cocok banget jadi saksi hari valentine kamu bersama pasangan\ud83e\udd70 Tapi jangan lupa, harus tetap patuh protokol kesehatan ya!\ud83d\ude00\ud83d\udc4d\n-\n #batutracking19  #amongtanibatu #dekleinebatu #dekleine #explorebatu #wisatabatu"},{"display_url":"https:\/\/scontent-sin6-2.cdninstagram.com\/v\/t51.2885-15\/e35\/149444815_2527240814236796_4586630501232924013_n.jpg?_nc_ht=scontent-sin6-2.cdninstagram.com&_nc_cat=108&_nc_ohc=XUBKjCRHiBEAX_hXYhX&tp=1&oh=cf1211c62e1eb72759d2e524db8f9a24&oe=6033AEF4","is_video":true,"captions":"[SELAMAT TAHUN BARU IMLEK 2021]\n-\nImlek tahun ini berlambang shio kerbau yang berarti keberuntungan melalui kerja keras & ketabahan. Semoga imlek tahun ini menjadi suatu keberuntungan dan kemakmuran bagi kita semua\ud83d\ude0a.\n\nINFO ANGPAO?\n-\n#batutracking19 #amongtanifoundation \n#amongtanimilenial \n#amongtanibatu #imlek2021 #explorebatu #wisatabatu #wisatabatumalang","video_url":"https:\/\/scontent-sin6-1.cdninstagram.com\/v\/t50.2886-16\/148897164_888244141747087_4339640297255012848_n.mp4?_nc_ht=scontent-sin6-1.cdninstagram.com&_nc_cat=107&_nc_ohc=wDWUKhuX6FEAX-h8mc7&oe=6033D76B&oh=affea61438879ee715d7a09c522c6681"},{"display_url":"https:\/\/scontent-sin6-3.cdninstagram.com\/v\/t51.2885-15\/e35\/147897847_406748370419170_8317788449773941027_n.jpg?_nc_ht=scontent-sin6-3.cdninstagram.com&_nc_cat=110&_nc_ohc=44V65AZVEAoAX9Q-Eor&tp=1&oh=1c8fa639ffaaa153ca227a66359d38ed&oe=603368C8","is_video":true,"captions":"Siapa yang udah nggak sabar long weekend?\ud83d\ude4b\n-\nMau ingetin besok terakhir kerja di minggu ini ya\ud83d\ude0c Tim #liburan  atau Tim #dirumahsaja nih? Tapi harus tetap jaga kesehatan dan taat protokol kesehatan ya! \ud83e\udd1c\ud83e\udd1b\n-\n#amongtanifoundation \n#batutracking19 \n#amongtanibatu #wisatabatu #wisatabatumalang #explorebatu","video_url":"https:\/\/scontent-sin6-3.cdninstagram.com\/v\/t50.2886-16\/148833858_177068257160524_4720933664225063857_n.mp4?_nc_ht=scontent-sin6-3.cdninstagram.com&_nc_cat=104&_nc_ohc=Oe1Tca3quIMAX9U9b8r&oe=60333FA0&oh=595de7bf917524db648f37557fd9e9d2"},{"display_url":"https:\/\/scontent-sin6-1.cdninstagram.com\/v\/t51.2885-15\/e35\/148200928_410456673384967_1578255107662849180_n.jpg?_nc_ht=scontent-sin6-1.cdninstagram.com&_nc_cat=107&_nc_ohc=heQaOUQlpTUAX_SBWeh&tp=1&oh=91322183daf9d5898b3e1da70bc7cc9b&oe=60338DED","is_video":true,"captions":"Setuju nggak nih kalau Batu emang paling pas buat refreshing?\ud83d\ude1d\n-\nJangan lupa 2 hari lagi menuju long weekend! Kamu udah planning liburan atau tetep di rumah aja? Mau di rumah aja atau liburan harus tetap patuh protokol kesehatan ya!\ud83e\udd1c\ud83e\udd1b\n-\n#batutracking19 \n#amongtanifoundation \n#amongtanibatu #wisatabatu #wisatabatumalang #explorebatu","video_url":"https:\/\/scontent-sin6-3.cdninstagram.com\/v\/t50.2886-16\/147986944_441564703629906_1992322505850848453_n.mp4?_nc_ht=scontent-sin6-3.cdninstagram.com&_nc_cat=106&_nc_ohc=q9RDnxHMLhoAX_3JAa_&oe=60334753&oh=62c726da0576477e60239ac486a5fed4"},{"display_url":"https:\/\/scontent-sin6-3.cdninstagram.com\/v\/t51.2885-15\/e35\/147439730_4107274945958925_668318369101300562_n.jpg?_nc_ht=scontent-sin6-3.cdninstagram.com&_nc_cat=109&_nc_ohc=O5dpWnhCrgoAX-obwep&tp=1&oh=da02bc2fbac746b2f69a461cbeae09e7&oe=605C3A2D","is_video":false,"captions":"[ROAD TO THE NEXT TRIP SS]\n -\nHari Minggu (07\/02) kemarin, diadakan trial dari The Next Trip SS yang dimulai dari Selecta sampai Songgoriti. Trial ini diikuti dari berbagai kalangan loh, mulai dari masyarakat Kota Batu dan Komunitas. \n-\nJadi siapa yang udah nggak sabar tanggal 21 nanti?\ud83e\udd29\ud83e\udd29 Yang udah daftar komen di bawah yaa!\n\n#batutracking19 \n#amongtanifoundation #wisatabatu #wisatabatumalang #explorebatu"},{"display_url":"https:\/\/scontent-sin6-3.cdninstagram.com\/v\/t51.2885-15\/e35\/131402324_1062552644170516_3179838995806200069_n.jpg?_nc_ht=scontent-sin6-3.cdninstagram.com&_nc_cat=104&_nc_ohc=cdLRag7oJu4AX-TOobz&tp=1&oh=f05ca27d141f97869d3d662cca55f2de&oe=6033CD40","is_video":true,"captions":"Sebuah rekap video dari acara Legend of Songgoriti @nikisonggoritifestival yang diadakan setiap tahun.\n\nDan konsep tahun ini adalah pengenalan Aplikasi Batutracking19 kepada Warga Songgokerto dan pelepasan hewan ke habitatnya (1 Nama 1 Satwa), peresmian tempat wisata baru dan penyerahan bantuan dari @bankjatim \n\n\u2014\u2014\u2014\n\n#batutracking19 #wisatabatu #1Nama1Pohon #batumenghijau #AmongTaniMilenial #AmongTaniFoundation #wisatabatu #explorebatu","video_url":"https:\/\/scontent-sin6-2.cdninstagram.com\/v\/t50.2886-16\/131354103_109712054335508_6824007913794938710_n.mp4?_nc_ht=scontent-sin6-2.cdninstagram.com&_nc_cat=108&_nc_ohc=WhWca3xQYAUAX9u0a4j&oe=6033C597&oh=30f923aae3b1306300c51fd40c4b4fcd"},{"display_url":"https:\/\/scontent-sin6-1.cdninstagram.com\/v\/t51.2885-15\/e35\/s1080x1080\/130874634_172292374605672_5300973852446328743_n.jpg?_nc_ht=scontent-sin6-1.cdninstagram.com&_nc_cat=100&_nc_ohc=tgGXB3TUFRQAX9vvfNW&tp=1&oh=f3752a1967fd241e75ca2b3d040744e3&oe=605A5C54","is_video":false,"captions":"Nah kan sudah liat sebagian rekap foto waktu acara Legend Of Songgoriti ternyata bisa terealisasi dengan aman dan sentosa dikarenakan Protokol Kesehatannya diperketat, tentunya juga berkat aplikasi Batutracking19 yang turut membantu proses registrasi untuk masuk ke dalam agar kuota pengunjung pun dijaga ketat.\n\nSerta dengan mata batu mengawasi jarak aman dari masing-masing pengunjung. Jangan lupa downlod aplikasi Batutracking19 ya!\n\n\u2014\u2014\u2014\n\n #batutracking19 #1Nama1Pohon #batumenghijau #AmongTaniMilenial #AmongTaniFoundation #explorebatu #wisatabatumalang #wisatabatu"},{"display_url":"https:\/\/scontent-sin6-2.cdninstagram.com\/v\/t51.2885-15\/e35\/130921504_188165303000396_7034110132242898467_n.jpg?_nc_ht=scontent-sin6-2.cdninstagram.com&_nc_cat=108&_nc_ohc=equkuUn9MBQAX86FYzU&tp=1&oh=4cb61d14fc4fac48c19fafe2b3f7b5b8&oe=605BC5CB","is_video":false,"captions":"The Legend of Songgoriti
    @nikisonggoritifestival by @amongtanifoundation \n\nSekaligus sebagai pengenalan fitur \u2018Pagar Batu\u2019 dari 
    aplikasi Batutracking19. Acara ini terealisasi dengan berbagai Protokol Kesehatan yang ketat mulai dari registrasi hingga acara selesai.\n\nBertempat di Pine Park Songgoriti, 
    acara ini dibuka oleh Ibu Walikota @dewanti_rumpoko dengan melepas satwa ke habitat alaminya. 
    Serta penyerahan bantuan dari @bankjatim untuk Warga Kota Batu.\n\n\u2014\u2014\u2014\n\n#batutracking19 #wisatabatu 
    #1Nama1Pohon #batumenghijau #AmongTaniMilenial #AmongTaniFoundation #wisatabatumalang #explorebatu"}]'
            )
        );
    }
}