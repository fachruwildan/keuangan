<?php 
use yii\helpers\Html;


$ttd_komisaris=(new \yii\db\Query())
->select(['tanda_tangan','name'])
->from('user')
->Where(['role_id'=>'6'])
->all();
?>
<table>
         <thead>
                <h1></h1>
                <tr>
               <th width="25%" style="padding-left: 5px;
                 padding-bottom: 3px;">
                <strong style="font-size: 13px;"><?="Komisaris"?></strong>
                </th>
            </tr>
            <tr class>
                    <td><?= Html::img(["uploads/".$ttd_komisaris[0]['tanda_tangan']], ["width"=>"150px"]); ?></td>
                </tr>
                <tr>
                    <td style="padding-left: 5px; padding-bottom: 3px;">
                    <strong style="font-size: 10px;"><?= $ttd_komisaris[0]['name'] ?></strong>
                    </td>
                </tr>
         </thead>
         <tbody>
         </tbody>
         </table>