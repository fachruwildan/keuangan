<?php

namespace app\controllers\api;

use app\models\FunTrack;
use app\models\FunTrackPack;
use app\models\FunTrackParticipant;
use app\models\FunTrackRoute;
use app\models\User;
use Yii;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * This is the class for REST controller "FunTrackParticipantController".
 */

class FunTrackParticipantController extends \yii\rest\ActiveController
{

    /**
     * List of allowed domains.
     * Note: Restriction works only for AJAX (using CORS, is not secure).
     *
     * @return array List of domains, that can access to this API
     */
    public static function allowedDomains()
    {
        return [
            '*', // star allows all domains
            // 'http://test1.example.com',
            // 'http://test2.example.com',
        ];
    }

/**
 * @inheritdoc
 */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [

            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => static::allowedDomains(),
                    'Access-Control-Request-Method' => ['POST'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],

        ]);
    }

    public $modelClass = 'app\models\FunTrackParticipant';

    public function actions()
    {
        $parent = parent::actions();
        unset($parent['index']);
        unset($parent['view']);
        unset($parent['create']);
        unset($parent['update']);
        unset($parent['delete']);
        return $parent;
    }

    public function actionRegistration()
    {
        $user_id = $_POST['user_id'];
        $fun_track_id = $_POST['fun_track_id'];
        $fun_track_pack_id = $_POST['fun_track_pack_id'];

        $user = User::findOne(['id' => $user_id]);
        if ($user == []) {
            return [
                "success" => false,
                "message" => "User doesnt exist.",
            ];
        }

        $fun_track = FunTrack::findOne(['id' => $fun_track_id]);
        if ($fun_track == []) {
            return [
                "success" => false,
                "message" => "Fun track not found.",
            ];
        }

        $fun_track_pack = FunTrackPack::findOne(['id' => $fun_track_pack_id]);
        if ($fun_track_pack == []) {
            return [
                "success" => false,
                "message" => "Fun track pack not found.",
            ];
        }

        $cek = FunTrackParticipant::findOne(['fun_track_id' => $fun_track->id, 'user_id' => $user->id]);
        if ($cek) {
            return [
                "success" => false,
                "message" => "User already register.",
            ];
        }

        // validasi tanggal dan jumlah peserta
        $now = time();
        if ($now > strtotime($fun_track->registration_deadline) && $now < strtotime($fun_track->registration_start)) {
            return [
                "success" => false,
                "message" => "Registration is closed because the participant has reached a maximum.",
            ];
        }

        $total = $fun_track->getFunTrackParticipants()->where(['fun_track_id' => $fun_track->id])->count();

        if ($total >= $fun_track->max_participant) {
            return [
                "success" => false,
                "message" => "Registration is closed because the participant has reached a maximum.",
            ];
        }

        $fun_track_participant = new FunTrackParticipant();
        $fun_track_participant->user_id = $user->id;
        $fun_track_participant->name = $user->name;
        $fun_track_participant->fun_track_id = $fun_track->id;
        $fun_track_participant->fun_track_pack_id = $fun_track_pack->id;

        if ($fun_track->is_action == 'track') {
            $file = UploadedFile::getInstance($fun_track_participant, "payment_attachment");
            if ($_POST['fun_track_id'] == null || $_POST['fun_track_pack_id'] == null || $file == [] || $_POST['disease_history'] == "") {
                return [
                    "success" => false,
                    "message" => "Image / Disease History cant be blank",
                ];
            }

            $response = $this->uploadFile($file);
            $fun_track_participant->payment_attachment = $response["data"]["filename"];
            $fun_track_participant->disease_history = $_POST['disease_history'];
        }

        if ($fun_track->is_action == 'speech') {
            if ($_POST['place_of_birth'] == "" || $_POST['date_of_birth'] == "" || $_POST['school_name'] == "") {
                return [
                    "success" => false,
                    "message" => "place of birth / date of birth / school name cant be blank.",
                ];
            }

            $birth_time = strtotime($_POST['date_of_birth']);
            $year_of_birth = (int) date('Y', $birth_time);
            $year_now = (int) date('Y');

            $fun_track_participant->age = $year_now - $year_of_birth;
            $fun_track_participant->date_of_birth = $_POST['date_of_birth'];
            $fun_track_participant->place_of_birth = $_POST['place_of_birth'];
            $fun_track_participant->school_name = $_POST['school_name'];

            $compare_date_month = str_replace("$year_now", "$year_of_birth", date("Y-m-d"));
            if ($birth_time > strtotime($compare_date_month)) {
                $fun_track_participant->age += 1;
            }

            if ($fun_track_participant->age < 9 || $fun_track_participant->age > 15) {
                return [
                    "success" => false,
                    "message" => "You're too young / old to participate in this event ",
                ];
            }

            // langsung dikonfirmasi sistem karena gratis
            $fun_track_participant->is_confirmed = 1;
        }

        // REGISTRATION NUMBER GENERATATOR
        if ($fun_track_pack_id) {
            $pack = FunTrackPack::findOne(['id' => $fun_track_pack_id]);
            if ($pack) {
                $code = $pack->code;
                $fun_track_participant->fun_track_pack_id = $pack->id;
            } else {
                $code = "LTC";
            }
        } else {
            $code = "LTC";
        }

        $suffix = $code;
        $check_aktiva = FunTrackParticipant::find()->where(['like', 'registration_number', "$suffix"])->max('registration_number');

        if ($check_aktiva != null) {
            $no_max = (int) explode("-", $check_aktiva)[1];
            $no_max++;
        } else {
            $no_max = 1;
        }

        $registration_number = $suffix . "-" . sprintf("%05d", $no_max);
        $fun_track_participant->registration_number = $registration_number;

        if ($fun_track_participant->validate() == false) {
            return [
                "success" => false,
                "message" => "Error when validation input.",
                "data" => $fun_track_participant->getErrors(),
            ];
        }

        $fun_track_participant->registered_at = date("Y-m-d H:i:s");
        $fun_track_participant->updated_at = date("Y-m-d H:i:s");

        $fun_track_participant->save();

        return [
            "success" => true,
            "message" => "Registration success.",
            "data" => $fun_track_participant,
        ];
    }

    public function uploadFile($instance, $default = null)
    {
        $response = [];

        if ($instance) {
            $arr_name = explode(".", $instance->name);
            $extension = end($arr_name);
            $filename = Yii::$app->security->generateRandomString(50) . ".$extension";
            $path = Yii::getAlias("@webroot/uploads/fun-track/participant/$filename");
            $instance->saveAs($path);

            $response = [
                "success" => true,
                "message" => "Upload Success",
                "data" => [
                    "filename" => $filename,
                ],
            ];
        } else {
            $response = [
                "success" => false,
                "message" => "File cant be blank",
                "data" => [],
            ];
        }
        return $response;
    }

    public function actionUpdateCoordinate()
    {
        $user_id = $_POST['user_id'];
        $fun_track_id = $_POST['fun_track_id'];
        $user = User::findOne(['id' => $user_id]);

        if ($user == []) {
            return [
                "success" => false,
                "message" => "User doesnt exist.",
            ];
        }

        $model = FunTrackParticipant::findOne(['fun_track_id' => $fun_track_id, 'user_id' => $user_id]);
        if ($model == []) {
            return [
                "success" => false,
                "message" => "You didn't register yet.",
            ];
        }

        $model->latitude = $_POST['latitude'];
        $model->longitude = $_POST['longitude'];

        if ($model->save()) {
            return [
                "success" => true,
                "message" => "Location updated.",
            ];
        }

        return [
            "success" => false,
            "message" => "failed when updating location.",
        ];
    }

    public function actionCheckUserRegistered()
    {
        $user_id = $_POST['user_id'];
        $fun_track_id = $_POST['fun_track_id'];
        $user = User::findOne(['id' => $user_id]);

        if ($user == []) {
            return [
                "success" => false,
                "message" => "User doesnt exist.",
            ];
        }

        $cek = FunTrackParticipant::findOne(['id' => $fun_track_id, 'user_id' => $user_id]);
        if ($cek) {
            return [
                "success" => true,
                "message" => "User is registered.",
            ];
        }

        return [
            "success" => false,
            "message" => "User isnt registered.",
        ];
    }

    public function actionViewUser()
    {
        $user_id = $_GET['user_id'];
        $fun_track_id = $_GET['fun_track_id'];
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($fun_track_id == "" && $user_id == "") {
            return [
                "success" => false,
                "message" => "Fun track not found.",
            ];
        }
        $user = User::findOne(['id' => $user_id]);

        if ($user == []) {
            return [
                "success" => false,
                "message" => "User doesnt exist.",
            ];
        }

        $fun_track = FunTrack::findOne(['id' => $fun_track_id]);

        if ($fun_track == []) {
            return [
                "success" => false,
                "message" => "Fun track not found.",
            ];
        }

        $cek = FunTrackParticipant::findOne(['fun_track_id' => $fun_track->id, 'user_id' => $user->id]);
        if ($cek) {
            return [
                "success" => true,
                "message" => "Data found",
                "data" => $cek,
            ];
        }

        return [
            "success" => false,
            "message" => "Data not found",
        ];
    }

    public function actionCheckParticipant()
    {
        $fun_track_id = $_GET['fun_track_id'];
        $fun_track_pack_id = $_GET['fun_track_pack_id'];
        $user_id = $_GET['user_id'];

        $model = FunTrackParticipant::find()->where(['user_id' => $user_id, "fun_track_id" => $fun_track_id]);

        if ($fun_track_pack_id) {
            $model->andWhere(['fun_track_pack_id' => $fun_track_pack_id]);
            $model = $model->one();

            if ($model == null) {
                return null;
            }

            $modelPack = $model->funTrackPack;
            if ($modelPack == []) {

            }
            $modelRoute = $modelPack->funTrackRoute;
            if ($modelRoute == []) {

            }

            return [
                "registration_number" => $model->registration_number,
                "qr_code" => $model->qr_code,
                "name" => $model->name,
                "disease_history" => $model->disease_history,
                "payment_attachment" => $model->payment_attachment,
                "is_confirmed" => $model->is_confirmed,
                "start_scan" => $model->start_scan,
                "end_scan" => $model->end_scan,
                "route_name" => $modelRoute->route_name,
                "audio_marker" => $modelRoute->getAudioMarker(true),
                "coordinates" => $modelRoute->getRoutePolyline(true),
            ];

        } else {
            $model = $model->asArray()->one();
            if ($model == []) {
                return null;
            }
        }

        return $model;
    }

    public function actionGetParticipant()
    {
        $fun_track_id = $_GET['fun_track_id'];
        $fun_track_pack_id = $_GET['fun_track_pack_id'];

        $model = FunTrackParticipant::find()->where(['fun_track_id' => $fun_track_id]);
        if ($fun_track_pack_id != null) {
            $model->andWhere(['fun_track_pack_id' => $fun_track_pack_id]);
        }

        return $model->all();
    }

    public function actionScanQrCode()
    {
        $qr_code = $_POST['qr_code'];
        $user_id = $_POST['user_id'];
        $action = $_POST['action'];

        $model = FunTrackParticipant::findOne(['qr_code' => $qr_code]);
        if ($model == []) {
            return [
                "status" => "fail",
                "message" => "QR Code doesnt exist in our system.",
            ];
        }

        if ($action == "start") {
            if ($model->start_scan == null) {
                $model->start_scan = date("Y-m-d H:i:s");
            } else {
                return [
                    "status" => "fail",
                    "message" => "This QR Code already scanned.",
                ];
            }
        } else if ($action == "end") {
            if ($model->end_scan == null) {
                $model->end_scan = date("Y-m-d H:i:s");
            } else {
                return [
                    "status" => "fail",
                    "message" => "This QR Code already scanned.",
                ];
            }
        }

        try {
            $model->save();
            return [
                "status" => "success",
                "message" => "QR Code success scanned.",
            ];
        } catch (\Throwable $th) {
            return [
                "status" => "fail",
                "message" => "Failed when scan QR Code.",
            ];
        }

    }

    public function actionUploadTranscript()
    {
        $fun_track_id = $_POST['fun_track_id'];
        $fun_track_pack_id = $_POST['fun_track_pack_id'];
        $user = $_POST['user_id'];
        $speech_script = $_POST['speech_script'];

        if ($fun_track_id == null || $fun_track_pack_id == null || $user == null) {
            return [
                "success" => false,
                "message" => "Forbidden access",
            ];
        }

        $model = FunTrackParticipant::findOne(['fun_track_id' => $fun_track_id, 'fun_track_pack_id' => $fun_track_pack_id]);

        if ($model == null) {
            return [
                "success" => false,
                "message" => "You're not registered in this event",
            ];
        }

        $event = $model->funTrack;
        $time_now = time();
        $transcript_deadline = strtotime($event->script_deadline);

        if ($time_now > $transcript_deadline) {
            return [
                "success" => false,
                "message" => "has exceeded the time limit for uploading script",
            ];
        }

        $file = UploadedFile::getInstanceByName("speech_script");
        if($file == null){
            return [
                "success" => false,
                "message" => "Speech Script cant be blank",
            ];
        }

        $response = $this->uploadFile($file);
        $model->speech_script = $response['data']['filename'];

        try {
            $model->save();
            return [
                "success" => false,
                "message" => "Success uploaded Transcript",
            ];
        } catch (\Throwable $th) {
            return [
                "success" => false,
                "message" => "Something went wrong",
            ];
        }
    }

    public function actionUploadVideo()
    {
        $fun_track_id = $_POST['fun_track_id'];
        $fun_track_pack_id = $_POST['fun_track_pack_id'];
        $user = $_POST['user_id'];
        $link_video = $_POST['link_speech_video'];

        if ($fun_track_id == null || $fun_track_pack_id == null || $user == null || $link_video == null) {
            return [
                "success" => false,
                "message" => "Forbidden access",
            ];
        }

        $model = FunTrackParticipant::findOne(['fun_track_id' => $fun_track_id, 'fun_track_pack_id' => $fun_track_pack_id]);

        if ($model == null) {
            return [
                "success" => false,
                "message" => "You're not registered in this event",
            ];
        }

        $event = $model->funTrack;
        $time_now = time();
        $transcript_deadline = strtotime($event->file_deadline);

        if ($time_now > $transcript_deadline) {
            return [
                "success" => false,
                "message" => "has exceeded the time limit for uploading video",
            ];
        }

        $model->speech_video = $link_video;

        try {
            $model->save();
            return [
                "success" => true,
                "message" => "Success uploaded link video",
            ];
        } catch (\Throwable $th) {
            return [
                "success" => true,
                "message" => "Something went wrong",
            ];
        }
    }
}
