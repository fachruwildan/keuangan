<?php
namespace App\Components;

class IG
{
    public static function ajaxBz(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://www.instagram.com/ajax/bz',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => 'q=%5B%7B%22user%22%3A%225809234189%22%2C%22page_id%22%3A%22vn9iux%22%2C%22app_id%22%3A%22936619743392459%22%2C%22device_id%22%3A%22326FEED9-40A5-41DE-BB73-349AB77A4395%22%2C%22posts%22%3A%5B%5B%22ods%3Aincr%22%2C%7B%22key%22%3A%22web.mqtt.protocol.onmessagedelivered%22%7D%2C1613870125454%2C0%5D%2C%5B%22ods%3Aincr%22%2C%7B%22key%22%3A%22web.mqtt.publish_success%22%7D%2C1613870125454%2C0%5D%2C%5B%22ods%3Aincr%22%2C%7B%22key%22%3A%22web.mqtt.protocol.onmessagedelivered%22%7D%2C1613870125455%2C0%5D%2C%5B%22ods%3Aincr%22%2C%7B%22key%22%3A%22web.mqtt.publish_success%22%7D%2C1613870125455%2C0%5D%2C%5B%22ods%3Aincr%22%2C%7B%22key%22%3A%22web.mqtt.protocol.onmessagedelivered%22%7D%2C1613870125456%2C0%5D%2C%5B%22ods%3Aincr%22%2C%7B%22key%22%3A%22web.mqtt.publish_success%22%7D%2C1613870125456%2C0%5D%2C%5B%22ods%3Aincr%22%2C%7B%22key%22%3A%22web.mqtt.protocol.onmessagearrived%22%7D%2C1613870125505%2C0%5D%2C%5B%22ods%3Aincr%22%2C%7B%22key%22%3A%22web.mqtt.message_arrived%22%7D%2C1613870125505%2C0%5D%5D%2C%22trigger%22%3A%22ods%3Aincr%22%2C%22send_method%22%3A%22ajax%22%7D%5D&ts=1613870135423',
        CURLOPT_HTTPHEADER => array(
            'DNT: 1',
            'X-IG-WWW-Claim: hmac.AR2AZ8iV3Ooe-isl_WD2w2OVDMFkjOm0T8AdN9l9fjWIIkR3',
            'X-Instagram-AJAX: a57770cf770b',
            'Content-Type: application/x-www-form-urlencoded',
            'Accept: */*',
            'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36',
            'X-Requested-With: XMLHttpRequest',
            'X-CSRFToken: Bz1OxfKfwOwHdE9XkLyU5yGOvO6c9p7k',
            'X-IG-App-ID: 936619743392459',
            'Sec-Fetch-Site: same-origin',
            'Sec-Fetch-Mode: cors',
            'Sec-Fetch-Dest: empty',
            'Cookie: ig_did=AE16C628-B9FF-40C0-BCB9-0E9A4104686E; csrftoken=c1Ge7ePh71gxUSZA60fq04iyj2oQsT8O; mid=YDGoVgALAAHMtnooL95FkT_oYX1v; ig_nrcb=1'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        // echo $response;
    }
    public static function request($url){
        $curl = curl_init();

        $headers = [
            "DNT:1",
            "Upgrade-Insecure-Requests:1",
            "User-Agent:Mozilla/5.0 (Linux; Android 7.0; SM-G930VC Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/58.0.3029.83 Mobile Safari/537.36",
            "Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
            // "Sec-Fetch-Site:none",
            // "Sec-Fetch-Mode:navigate",
            // "Sec-Fetch-User:?1",
            // "Sec-Fetch-Dest:document",
        ];
        
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_AUTOREFERER, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => $headers,
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        IG::ajaxBz();

        return $response;

    }

    public static function get($username, $custom_header = null)
    {
        $url = "https://www.instagram.com/graphql/query/?query_hash=003056d32c2554def87228bc3fd9668a&variables={\"id\":\"40046624277\",\"first\":12}";

        $json_response = IG::request($url);
        // $json_response = explode("window._sharedData = ", $html_response)[1];
        // $json_response = explode(";</script>", $json_response)[0];

        $response = json_decode($json_response);
        $timelines_default = $response
            ->data
            // ->entry_data
            // ->ProfilePage[0]
            // ->graphql
            ->user
            ->edge_owner_to_timeline_media
            ->edges;
        $timelines = [];

        foreach ($timelines_default as $index => $timeline) {
            $timeline = $timeline->node;
            $timelines[$index] = [
                "display_url" => $timeline->display_url,
                "is_video" => $timeline->is_video,
                "captions" => $timeline->edge_media_to_caption->edges[0]->node->text
            ];

            if($timeline->is_video == true){
                $timelines[$index] = (object) array_merge($timelines[$index], [ 
                    "video_url" => $timeline->video_url
                ]);
            }
        }

        return [
            "success" => true,
            "message" => $timelines
        ];
    }

    public static function toObject($data)
    {
        return (object) json_decode($data);
    }
}
