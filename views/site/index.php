<?php

/* @var $this yii\web\View */

use app\models\TransaksiUang;
use app\models\HutangPiutang;
use app\models\User;

$this->title = 'Dashboard';

//$tableData = array_diff($tableData,$stk);
?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>

<div class="site-index">

    <div class="row">
<?php
$month = date('m');
$year = date('Y');
$mm = date('F');
$datenow = date('Y-m-d');

// var_dump($manager[0]["nomor_handphone"]);
// die;
$sum_debit = TransaksiUang::find()
    ->where(['and', ['>=', 'tanggal', "$year-$month-01"], ['<=', 'tanggal', "$datenow"]])
    ->sum('debit');

$sum_kredit = TransaksiUang::find()
    ->where(['and', ['>=', 'tanggal', "$year-$month-01"], ['<=', 'tanggal', "$datenow"]])
    ->sum('kredit');

$sum_piutang = HutangPiutang::find()
// ->where(['and', ['>=', 'created_at', "$year-$month-01"], ['<=', 'created_at', "$datenow"]])
->sum('piutang');
$sum_hutang = HutangPiutang::find()
// ->where(['and', ['>=', 'created_at', "$year-$month-01"], ['<=', 'created_at', "$datenow"]])
->sum('hutang');

$pengurangan = $sum_debit - $sum_kredit;
$pengurangan2 = $sum_piutang - $sum_hutang;
?>
 <div class="box-header with-border">
    </div>
	<div class="box-body">
    <?php if ($sum_debit > $sum_kredit) {?>
    <div class="col-lg-6 col-md-4 col-xs-6">
         <div class="small-box bg-green">
            <div class="inner">
            <h3><?= \app\components\Angka::toReadableHarga($pengurangan); ?></h3>
               <p>Laba</p>
            </div>
            <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="/keuangans/web/site/list-perhitungan" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>


         </div>
      </div>
      <?php } else {?>
      <div class="col-lg-6 col-md-4 col-xs-6">
         <div class="small-box bg-red">
            <div class="inner">
            <h3><?= \app\components\Angka::toReadableHarga($pengurangan); ?></h3>
               <p>Rugi</p>
            </div>
            <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="/keuangans/web/site/list-perhitungan" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>



         </div>
      </div>
      <?php }?>
      <?php if ($sum_piutang > $sum_hutang) {?>
    <div class="col-lg-6 col-md-4 col-xs-6">
         <div class="small-box bg-green">
            <div class="inner">
            <h3><?= \app\components\Angka::toReadableHarga($pengurangan2); ?></h3>
               <p>Piutang</p>
            </div>
            <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="/keuangans/web/hutang-piutang" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>


         </div>
      </div>
      <?php } else {?>
      <div class="col-lg-6 col-md-4 col-xs-6">
         <div class="small-box bg-red">
            <div class="inner">
            <h3><?= \app\components\Angka::toReadableHarga($pengurangan2); ?></h3>
               <p>Hutang</p>
            </div>
            <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="/keuangans/web/hutang-piutang" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>



         </div>
      </div>
      <?php }?>
      <div class="col-lg-6 col-md-4 col-xs-6">
         <div class="small-box bg-blue">
            <div class="inner">
               <h3>Laporan Jurnal</h3>
               <p>Jurnal</p>
            </div>
            <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="/keuangans/web/site/jurnal" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>



         </div>
	  </div>
      <div class="col-lg-6 col-md-4 col-xs-6">
         <div class="small-box bg-blue">
            <div class="inner">
               <h3>Laporan Neraca</h3>
               <p>Neraca</p>
            </div>
            <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="/keuangans/web/site/neraca" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
         </div>
	  </div>
      <div class="col-lg-6 col-md-4 col-xs-6">
         <div class="small-box bg-blue">
            <div class="inner">
               <h3>Laporan Arus Kas</h3>
               <p>Arus Kas</p>
            </div>
            <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="/keuangans/web/site/arus-kas" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
         </div>
	  </div>
     <div class="col-lg-6 col-md-4 col-xs-6">
         <div class="small-box bg-blue">
            <div class="inner">
               <h3>Laporan Proyeksi</h3>
               <p>Proyeksi</p>
            </div>
            <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="/keuangans/web/site/proyeksi" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
         </div>
	  </div>
     <div class="col-lg-6 col-md-4 col-xs-6">
         <div class="small-box bg-blue">
            <div class="inner">
               <h3>Laporan Pemasukkan</h3>
               <p>Pemasukkan</p>
            </div>
            <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="/keuangans/web/site/pemasukkan" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
         </div>
	  </div>
     <div class="col-lg-6 col-md-4 col-xs-6">
         <div class="small-box bg-blue">
            <div class="inner">
               <h3>Laporan Pengeluaran</h3>
               <p>Pengeluaran</p>
            </div>
            <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="/keuangans/web/site/pengeluaran" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
         </div>
	  </div>
</div>



    <div class='row'>
    <div class='col-md-12'>
    <?= $this->render('_area_chart.php') ?>
    </div>
    <?php /* ?>
<div class='col-md-6'>
</div>
<?= $this->render('_chart_pelatihan_bulan.php') ?>
<div class='col-md-12'>
<?= $this->render('_chart_transaksi_keluar.php') ?>
</div>
<?php */ ?>
<?php /* 
<div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Donut Chart</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <canvas id="pieChart" style="height:250px"></canvas>
            </div>
            <!-- /.box-body -->
          </div> */ ?>
<?php ?>
    </div>
</div>

<?php
$dateY=date('Y');
$query="SELECT b.nama as keterangan,a.debit AS total FROM `transaksi_uang` `a` 
LEFT JOIN `kelompok_rekening` `b` ON a.id_kelompok_rekening=b.id
WHERE (`b`.`kelompok`='Debit') AND (SUBSTRING(a.tanggal,1,4)='$dateY')";
$res= Yii::$app->db->createCommand($query)->queryAll();
$this->registerJs("
var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
   var pieChart       = new Chart(pieChartCanvas)
   var PieData        = [
     {
       value    : 700,
       color    : '#f56954',
       highlight: '#f56954',
       label    : 'Chrome'
     },
     {
       value    : 500,
       color    : '#00a65a',
       highlight: '#00a65a',
       label    : 'IE'
     },
     {
       value    : 400,
       color    : '#f39c12',
       highlight: '#f39c12',
       label    : 'FireFox'
     },
     {
       value    : 600,
       color    : '#00c0ef',
       highlight: '#00c0ef',
       label    : 'Safari'
     },
     {
       value    : 300,
       color    : '#3c8dbc',
       highlight: '#3c8dbc',
       label    : 'Opera'
     },
     {
       value    : 100,
       color    : '#d2d6de',
       highlight: '#d2d6de',
       label    : 'Navigator'
     }
   ]
   var pieOptions     = {
     //Boolean - Whether we should show a stroke on each segment
     segmentShowStroke    : true,
     //String - The colour of each segment stroke
     segmentStrokeColor   : '#fff',
     //Number - The width of each segment stroke
     segmentStrokeWidth   : 2,
     //Number - The percentage of the chart that we cut out of the middle
     percentageInnerCutout: 50, // This is 0 for Pie charts
     //Number - Amount of animation steps
     animationSteps       : 100,
     //String - Animation easing effect
     animationEasing      : 'easeOutBounce',
     //Boolean - Whether we animate the rotation of the Doughnut
     animateRotate        : true,
     //Boolean - Whether we animate scaling the Doughnut from the centre
     animateScale         : false,
     //Boolean - whether to make the chart responsive to window resizing
     responsive           : true,
     // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
     maintainAspectRatio  : true,
     //String - A legend template
   }
   //Create pie or douhnut chart
   // You can switch between pie and douhnut using the method below.
   pieChart.Doughnut(PieData, pieOptions)
");