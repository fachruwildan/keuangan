<?php 
use yii\helpers\Html;
$ttd_manager=(new \yii\db\Query())
->select(['tanda_tangan','name'])
->from('user')
->Where(['role_id'=>'5'])
->all();
$ttd_direktur=(new \yii\db\Query())
->select(['tanda_tangan','name'])
->from('user')
->Where(['role_id'=>'4'])
->all();
?>
<table>
         <thead>
            <tr>
               <th width="15%"></th>
               <th width="25%" style="padding-left: 5px;
                 padding-bottom: 3px;">
                <strong style="font-size: 13px;"><?="Manager"?></strong>
                </th>
               <th></th>
               <th width="15%"></th>
               <th width="25%" style="padding-left: 5px;
                 padding-bottom: 3px;">
                <strong style="font-size: 13px;"><?="Direktur"?></strong>
                </th>
               <th></th>
            </tr>
            <tr class>
                    <td></td>
                    <td><?= Html::img(["uploads/".$ttd_manager[0]["tanda_tangan"]], ["width"=>"150px"]); ?></td>
                    <td></td>
                    <td></td>
                    <td><?= Html::img(["uploads/".$ttd_direktur[0]["tanda_tangan"]], ["width"=>"150px"]); ?></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td style="padding-left: 5px; padding-bottom: 3px;">
                    <strong style="font-size: 10px;"><?= $ttd_manager[0]['name'] ?></strong>
                    </td>
                    <td></td>
                    <td></td>
                    <td style="padding-left: 5px; padding-bottom: 3px;">
                    <strong style="font-size: 10px;"><?= $ttd_direktur[0]['name'] ?></strong>
                    </td>
                    <td></td>
                </tr>
                <h1></h1>
         </thead>
         <tbody>
         </tbody>
         </table>