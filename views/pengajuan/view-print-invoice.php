<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;
use mdm\admin\components\Helper;
use app\models\Invoice;
use app\models\Pengajuan;
/* @var $this yii\web\View */
/* @var $model common\models\Absensi */

$title = $model->tujuan;
$title = explode('Pengajuan Dengan Tujuan ', $title);

if (count($title) > 4) {
    $title = implode(' ', array_slice($title, 0, 3)) . "...";
} else {
    $title = implode(' ', $title);
}

$this->title = $title;
$this->params['breadcrumbs'][] = ['label' => 'Pengajuan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$sum_total = Invoice::find()
    ->where(['id_pengajuan'=>$model->id])
    ->sum('total');
?>
<div class="absensi-view">

<table class="table table table-striped table-bordered">
         <thead>
            <tr>
               <th width="15%"></th>
               <th width="20%"></th>
               <th style="padding-left: 5px; padding-bottom: 3px;">
                    <!-- <strong style="font-size: 20px;">AMONG TANI FOUNDATION</strong> -->
               </th>
            </tr>
            <tr>
                <td colspan = 2><?= Html::img(["uploads/kop.jpg"], ["width"=>"220px"]); ?></td>
                <td></td>
                <td style="padding-left: 5px; padding-bottom: 3px; font-size: 12px;">
                <br>
                <strong style="font-size: 20px;">AMONG TANI FOUNDATION</strong><br>
                    JL.HASANUDIN NO.22 KOTA BATU,65313 <br>
                    Phone : (0341 3061627) Email : amongtanifound@gmail.com
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td  style="padding-left: 5px; padding-bottom: 3px; font-size: 9px;">
                   <!-- Phone : (0341 3061627) Email : amongtanifound@gmail.com -->
                </td>
            </tr>
         </thead>
         <tbody>
         </tbody>
         </table>
         =====================================================================================
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                <p class='pull-left'> 
                </div>
                <div class="box-body">
                    <div class="pt-3">
                        <?php
                        if ($providerInvoice->totalCount) {
                            $gridColumnInvoice = [
                                ['class' => 'yii\grid\SerialColumn'],
                                ['attribute' => 'id', 'visible' => false],
                                [
                                    'attribute' => 'invoice.keterangan',
                                    'label' => 'Description Invoice',
                                    'value' => function($model) {
                                        return $model->keterangan;
                                    }
                                ],
                                [
                                    'attribute' => 'invoice.price',
                                    'label' => 'Price',
                                    'format'=>'raw',
                                    'value' => function($model) {
                                        return \app\components\Angka::toReadableHarga($model->price);
                                    }
                                ],
                                [
                                    'attribute' => 'invoice.quantity',
                                    'label' => 'Quantity Invoice',
                                    'value' => function($model) {
                                        return $model->quantity;
                                    }
                                ],
                                [
                                    'attribute' => 'invoice.total',
                                    'label' => 'Total Price Invoice',
                                    'footer' => "IDR  " . number_format($sum_total, 2, ",", "."),
                                    'format' =>'raw',
                                    'value' => function($model) {
                                        return \app\components\Angka::toReadableHarga($model->total);
                                    }
                                ],
                            ];
                            echo Gridview::widget([
                                'dataProvider' => $providerInvoice,
                                'pjax' => true,
                                'showFooter' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-invoice']],
                                'panel' => [
                                    //'type' => GridView::TYPE_PRIMARY,
                                    'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Invoice'),
                                ],
                                'export' => false,
                                'columns' => $gridColumnInvoice,
                            ]);
                        }
                        ?>
                        <!-- <style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
  font-size: 24px;
}
</style> -->
<table>
         <thead>
            <tr>
               <th width="15%"></th>
               <th width="15%" style="padding-left: 5px;
                 padding-bottom: 3px;">
                <strong style="font-size: 20px;"><?="Pembuat"?></strong>
                </th>
               <th></th>
               <th width="15%"></th>
               <th width="25%" style="padding-left: 5px;
                 padding-bottom: 3px;">
                <strong style="font-size: 20px;"><?="Yang Menyetujui"?></strong>
                </th>
               <th></th>
            </tr>
            <?php   if($model->user->tanda_tangan != null || $model->persetujuan->tanda_tangan != null ){?> 
            <tr class>
                    <td></td>
                    <td><?= Html::img(["uploads/".$model->user->tanda_tangan], ["width"=>"150px"]); ?></td>
                    <td></td>
                    <td></td>
                    <td><?= Html::img(["uploads/".$model->persetujuan->tanda_tangan], ["width"=>"150px"]); ?></td>
                    <td></td>
                </tr>
                <?php } ?>
                <tr>
                    <td></td>
                    <td style="padding-left: 5px; padding-bottom: 3px;">
                    <strong style="font-size: 20px;"><?= $model->user->name ?></strong>
                    </td>
                    <td></td>
                    <td></td>
                    <td style="padding-left: 5px; padding-bottom: 3px;">
                    <strong style="font-size: 20px;"><?= $model->persetujuan->name ?></strong>
                    </td>
                    <td></td>
                </tr>
         </thead>
         <tbody>
         </tbody>
         </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>