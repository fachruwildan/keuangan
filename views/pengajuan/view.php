<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;
use mdm\admin\components\Helper;
use app\models\Invoice;

/* @var $this yii\web\View */
/* @var $model common\models\Absensi */

$title = $model->tujuan;
$title = explode('Pengajuan Dengan Tujuan ', $title);

if (count($title) > 4) {
    $title = implode(' ', array_slice($title, 0, 3)) . "...";
} else {
    $title = implode(' ', $title);
}

$this->title = $title;
$this->params['breadcrumbs'][] = ['label' => 'Pengajuan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$sum_total = Invoice::find()
    ->where(['id_pengajuan'=>$model->id])
    ->sum('total');
?>
<div class="absensi-view">

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                <p class='pull-left'>
                <?php if($model->status == 1){ ?>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> ' . 'Edit', ['update', 'id' => $model->id],['class' => 'btn btn-info']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-trash"></span> ' . 'Delete', ['delete', 'id' => $model->id],
            [
                'class' => 'btn btn-danger',
                'data-confirm' => '' . 'Are you sure to delete this item?' . '',
                'data-method' => 'post',
                ]); ?>
                <?php } ?>
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> ' . 'Tambah Baru', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <p class="pull-right">
        <?= Html::a('<span class="glyphicon glyphicon-list"></span> ' . 'Daftar Pengajuan', ['index'], ['class'=>'btn btn-default']) ?>
    </p>
                </div>
                <div class="box-body">
                    <?php
                    $gridColumn = [
                        ['attribute' => 'id', 'visible' => false],
                        
                        ['attribute' => 'no_pengajuan'],
                        [
                            'attribute'=> 'id_user',
                            'value'=>function($model){
                                return $model->user->name;
                            }
                        ],
                                'tujuan',
                                [
                                    'attribute' => 'id_perusahaan',
                                    'value'=>function($model){
                                        return $model->perusahaan->nama_perusahaan;
                                    }
                                ],
                                [
                                    'attribute' => 'status',
                                    'filter'=>false,
                                    'value' => function($model){
                                        if($model->status == 2){
                                            return ($model->status == 2) ? "Disetujui oleh Finance" : "Belum Disetujui";
                        
                                        }elseif($model->status == 0){
                                            return ($model->status == 0) ? "Ditolak" : "Belum Disetujui";
                                        }elseif($model->status == 3){
                                            return ($model->status == 3) ? "Disetujui Oleh Direktur" : "Belum Disetujui";
                                        }elseif($model->status == 4){
                                            return ($model->status == 4) ? "Disetujui Oleh Direktur Utama" : "Belum Disetujui";
                                        }else{
                                            return ($model->status == 1) ? "Belum Disetujui" : "Belum Disetujui";
                                        }
                                    }
                                ],
                                [
                                    'attribute'=>'tanggal',
                                    'value'=>$hari,
                                ],
                                [
                                    'attribute' => 'note',
                                    'format' => 'html',
                                    'value' => function($model) {
                                        if($model->note !=null){
                                            return "<span style='font-family: Dejavu Sans, monospace'>" 
                                            . Yii::$app->formatter->asNtext($model->note) . '</span>';
                                        }else{
                                            return '';
                                        }
                                       
                                    }
                                ],
                    ];
                    echo DetailView::widget([
                        'model' => $model,
                        'attributes' => $gridColumn,
                    ]);
                    ?>
                    <br>
                    <div class="pt-3">
                        <?php
                        if ($providerInvoice->totalCount) {
                            $gridColumnInvoice = [
                                ['class' => 'yii\grid\SerialColumn',
                                'header' => 'No',
                            ],
                                ['attribute' => 'id', 'visible' => false],
                                
                                [
                                    'attribute' => 'invoice.keterangan',
                                    'label' => 'Item',
                                    'value' => function($model) {
                                        return $model->keterangan;
                                    }
                                ],
                                [
                                    'attribute' => 'invoice.price',
                                    'label' => 'Price',
                                    'format'=>'raw',
                                    'value' => function($model) {
                                        return \app\components\Angka::toReadableHarga($model->price);
                                    }
                                ],
                                [
                                    'attribute' => 'invoice.quantity',
                                    'label' => 'Quantity',
                                    'value' => function($model) {
                                        return $model->quantity . " " . $model->keterangan_barang;
                                    }
                                ],
                                [
                                    'attribute' => 'invoice.total',
                                    'label' => 'Total Price ',
                                    'footer' => "IDR  " . number_format($sum_total, 2, ",", "."),
                                    'format' =>'raw',
                                    'value' => function($model) {
                                        return \app\components\Angka::toReadableHarga($model->total);
                                    }
                                ],
                            ];
                            echo Gridview::widget([
                                'dataProvider' => $providerInvoice,
                                // 'pjax' => true,
                                'showFooter' => true,
                                // 'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-invoice']],
                                // 'panel' => [
                                //     //'type' => GridView::TYPE_PRIMARY,
                                //     'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Invoice'),
                                // ],
                                // 'export' => false,
                                'columns' => $gridColumnInvoice,
                            ]);
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>