<?php

use wbraganca\dynamicform\DynamicFormWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use kartik\date\DatePicker;
/**
 * @var yii\web\View $this
 * @var app\models\Pengajuan $model
 * @var yii\widgets\ActiveForm $form
 */

?>

<div class="box box-info">
    <div class="box-body">
        <?php $form = ActiveForm::begin([
    'id' => 'dynamic-form',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-error',
]
);
?>

			<?=$form->field($model, 'tujuan')->textInput(['maxlength' => true])?>
            <?=$form->field($model, 'tanggal')->widget(DatePicker::classname(), [
    'options' => ['placeholder' => 'Pilih Tanggal Pengajuan'],
    'readonly' => true,
    'pluginOptions' => [
        'format' => 'yyyy-m-d',
        'autocomplete' => "off",
        'autoclose'=>true,
    ],
]);?>
<?= $form->field($model, 'id_perusahaan')->widget(\kartik\select2\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Perusahaan::find()->where(['nama_perusahaan' => 'Batu Tracking 19'])->asArray()->all(), 'id_perusahaan', 'nama_perusahaan'),
        'options' => ['placeholder' => 'Pilih Perusahaan'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
			<?= $form->field($model, 'note')->textarea(['rows' => 6]) ?> 
			<!-- ?= $form->field($model, 'status')->textInput() ?>        <hr/> -->
        <?php echo $form->errorSummary($model); ?>
        <div class="row">
        <div class="panel panel-default">
        <!-- <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i> Addresses</h4></div> -->
        <div class="panel-body">
             <?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
    'widgetBody' => '.container-items', // required: css class selector
    'widgetItem' => '.item', // required: css class
    'limit' => 20, // the maximum times, an element can be cloned (default 999)
    'min' => 1, // 0 or 1 (default 1)
    'insertButton' => '.add-item', // css class
    'deleteButton' => '.remove-item', // css class
    'model' => $modelsInvoices[0],
    'formId' => 'dynamic-form',
    'formFields' => [
        'keterangan',
        'quantity',
        'price',
    ],
]);?>

            <div class="container-items"><!-- widgetContainer -->
            <?php foreach ($modelsInvoices as $i => $modelsInvoice): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left"></h3>
                        <div class="pull-right">
                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?php
// necessary for update action.
if (!$modelsInvoice->isNewRecord) {
    echo Html::activeHiddenInput($modelsInvoice, "[{$i}]id");
}
?>
 <?php /*
                        <?=$form->field($modelsInvoice, "[{$i}]keterangan")->textInput(['maxlength' => true])?>
                        <div class="row">
                            <div class="col-sm-6">
                                <?=$form->field($modelsInvoice, "[{$i}]quantity")->textInput(['maxlength' => true])?>
                            </div>
                            <div class="col-sm-6">
                                <?=$form->field($modelsInvoice, "[{$i}]price")->textInput(['maxlength' => true])?>
                            </div>
                        </div><!-- .row -->
                        */ ?>     
<div class="row">
<div class="col-sm-6">
<?= $form->field($modelsInvoice, "[{$i}]keterangan")->textarea(['rows' => 6]) ?>
</div>
<div class="col-sm-6">
<?= $form->field($modelsInvoice, "[{$i}]keterangan_barang")->textarea(['rows' => 6]) ?>
</div>
<div class="col-sm-4">
<?= $form->field($modelsInvoice, "[{$i}]quantity")->textInput(['type' => 'number']) ?>
</div>
<div class="col-sm-4">
<?= $form->field($modelsInvoice, "[{$i}]price")->widget(\kartik\money\MaskMoney::classname(), [
     'value' => 1000,
     'pluginOptions' => [
         'prefix' => 'Rp ',
         'precision' => 0
    ]
]); ?>
</div>
</div><!-- .row -->
                    </div>
                </div>
            <?php endforeach;?>
            </div>
            <?php DynamicFormWidget::end();?>
        </div>
        </div>
        <div class="row">
            <div class="col-md-offset-3 col-md-10">
                <?=Html::submitButton('<i class="fa fa-save"></i> Simpan', ['class' => 'btn btn-success']);?>
                <?=Html::a('<i class="fa fa-chevron-left"></i> Kembali', ['index'], ['class' => 'btn btn-default'])?>
            </div>
        </div>

        <?php ActiveForm::end();?>

    </div>
</div>