<?php

namespace app\controllers\api;

/**
 * This is the class for REST controller "UserController".
 */

use app\components\Constant;
use app\components\EMAIL;
use app\components\WA;
use app\models\OTP;
use app\models\User;
use Dompdf\Exception;
use Yii;
use yii\web\UploadedFile;

class UserController extends \yii\rest\ActiveController
{
    public $modelClass = 'app\models\User';

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        return $actions;
    }
    public function actionLogin()
    {
        $result = [];
        try {
            $user = User::findOne([
                "username" => $_POST['username'],
                "password" => md5($_POST['password']),
            ]);
            if (isset($user)) {
                // $user->fcm_token = $_POST['fcm_token'];
                $user->save();

                $result['success'] = 1;
                $result['message'] = "success";
                // unset($user->fcm_token);
                unset($user->password); // remove password from response
                $result["result"] = [$user];
            } else {
                $result["success"] = 0;
                $result["message"] = "gagal";
                $result["result"] = "data kosong";
            }
        } catch (\Exception $e) {
            $result["success"] = 0;
            $result["message"] = "gagal";
            $result["result"] = "something-wrong";
        }
        return $result;
    }

    public function actionUpdateUser($id_user)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = User::find()->where(['id' => $id_user])->one();
        //$data->password="";
        if ($data != null) {
            $oldMd5Password = $data->password;
            $oldPhotoUrl = $data->photo_url;
            $pass = $_POST['password'];
            if ($pass == "") {
                $_POST['password'] = $oldMd5Password;
            }
            //return $oldMd5Password;
            if ($pass != "") {
                $_POST['password'] = md5($pass);
                //$data->password = md5($_POST['password']);
            }
            $image = UploadedFile::getInstanceByName('photo_url');
            if ($image != null) {
                # store the source file name
                $data->photo_url = $image->name;
                $arr = explode(".", $image->name);
                $extension = end($arr);

                # generate a unique file name
                $data->photo_url = \Yii::$app->security->generateRandomString() . ".{$extension}";

                # the path to save file
                $path = \Yii::getAlias("@app/web/uploads/") . $data->photo_url;
                $image->saveAs($path);
            } else {
                $data->photo_url = $oldPhotoUrl;
            }
            $data->attributes = \yii::$app->request->post();

            if ($data->validate()) {
                $data->update();
                return array('status' => true, 'message' => 'success', 'data' => [$data]);
            } else {
                return array('status' => false, 'message' => 'gagal', 'data' => $data->getErrors());
            }
        } else {
            return array('status' => false, 'message' => 'gagal', 'data' => "Data Kosong");
        }

    }

    public function actionGetUser($id)
    {
        $result = [];
        $data = User::find()->select('id,username,name,photo_url,role_id,last_login,last_logout')->where(['id' => $id])->one();

        if ($data != null) {
            $result['status'] = true;
            $result['message'] = "success";
            $result['data'] = [$data];
        } else {
            $result['status'] = false;
            $result['message'] = "gagal";
            $result['data'] = "Data Kosong";
        }
        return $result;
    }

    public function actionRegister()
    {
        date_default_timezone_set('Asia/Jakarta');
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $val = \yii::$app->request->post();
        $user = new \app\models\User();
        $user->name = $val['name'];
        $user->username = $val['username'];
        $user->address = $val['address'];
        $user->no_hp = ($val['no_hp']) ?? '';
        $user->email = ($val['email']) ?? '';
        $user->password = md5($val['password']);
        $user->photo_url = 'default.png';
        $user->role_id = 3;


        if($user->no_hp == ''){
            return array('status' => false, 'message' => 'gagal', 'data' => 'No Telp tidak boleh kosong');
        }
        
        if($user->email == ''){
            return array('status' => false, 'message' => 'gagal', 'data' => 'Email tidak boleh kosong');
        }
        
        if(strlen($val['password']) < 6){
            return array('status' => false, 'message' => 'gagal', 'data' => 'Password minimal 6 karakter');
        }
        
        if(filter_var($user->email, FILTER_VALIDATE_EMAIL) == false){
            return array('status' => false, 'message' => 'gagal', 'data' => 'Email anda tidak valid');
        }
        
        $check = User::findOne(['no_hp' => $user->no_hp]);
        if($check != null){
            return array('status' => false, 'message' => 'gagal', 'data' => 'No Telp telah digunakan');
        }
        
        $check = User::findOne(['email' => $user->email]);
        if($check){
            return array('status' => false, 'message' => 'gagal', 'data' => 'Email telah digunakan');
        }

        // check username
        if ($user->username) {
            $cek = \app\models\User::find()->where(['username' => $user->username])->asArray()->one();
            if (isset($cek)) {
                return array('status' => false, 'message' => 'gagal', 'data' => 'Username telah digunakan');
            }
        }

        if ($user->validate()) {
            $user->save();
            return array('status' => true, 'message' => 'success', 'result' => [$user]);
        } else {
            return array('status' => false, 'message' => 'gagal', 'data' => $user->getErrors());
        }
    }

    public function actionRequestForgotPassword()
    {
        $data = $_POST['data_reminder'];

        if (filter_var($data, FILTER_VALIDATE_EMAIL)) {
            return [
                "success" => false,
                "message" => "Mohon maaf service email sedang dalam perbaikan",
            ];
            $user = User::findOne(['email' => $data]);
            if ($user == []) {
                return [
                    "success" => false,
                    "message" => "data not found",
                ];
            }

            $otp = OTP::findOne(['user_id' => $user->id, 'is_used' => 0]);
            if($otp == null){
                $otp = new OTP();
            }

            $otp->user_id = $user->id;
            $otp->otp_code = (string)random_int(100000, 999999);
            $otp->created_at = date('Y-m-d H:i:s');
            $otp->save();

            $content = "
            Hay,
            <br>ini adalah kode OTP untuk reset password anda.
            <br>
            <br>{$otp->otp_code}
            <br>
            <br>Jangan bagikan kode ini dengan siapapun.
            ";
            // EMAIL::send($user->email, $content);
            return [
                "success" => true,
                "message" => "OTP send to your email",
            ];
        } else if ($this->validate_phone_number($data)) {
            $user = User::findOne(['no_hp' => $data]);
            if ($user == []) {
                return [
                    "success" => false,
                    "message" => "data not found",
                ];
            }

            $otp = OTP::findOne(['user_id' => $user->id, 'is_used' => 0]);
            if($otp == null){
                $otp = new OTP();
            }

            $otp->user_id = $user->id;
            $otp->otp_code = (string)random_int(100000, 999999);
            $otp->created_at = date('Y-m-d H:i:s');
            $otp->save();

            $no_hp = Constant::purifyPhone($user->no_hp);
            if ($no_hp) {
                WA::send($no_hp, "Hay, \n ini adalah kode OTP untuk reset password anda.\n\n{$otp->otp_code}\n\nJangan bagikan kode ini dengan siapapun.");
            }

            return [
                "success" => true,
                "message" => "OTP send to your whatsapp",
            ];
        } else {
            return [
                "success" => false,
                "message" => "data isnt valid reminder",
            ];
        }
    }

    public function actionCheckOtp(){
        $otp_code = $_POST['otp_code'];

        $otp = OTP::findOne(['otp_code' => $otp_code, 'is_used' => 0]);
        if($otp){
            $now = time();
            $valid_time = strtotime($otp->created_at) + (60*60*60);

            if($now < $valid_time){
                $otp->token = Yii::$app->security->generateRandomString(50);
                $otp->is_used = 1;
                $otp->save();

                // $user = $otp->user;
                // $new_password = Yii::$app->security->generateRandomString(10);
                // $user->password = md5($new_password);
                // $user->save();

                // if(filter_var($user->email, FILTER_VALIDATE_EMAIL)){
                //     Email::send($user->email, "
                //     Hallo {$user->name},<br><br>
                //     Dibawah ini adalah password yang dapat anda gunakan  untuk login ke akun anda.<br><br>
                //     {$new_password}<br><br>
                //     ");
                // }

                // WA::send(Constant::purifyPhone($user->no_hp), "
                // Hallo {$user->name},\n\n
                // Dibawah ini adalah password yang dapat anda gunakan  untuk login ke akun anda.\n\n
                // {$new_password}\n\n
                // ");
                
                return [
                    "success" => true,
                    "message" => "Otp Valid",
                    "data" => [
                        "token" => $otp->token
                    ]
                ];
            }

        }

        return [
            "success" => false,
            "message" => "OTP yang anda masukan tidak valid"
        ];
    }

    public function actionPasswordConfirm(){
        $token = $_POST['token'];
        $new_password = $_POST['password'];
        $new_password_confirm = $_POST['password_confirm'];

        if($new_password != $new_password_confirm){
            return [
                "success" => false,
                "message" => "Password doesnt match"
            ];
        }

        $otp = OTP::findOne(['is_used' => 1, 'token' => $token, 'is_token_used' => 0]);

        if($otp == []){
            return [
                "success" => false,
                "message" => "otp tidak ditemukan"
            ];
        }
        
        $otp->is_token_used = 1;
        $user = $otp->user;
        $user->password = md5($new_password);
        

        try{
            $otp->save();
            $user->save();
            $result = [];
            // unset($user->fcm_token);
            // unset($user->password); // remove password from response
            $result["success"] = true;
            $result["message"] = "Password changed";
            $result["result"] = [[
                "username" => $user->username,
                "name" => $user->name,
                "role_id" => $user->role_id,
                "id" => $user->id,
                "photo_url" => $user->photo_url,
            ]];
            return $result;
        }catch(\Throwable $th){
            return [
                "success" => false,
                "message" => "Telah terjadi kesalahan "
            ];
        }
        
    }

    public function validate_phone_number($phone)
    {
        // Allow +, - and . in phone number
        $filtered_phone_number = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);
        // Remove "-" from number
        $phone_to_check = str_replace("-", "", $filtered_phone_number);
        // Check the lenght of number
        // This can be customized if you want phone number from a specific country
        if (strlen($phone_to_check) < 10 || strlen($phone_to_check) > 14) {
            return false;
        } else {
            return true;
        }
    }

    public function actionGetAllFcmToken(){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $fcm_token = User::find()->where(['or', 
            ["!=", "fcm_token", ""],
            ["!=", "fcm_token", null],
        ])->select(['fcm_token'])->all();
        
        $list_token = [];
        foreach($fcm_token as $token){
            $list_token[] = $token->fcm_token;
        }

        return [
            "success" => true,
            "data" => $list_token
        ];
    }
}
