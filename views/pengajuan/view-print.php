<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;
use mdm\admin\components\Helper;
use app\models\Invoice;
use app\models\Pengajuan;
/* @var $this yii\web\View */
/* @var $model common\models\Absensi */

$title = $model->tujuan;
$title = explode('Pengajuan Dengan Tujuan ', $title);

if (count($title) > 4) {
    $title = implode(' ', array_slice($title, 0, 3)) . "...";
} else {
    $title = implode(' ', $title);
}

$sum_total = Invoice::find()
    ->where(['id_pengajuan'=>$model->id])
    ->sum('total');
$pajak=$sum_total*10/100;
$sum_bayar=$sum_total + $pajak; 
$details = (new \yii\db\Query())
    ->select(['a.keterangan as ket','a.quantity as qty','a.keterangan_barang as unit','a.total as price'])
    ->from('invoice a')
    ->leftJoin('pengajuan b', 'a.id_pengajuan=b.id')
    ->Where(['b.id'=>$model->id])
    ->all();
$pengajuans=Pengajuan::find()->where(['id'=>$model->id])->all();

$this->title = $title;
$this->params['breadcrumbs'][] = ['label' => 'Pengajuan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$sum_total = Invoice::find()
    ->where(['id_pengajuan'=>$model->id])
    ->sum('total');
?>
<div class="absensi-view">
         <br>
         <br>
         <br>
         <h3 style="text-align:center">Pengajuan Dana</h3>
         <table>
         <thead>
         <tr>
         <th style=" solid black;text-align: center;">No</th>
        <td style=" solid black;text-align: center;">: <?= $model->no_pengajuan ?></td>
        </tr>
        <tr>
         <th style=" solid black;text-align: center;">Hari / Tanggal</th>
         <td style=" solid black;text-align: center;">: <?= $hari ?></td>
        </tr>
        <tr>
         <th style=" solid black;text-align: center;">Nama</th>
         <td style=" solid black;text-align: center;">: <?= $model->user->name ?></td>
        </tr>
        <tr>
         <th style=" solid black;text-align: center;">Jabatan</th>
         <td style=" solid black;text-align: center;">: <?= $model->user->role->name ?></td>
        </tr>
        <tr>
         <th style=" solid black;text-align: center;">Lampiran</th>
         <td style=" solid black;text-align: center;">: -</td>
        </tr>
         
        
         </thead>
         <tbody>
         <tr>
         </tr>
         </tbody>
         </table>
         <br>
        
         <table style=" border: 1px solid black;">
         <thead>
         <tr style=" border: 1px solid black;">
         <th style=" border: 1px solid black;text-align: center;">No</th>
         <th style=" border: 1px solid black;text-align: center;">Item</th>
         <th style=" border: 1px solid black;text-align: center;">Qty</th>
         <th style=" border: 1px solid black;text-align: center;">Unit</th>
         <th style=" border: 1px solid black;text-align: center;">Price</th>
         </tr>
         <?php
            // var_dump($aktivas_uang);
            // die;
            $i=0;foreach($details as $detail){ $i++ ?>
                <tr>
                    <td style=" border: 1px solid black;text-align: center;">
                        <?= $i ?>
                    </td>
                    <td style=" border: 1px solid black;text-align: center;">
                        <?= $detail['ket']  ?>
                    </td>
                    <td style=" border: 1px solid black;text-align: center;">
                        <?= $detail['qty']  ?>
                    </td>
                    <td style=" border: 1px solid black;text-align: center;">
                        <?= $detail['unit']  ?>
                    </td>
                    <td style=" border: 1px solid black;text-align: center;">
                        <?= "Rp  " . number_format($detail['price'], 2, ",", ".") ?>
                    </td>
                </tr>
            <?php } ?>
         </thead>
         <tbody>
         <tr>
         <td colspan=4 style="text-align: center; border: 1px solid black;">Total </td>
         <!-- <td>SubTotal</td> -->
         <td style="border: 1px solid black;text-align: center;">  <?=  " Rp  " . number_format($sum_total, 2, ",", ".")?>  </td>
         </tr>
         <tr>
         <?php $a=0;foreach($pengajuans as $pengajuan){ $a++ ?>
         <td colspan=5 style="border: 1px solid black;">Alasan Kebutuhan : <br>
         <?= $a.".". $pengajuan->tujuan ?>
          </td>
          <?php } ?>
         </tr>
         <tr>
         <td colspan=5 style="border: 1px solid black;">
         
         Catatan: <br>  
         <?= Yii::$app->formatter->asNtext($model->note) ?>
         </td>
         </tr>
         </tbody>
         </table>
         <br> 
                        <!-- <style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
  font-size: 24px;
}
</style> -->
<table>
         <thead>
            <tr>
               <th width="15%"></th>
               <th width="25%" style="padding-left: 50px;
                 padding-bottom: 3px;">
                <strong style="font-size: 13px;">Dibuat Oleh</strong>
                </th>
               <th></th>
               <th width="15%"></th>
               <th width="25%" style="padding-left: 50px;
                 padding-bottom: 3px; ">
                <strong style="font-size: 13px;">Mengetahui</strong>
                </th>
               <th></th>
            </tr>
            <tr class>
                    <td></td>
                    <td style="text-align: center;"><?= Html::img(["uploads/".$ttd_pembuat[0]["tanda_tangan"]], ["width"=>"100px","padding-top:-20px"]); ?><br>
                    <strong style="font-size: 13px;">( <?= $ttd_pembuat[0]['name'] ?> )<br><?= $ttd_pembuat[0]['role'] ?></strong>
                    </td>
                    <td></td>
                    <td></td>
                    <td style="text-align: center;">
                    <?= Html::img(["uploads/".$ttd_finance[0]['tanda_tangan']], ["width"=>"100px"]); ?><br>
                    <strong style="font-size: 13px;"> <br> ( <?= $ttd_finance[0]['name'] ?> )<br><?= $ttd_finance[0]['role'] ?></strong>
                    </td>
                    <td></td>
                </tr>
                <!-- <tr>
                    <td></td>
                    <td style="padding-left: 15px; padding-bottom: 3px; position: absolute;text-align: center;">
                    </td>
                    <td></td>
                    <td></td>
                    <td style="padding-left: 5px; padding-bottom: 3px; position: absolute; padding-top:-50px;text-align: center;">
                    </td>
                    <td></td>
                </tr> -->
                <h1></h1>
                <tr>
               <th width="15%"></th>
               <th width="25%" style="padding-left: 5px;
                 padding-bottom: 3px;">
                <strong style="font-size: 13px;"></strong>
                </th>
               <th></th>
               <th width="15%" style="padding-left: -5px;"> Menyetujui </th>
               <th width="25%" style="padding-left: 5px;
                 padding-bottom: 3px;">
                <strong style="font-size: 13px;"></strong>
                </th>
               <th></th>
            </tr>
            <tr class>
                    <td></td>
                    <td style="text-align: center;">
                    <?= Html::img(["uploads/".$ttd_direktur[0]["tanda_tangan"]], ["width"=>"125px"]); ?><br>
                    <strong style="font-size: 13px;"><br> ( <?= $ttd_direktur[0]['name'] ?> )<br><?= $ttd_direktur[0]['role'] ?> </strong>
                    </td>
                    <td></td>
                    <td></td>
                    <td style="text-align: center;"><?= Html::img(["uploads/".$ttd_direktur_utama[0]['tanda_tangan']], ["width"=>"125"]); ?><br>
                    <strong style="font-size: 13px;"> <br> ( <?= $ttd_direktur_utama[0]['name'] ?> )<br><?= $ttd_direktur_utama[0]['role'] ?></strong>
                    </td>
                    <td></td>
                </tr>
                <!-- <tr>
                    <td></td>
                    <td style="padding-left: 5px; padding-bottom: 3px;  position: absolute;padding-top:-35px; text-align: center;">
                    </td>
                    <td></td>
                    <td></td>
                    <td style="padding-left: 5px; padding-bottom: 3px; position: absolute; padding-top:-50px; text-align: center; ">
                    </td>
                    <td></td>
                </tr> -->
         </thead>
         <tbody>
         </tbody>
         </table>                    </div>
                </div>
            </div>
        </div>
    </div>

</div>