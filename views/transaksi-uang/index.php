<?php

/* @var $this yii\web\View */
/* @var $searchModel common\models\AbsensiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Transaksi';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
    $('.search-form').toggle(1000);
    return false;
});";
$this->registerJs($search);
?>
<div class="absensi-index">

    <div class="row">
        <div class="col-md-12">
                <div class="box-header">
                    <p>
                        <?=Html::a('Create Transaksi', ['create'], ['class' => 'btn btn-success'])?>
                        <?= Html::submitButton('Delete Selection', [
                            'id' => "btn-delete",
                            'class' => 'btn btn-danger',
                            "title"=>"Hapus Data",
                            // "data-confirm" => "Apakah Anda yakin data ini sudah benar ?",
                        ]); ?>
                    </p>
                    <div class="search-form" style="display:none">
                        <?=$this->render('_search', ['model' => $searchModel]);?>
                    </div>
                </div>
                <div class="box-body">
                <?=Html::beginForm(['/transaksi-uang/check'], 'post', [
                    'id' => 'form-delete'
                ]);?>
 
     <div class="box box-info">
        <div class="box-body">
            <div class="table-responsive">
                <?=GridView::widget([
    'layout' => '{summary}{pager}{items}{pager}',
    'dataProvider' => $dataProvider,
    'pager' => [
        'class' => yii\widgets\LinkPager::className(),
        'firstPageLabel' => 'First',
        'lastPageLabel' => 'Last'],
    'filterModel' => $searchModel,
    'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
    'headerRowOptions' => ['class' => 'x'],
    'columns' => [

        [
            'class' => 'yii\grid\CheckboxColumn',
            // you may configure additional properties here
        ],
        ['class' => 'yii\grid\SerialColumn'],
        \app\components\ActionButton::getButtons(),
        [
            'attribute' => 'tanggal',
            'filter' => false,
        ],
        [
            'attribute'=>'id_user',
            'filter'=>false,
            'label'=>'Pembuat',
            'value'=> function ($model){
                return $model->user->name;
            }
    
        ],
        [
            'attribute' => 'id_jenis_transaksi',
            'filter' =>false,
            'value' => function ($model) {
                return $model->jenisTransaksi->nama;
    
            },
        ],
        [
            'attribute' => 'id_perusahaan',
            'filter' =>false,
            'value' => function ($model) {
                return $model->perusahaan->nama_perusahaan;
    
            },
        ],
        [
            'attribute' => 'id_kelompok_rekening',
            'filter' =>false,
            'value' => function ($model) {
                return $model->kelompokRekening->kelompok;
    
            },
        ],
        [
            'attribute' => 'keterangan',
            'filter' => false,
        ],
        [
            'attribute' => 'debit',
            'label' => 'Debit',
            'format' => 'raw',
            'filter' => false,
            'value' =>function ($model) {
    
                return \app\components\Angka::toReadableHarga($model->debit);
        
            },
        ],
        [
            'attribute' => 'kredit',
            'label' => 'Kredit',
            'format' => 'raw',
            'filter' => false,
            'value' =>function ($model) {
                return \app\components\Angka::toReadableHarga($model->kredit);
        
            },
        ],
    ],
]);?>
            </div>
        </div>
    </div>
<?= Html::endForm() ?>
            </div>
        </div>
    </div>

</div>

<?php

$this->registerJs("
    let btnDelete = document.querySelector('#btn-delete');
    let formDelete = document.querySelector('#form-delete');

    btnDelete.addEventListener('click', () => {

        if(confirm('Yakin ingin menghapus data ?')){
            formDelete.submit();
        }
    });
");