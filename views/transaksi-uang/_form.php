<?php

use kartik\money\MaskMoney;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use kartik\depdrop\DepDrop;
/**
 * @var yii\web\View $this
 * @var app\models\Debit $model
 * @var yii\widgets\ActiveForm $form
 */
$kelompok = $model->isNewRecord 
? null 
: $model->id_kelompok_rekening;
?>

<div class="box box-info">
    <div class="box-body">
        <?php $form = ActiveForm::begin([
    'id' => 'Debit',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-error',
]
);
?>
<?= $form->field($model, 'id_jenis_transaksi')->widget(\kartik\select2\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\JenisTransaksi::find()->asArray()->all(), 'id', 'nama'),
        'options' => ['placeholder' => 'Pilih Jenis Transaksi'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
     <?= $form->field($model, 'pilihan')->widget(\kartik\select2\Select2::className(),[
            'data' => \yii\helpers\ArrayHelper::map(
                \app\models\KelompokRekening::find()
                    ->groupBy('kelompok')->select('kelompok')->all(), 
                'kelompok', 
                'kelompok'),
            'value' => $kelompok,
            'options' => ['id' => 'kelompok', 'placeholder' => 'Pilih Debit / Kredit'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
     <?= $form->field($model, 'id_kelompok_rekening')->widget(\kartik\depdrop\DepDrop::classname(), [
        'type' => \kartik\depdrop\DepDrop::TYPE_SELECT2,
        'select2Options' => ['pluginOptions' => ['allowClear' => true]],
        'options'=>['id'=>'id_kelompok_rekening'],
        'pluginOptions'=>[
            'depends'=>['kelompok'],
            'initialize' => true,
            'placeholder'=>'Pilih Kelompok Rekening...',
            'url'=>\yii\helpers\Url::to(['/transaksi-uang/kelompok'])
        ]
    ]); ?>
    <?= $form->field($model, 'id_perusahaan')->widget(\kartik\select2\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Perusahaan::find()->asArray()->all(), 'id_perusahaan', 'nama_perusahaan'),
        'options' => ['placeholder' => 'Pilih Perusahaan'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
        <?=$form->field($model, 'nominal')->widget(\kartik\money\MaskMoney::classname(), [
     'value' => 1000,
     'pluginOptions' => [
         'prefix' => 'Rp ',
         'precision' => 0
    ]
]); ?>
			<?=$form->field($model, 'keterangan')->textarea(['rows' => 6])?>        <hr/>
        <?php echo $form->errorSummary($model); ?>
        <div class="row">
            <div class="col-md-offset-3 col-md-10">
                <?=Html::submitButton('<i class="fa fa-save"></i> Simpan', ['class' => 'btn btn-success']);?>
                <?=Html::a('<i class="fa fa-chevron-left"></i> Kembali', ['index'], ['class' => 'btn btn-default'])?>
            </div>
        </div>

        <?php ActiveForm::end();?>

    </div>
</div>
<?php 
$Script = <<< JS

$('#pilihan').change(function(){
    var kelompok = $(this).val();
    $.get('../../web/kelompok-rekening/get-kelompok',{ kelompok : kelompok},function(data){
         var data = $.parseJSON(data);
            $('#transaksiuang-id_kelompok_rekening').attr('value',data.kelompok);
        });
    });

JS;
$this->registerJS($Script);
?> 