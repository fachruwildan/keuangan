<?php 
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;
use mdm\admin\components\Helper;
use app\models\DetailOrder;
$sum_total = DetailOrder::find()
->where(['id_order'=>$model->id])
->sum('total');
    
?>      
</style>
<div class="form-group">
</div>
   <div class="col-md-12">
   
   <table>
         </table><br>
        <strong><h1 style="text-align:center;margin-top:110px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KWITANSI &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= date('d M Y') ?></h1></strong>
         <table>
         <thead>
            <tr>
               <th width="80%"> No  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?= $model->order_no ?>/ATK/PO/III/<?= date('Y') ?></th>
               <th width="20%"></th>
               <th style="padding-left: 5px; padding-bottom: 3px;">
                    <!-- <strong style="font-size: 20px;">AMONG TANI FOUNDATION</strong> -->
                    
               </th>
            </tr>
            <tr>
                <td><strong>Telah Terima Dari&nbsp;&nbsp; : <?= $model->ditagihkanOleh->nama_perusahaan ?></strong></td>
                <td></td>
                <td>
                </td>
            </tr>
            <tr>
                <td><strong>Uang Sebesar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</strong> <i><?=  Nasution\Terbilang::convert($sum_total)." Rupiah" ?></i></td>
                <td></td>
                <td>
                </td>
            </tr>
            <tr>
                <td><strong>Untuk Pembayaran :</strong> <?= $model->tujuan ?></td>
                <td></td>
                <td>
                </td>
            </tr>
            <tr>
                <td><i>Rp. <?= number_format($sum_total, 2, ",", ".") ?></i></td>
                <td></td>
                <td>
                </td>
            </tr>
         </thead>
         <tbody>
         </tbody>
         </table>
        <table style="margin-bottom:100px">
         <thead>
           
         </thead>
         <tbody>
          <tr>
               <th width="15%"></th>
               <th width="15%" style="padding-left: 5px;
                 padding-bottom: 3px;">
                <strong style="font-size: 20px;"></strong>
                </th>
               <th></th>
               <th width="15%"></th>
               <th width="25%" style="padding-left: 15px; text-align:center;
                 padding-bottom: 3px;">
                <strong style="font-size: 15px;"><?="Dibuat Oleh"?></strong>
                </th>
               <th></th>
            </tr>
                <tr class>
                    <td></td>
                    <td style="padding-left: 5px; padding-bottom: 3px;">
                    <strong style="font-size: 20px;"></strong>
                    </td>
                    <td></td>
                    <td></td>
                    <td style="padding-left: 5px; padding-bottom: 3px; text-align:center;" >
                    <?= Html::img(["uploads/".$ttd_pembuat[0]["tanda_tangan"]], ["width"=>"100px","padding-top:-20px"]); ?><br>
                    <strong style="font-size: 13px;">( <?= $ttd_pembuat[0]['name'] ?> )<br><?= $ttd_pembuat[0]['role'] ?></strong>
                    <!-- <br>
                    <br>
                    <br>
                    <br>
                    <strong style="font-size: 13px;"> <br> 
                    (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; )
                    <br>Marketing</strong> -->
                    
                    </td>
                    <td></td>
                </tr>
         </tbody>
         </table>


         <!-- <table style="margin-top:400px;">
         <thead>
         <tr>
         <th>Contact Person</th>
         <th></th>
         <th></th>
         </tr>
         </thead>
         <tbody>
         <tr>
         <td>&nbsp; Vita &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(082132022323)</td>
         <td></td>
         <td></td>
         </tr>
         <tr>
         <td>&nbsp; Shabila (081252679774)</td>
         <td></td>
         <td></td>
         </tr>
         </tbody>
         </table> -->
      
   </div>
</div>
