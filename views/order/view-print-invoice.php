<?php 
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;
use mdm\admin\components\Helper;
use app\models\DetailOrder;
$sum_total = DetailOrder::find()
    ->where(['id_order'=>$model->id])
    ->sum('total');
$pajak=$sum_total*10/100;
$sum_bayar=$sum_total + $pajak; 
$detail = (new \yii\db\Query())
    ->select(['a.deskripsi_barang as deskripsi_barang','a.quantity as qty','a.keterangan as ket','a.price as price','a.total as total','a.diskon as diskon'])
    ->from('detail_order a')
    ->leftJoin('order b', 'a.id_order=b.id')
    ->Where(['b.id'=>$model->id])
    ->all();
  
    
?>      
</style>
<div class="form-group">
</div>
   <div class="col-md-12">
   <table>
         <thead>
            <tr>
               <th width="15%"></th>
               <th width="20%"></th>
               <th style="padding-left: 5px; padding-bottom: 3px;">
                    <!-- <strong style="font-size: 20px;">AMONG TANI FOUNDATION</strong> -->
               </th>
            </tr>
            <tr>
                <td colspan = 2></td>
                <td></td>
                <td style="padding-left: 5px; padding-bottom: 3px; font-size: 15px;">
                <br>
                <!-- <strong style="font-size: 23px;">Batu Tracking - 19</strong><br>
                    JL.Hasanudin No.22 Kota Batu,65313 <br>
                    batutracking19@gmail.com -->
                </td>
                <td style="text-align: right ">
                <strong style="font-size:15px;">INVOICE</strong><br>
                =========<br>
                Date: <?= date('d F Y') ?> <br>
                No Invoice: <?=  $model->order_no ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td  style="padding-left: 5px; padding-bottom: 3px; font-size: 9px;">
                   <!-- Phone : (0341 3061627) Email : amongtanifound@gmail.com -->
                </td>
            </tr>
         </thead>
         <tbody>
         </tbody>
         </table><br>
         <table>
         <thead>
            <tr>
               <th width="40%" colspan = 2>BILL TO</th>
               <th width="20%"></th>
               <th style="padding-left: 5px; padding-bottom: 3px;">
                    <!-- <strong style="font-size: 20px;">AMONG TANI FOUNDATION</strong> -->
                    
               </th>
            </tr>
            <tr>
                <td><?= $model->cust_name ?></td>
                <td></td>
                <td>
                </td>
            </tr>
            <tr>
                <td><?= $model->ditagihkan_kepada ?></td>
                <td></td>
                <td>
                </td>
            </tr>
            <tr>
                <td><?= $model->address ?></td>
                <td></td>
                <td>
                </td>
            </tr>
         </thead>
         <tbody>
         </tbody>
         </table>
         <h1></h1>

         <table style=" border: 1px solid black;">
         <thead>
         <tr style=" border: 1px solid black;">
         <th style=" border: 1px solid black;text-align: center;">Qty</th>
         <th style=" border: 1px solid black;text-align: center;">Description</th>
         <th style=" border: 1px solid black;text-align: center;">Unit Price</th>
         <th style=" border: 1px solid black;text-align: center; width:15%">Disc</th>
         <th style=" border: 1px solid black;text-align: center;">Amount</th>
         </tr>
         <?php
            // var_dump($aktivas_uang);
            // die;
            $len = count($detail); 
            for($i=0;$i < $len; $i++){ ?>
                <tr>
                    <td style=" border: 1px solid black; text-align: center;">
                        <?= $detail[$i]['qty']." ".$detail[$i]['ket']  ?>
                    </td>
                    <td style=" border: 1px solid black; text-align: center;">
                        <?= $detail[$i]['deskripsi_barang']  ?>
                    </td>
                    <td style=" border: 1px solid black; text-align: center;">
                        <?= "Rp  " . number_format($detail[$i]['price'], 2, ",", ".") ?>
                    </td>
                    <td style=" border: 1px solid black;">
                    <?php
                    $diskon=$detail[$i]['diskon'] * 100;
                    if($diskon == 0){
                        echo "";
                    }else{
                        echo $diskon."%";
                    }
                    ?>
                    </td>
                    <td style=" border: 1px solid black; text-align: center;">
                        <?= "Rp  " . number_format($detail[$i]['total'], 2, ",", ".") ?>
                    </td>
                </tr>
            <?php } ?>
         </thead>
         <tbody>
         <tr>
         <td colspan=4 style="text-align: right;">SubTotal </td>
         <!-- <td>SubTotal</td> -->
         <td style="border: 1px solid black;text-align: center;">  <?=  " Rp  " . number_format($sum_total, 2, ",", ".")?>  </td>
         </tr>
         <tr>
         <td colspan=3><i>
        <?php
          $uang= Nasution\Terbilang::convert($sum_total)." Rupiah";
          echo ucwords($uang);
        ?></i></td>
         <td style="text-align: right;">Invoice Total </td>
         <!-- <td>SubTotal</td> -->
         <td style="border: 1px solid black;text-align: center;">  <?=  " Rp  " . number_format($sum_total, 2, ",", ".")?>  </td>
         </tr>
         </tbody>
         </table>
         <br>
         <h5 style="padding-top:-20px; margin-left:50px;">Pembayaran via Transfer/Cek/Giro ke rekening <br>
         Atas nama : Among Tani Kemilau <br>
         <strong>BNI NO: 1149055363</strong></h5>
         <h5 style="margin-left:50px;">If you have any questions concerning this invoice, use the following contact information: <br> 
                  <strong>
                  &nbsp;&nbsp;&nbsp;Vita &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(082132022323) <br>
                  &nbsp;&nbsp;&nbsp;Shabila (081252679774)
                  </strong>
         </h5>
         <br>
         <h3 style="margin-left:50px;">THANK YOU FOR YOUR BUSINESS!</h3>
         
         <br>
         <table>
         <thead>
            <tr>
               <th width="15%"></th>
               <th width="25%" style="padding-left: 50px;
                 padding-bottom: 3px;">
                <strong style="font-size: 13px;"></strong>
                </th>
               <th></th>
               <th width="15%"></th>
               <th width="25%" style="padding-left: 50px;
                 padding-bottom: 3px; ">
                <strong style="font-size: 13px;">Dibuat Oleh</strong>
                </th>
               <th></th>
            </tr>
            <tr class>
                    <td></td>
                    <td style="text-align: center;">
                    
                    </td>
                    <td></td>
                    <td></td>
                    <td style="text-align: center;">
                    <?= Html::img(["uploads/".$ttd_pembuat[0]["tanda_tangan"]], ["width"=>"100px","padding-top:-20px"]); ?><br>
                    <strong style="font-size: 13px;">( <?= $ttd_pembuat[0]['name'] ?> )<br><?= $ttd_pembuat[0]['role'] ?></strong>
                    <!-- <br>
                    <br>
                    <br>
                    <br>
                    <strong style="font-size: 13px;"> <br> 
                    (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; )
                    <br>Marketing</strong> -->
                    </td>
                    <td></td>
                </tr>
                <!-- <tr>
                    <td></td>
                    <td style="padding-left: 15px; padding-bottom: 3px; position: absolute;text-align: center;">
                    </td>
                    <td></td>
                    <td></td>
                    <td style="padding-left: 5px; padding-bottom: 3px; position: absolute; padding-top:-50px;text-align: center;">
                    </td>
                    <td></td>
                </tr> -->
                <h1></h1>
                <tr>
               <th width="15%"></th>
               <th width="25%" style="padding-left: 5px;
                 padding-bottom: 3px;">
                <strong style="font-size: 13px;"></strong>
                </th>
               <th></th>
               <th width="15%" style="padding-left: -40px;">  </th>
               <th width="25%" style="padding-left: 5px;
                 padding-bottom: 3px;">
                <strong style="font-size: 13px;"></strong>
                </th>
               <th></th>
            </tr>
            <tr class>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="text-align: center; padding-left: -100px; "><br>
                    <!-- <br>
                    <br>
                    <br>
                    <br>
                    <strong style="font-size: 13px;">
                    (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; )
                    <br>Client</strong> -->
                    </td>
                    <td></td>
                </tr>
                <!-- <tr>
                    <td></td>
                    <td style="padding-left: 5px; padding-bottom: 3px;  position: absolute;padding-top:-35px; text-align: center;">
                    </td>
                    <td></td>
                    <td></td>
                    <td style="padding-left: 5px; padding-bottom: 3px; position: absolute; padding-top:-50px; text-align: center; ">
                    </td>
                    <td></td>
                </tr> -->
         </thead>
         <tbody>
         </tbody>
         </table>


         <!-- <table style="margin-top:400px;">
         <thead>
         <tr>
         <th>Contact Person</th>
         <th></th>
         <th></th>
         </tr>
         </thead>
         <tbody>
         <tr>
         <td>&nbsp; Vita &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(082132022323)</td>
         <td></td>
         <td></td>
         </tr>
         <tr>
         <td>&nbsp; Shabila (081252679774)</td>
         <td></td>
         <td></td>
         </tr>
         </tbody>
         </table> -->
      
   </div>
</div>
