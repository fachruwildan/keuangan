<?php

use wbraganca\dynamicform\DynamicFormWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use kartik\date\DatePicker;
/**
 * @var yii\web\View $this
 * @var app\models\Order $model
 * @var yii\widgets\ActiveForm $form
 */

?>

<div class="box box-info">
    <div class="box-body">
        <?php $form = ActiveForm::begin([
    'id' => 'dynamic-form',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-error',
]
);
?>
<?= $form->field($model, 'ditagihkan_kepada')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'ditagihkan_oleh')->widget(\kartik\select2\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Perusahaan::find()->asArray()->all(), 'id_perusahaan', 'nama_perusahaan'),
        'options' => ['placeholder' => 'Pilih Perusahaan'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
			<?=$form->field($model, 'cust_name')->textInput(['maxlength' => true])?>
            <?=$form->field($model, 'address')->textarea(['rows' => 6]) ?>
            <?=$form->field($model, 'tujuan')->textarea(['rows' => 6]) ?>

			<!-- ?= $form->field($model, 'status')->textInput() ?>        <hr/> -->
        <?php echo $form->errorSummary($model); ?>
        <div class="row">
        <div class="panel panel-default">
        <!-- <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i> Addresses</h4></div> -->
        <div class="panel-body">
             <?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
    'widgetBody' => '.container-items', // required: css class selector
    'widgetItem' => '.item', // required: css class
    'limit' => 4, // the maximum times, an element can be cloned (default 999)
    'min' => 1, // 0 or 1 (default 1)
    'insertButton' => '.add-item', // css class
    'deleteButton' => '.remove-item', // css class
    'model' => $modelsDetailOrders[0],
    'formId' => 'dynamic-form',
    'formFields' => [
        'deskripsi_barang',
        'keterangan',
        'quantity',
        'price',
    ],
]);?>

            <div class="container-items"><!-- widgetContainer -->
            <?php foreach ($modelsDetailOrders as $i => $modelsDetailOrder): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">Detail Order</h3>
                        <div class="pull-right">
                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?php
// necessary for update action.
if (!$modelsDetailOrder->isNewRecord) {
    echo Html::activeHiddenInput($modelsDetailOrder, "[{$i}]id");
}
?>
 <?php /*
                        <?=$form->field($modelsDetailOrder, "[{$i}]keterangan")->textInput(['maxlength' => true])?>
                        <div class="row">
                            <div class="col-sm-6">
                                <?=$form->field($modelsDetailOrder, "[{$i}]quantity")->textInput(['maxlength' => true])?>
                            </div>
                            <div class="col-sm-6">
                                <?=$form->field($modelsDetailOrder, "[{$i}]price")->textInput(['maxlength' => true])?>
                            </div>
                        </div><!-- .row -->
                        */ ?>     
<div class="row">
<div class="col-sm-6">
<?= $form->field($modelsDetailOrder, "[{$i}]deskripsi_barang")->textarea(['rows' => 6]) ?>
</div>
<div class="col-sm-6">
<?= $form->field($modelsDetailOrder, "[{$i}]keterangan")->dropDownList(['Paket'=> 'Paket','Unit'=> 'Unit','Kg' =>'Kg',
    'Gram' =>'Gram'],['prompt'=>' ']) ?>
</div>
<div class="col-sm-6">
<?= $form->field($modelsDetailOrder, "[{$i}]quantity")->textInput(['type' => 'number']) ?>
</div>
<div class="col-sm-6">
<?= $form->field($modelsDetailOrder, "[{$i}]price")->widget(\kartik\money\MaskMoney::classname(), [
     'value' => 1000,
     'pluginOptions' => [
         'prefix' => 'Rp ',
         'precision' => 0
    ]
]); ?>

</div>
<div class="col-sm-6">
<?= $form->field($modelsDetailOrder, "[{$i}]diskon")->dropDownList(['0'=> '0%','0.1'=> '10%','0.15' =>'15%',
    '0.2' =>'20%','0.25' =>'25%','0.30' =>'30%','0.5' =>'50%','0.75' =>'75%'],['prompt'=>' ']) ?>
</div>
</div><!-- .row -->
                    </div>
                </div>
            <?php endforeach;?>
            </div>
            <?php DynamicFormWidget::end();?>
        </div>
        </div>
        <div class="row">
            <div class="col-md-offset-3 col-md-10">
                <?=Html::submitButton('<i class="fa fa-save"></i> Simpan', ['class' => 'btn btn-success']);?>
                <?=Html::a('<i class="fa fa-chevron-left"></i> Kembali', ['index'], ['class' => 'btn btn-default'])?>
            </div>
        </div>

        <?php ActiveForm::end();?>

    </div>
</div>