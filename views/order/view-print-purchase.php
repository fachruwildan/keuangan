<?php 
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;
use mdm\admin\components\Helper;
use app\models\DetailOrder;
$sum_total = DetailOrder::find()
    ->where(['id_order'=>$model->id])
    ->sum('total');
$pajak=$sum_total*10/100;
$sum_bayar=$sum_total + $pajak; 
$detail = (new \yii\db\Query())
    ->select(['a.deskripsi_barang as deskripsi_barang','a.quantity as qty','a.keterangan as ket','a.price as price','a.total as total','a.diskon as diskon'])
    ->from('detail_order a')
    ->leftJoin('order b', 'a.id_order=b.id')
    ->Where(['b.id'=>$model->id])
    ->all();
  
    
?>      
</style>
<div class="form-group">
</div>
   <div class="col-md-12">
   <table>
         <thead>
            <tr>
               <th width="15%"></th>
               <th width="20%"></th>
               <th style="padding-left: 5px; padding-bottom: 3px;">
                    <!-- <strong style="font-size: 20px;">AMONG TANI FOUNDATION</strong> -->
               </th>
            </tr>
            <tr>
                <td colspan = 2></td>
                <td></td>
                <td style="padding-left: 5px; padding-bottom: 3px; font-size: 15px;">
                <br>
                <strong style="font-size: 23px;">
                <b><u> PURCHASE ORDER</u></b>
                </strong>
                <br>
                <strong style="font-size: 15px;">
                <b>No : <?= $model->order_no ?>/ATK/PO/III/<?= date('Y') ?></b>
                </strong>
                </td>
                <td style="text-align: center ">
                <!-- <strong style="font-size:15px;">PURCHASE ORDER</strong><br>
                =========<br>
                Date: <?= date('d F Y') ?> <br>
                No Invoice: <?=  $model->order_no ?> -->
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td  style="padding-left: 5px; padding-bottom: 3px; font-size: 9px;">
                   <!-- Phone : (0341 3061627) Email : amongtanifound@gmail.com -->
                </td>
            </tr>
         </thead>
         <tbody>
         </tbody>
         </table><br>
         <table>
         <thead>
            <tr>
               <th width="40%" colspan = 2>Kepada Yth:</th>
               <th width="20%"></th>
               <th style="padding-left: 5px; padding-bottom: 3px;">
                    <!-- <strong style="font-size: 20px;">AMONG TANI FOUNDATION</strong> -->
                    
               </th>
            </tr>
            <tr>
                <td><?= $model->cust_name ?><br>Up. <?= $model->ditagihkan_kepada ?><br><?=$model->address ?></td>
                <td></td>
                <td>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>
                </td>
            </tr>
         </thead>
         <tbody>
         </tbody>
         </table>
         <h1></h1>
         <p>Dengan Hormat. <br>
         Dengan ini kami mengirimkan daftar pesanan kami untuk <?=$model->cust_name ?>, sebagai berikut:</p>
         <table style=" border: 1px solid black;">
         <thead>
         <tr style=" border: 1px solid black;">
         <th style=" border: 1px solid black;text-align: center; width:30%">Deskripsi Barang</th>
         <th style=" border: 1px solid black;text-align: center; width:10%">Qty</th>
         <th style=" border: 1px solid black;text-align: center; width:10%">Ket</th>
         <th style=" border: 1px solid black;text-align: center;width:15%">Nominal</th>
         </tr>
         <?php
            // var_dump($aktivas_uang);
            // die;
            $len = count($detail); 
            for($i=0;$i < $len; $i++){ ?>
                <tr>
                    <td style=" border: 1px solid black; text-align: center;">
                        <?= $detail[$i]['deskripsi_barang']  ?>
                    </td>
                    <td style=" border: 1px solid black; text-align: center;">
                        <?= $detail[$i]['qty']  ?>
                    </td>
                    <td style=" border: 1px solid black; text-align: center;">
                        <?= $detail[$i]['ket'] ?>
                    </td>
                    <td style=" border: 1px solid black; text-align: center;">
                        <?= "Rp  " . number_format($detail[$i]['total'], 2, ",", ".") ?>
                    </td>
                </tr>
            <?php } ?>
         </thead>
         <tbody>
         
         </tbody>
         </table>
         <br>
         <h5 style="padding-top:-20px; margin-left:30px; font-size:17px">Demikian PO dari kami apabila ada hal yang sekiranya masih belum jelas, bisa <br>
menghubungi bagian Marketing :
 <br> 
                  <strong>
                  Shabila (081252679774)
                  </strong>
         </h5>
         <br>
         <h5 style="padding-top:-5px; margin-left:30px; font-size:17px">Demikian Atas perhatian dan kerjasamanya kami ucapkan terimakasih
         </h5>
         <h1></h1>
         <br>
         <table>
         <thead>
            <tr>
               <th width="5%"></th>
               <th width="25%" style="
                 padding-bottom: 3px;">
                 Menyetujui Oleh,
                </th>
               <th style="padding-top:-50px;">Hormat kami, </th>
               <th style="padding-right: -10px; text-align:right">Dibuat Oleh,</th>
               
               <th></th>
            </tr>
            <tr class>
                    <td></td>
                    <td style="text-align: center; padding-left:-65px">
                    <br> 
                     <br>
                     <br>
                     <br>
                    (<?= $model->ditagihkan_kepada ?> )
                    </td>
                    <td></td>
                    <td></td>
                    <td style="text-align: left;padding-left:-125px">
                     
                    <br>
                    <br>
                    <br>
                    <br> 
                    <p>
                    (Shabila Gadis Halida)
                    </p>
                    
                    </td>
                </tr>
                <!-- <tr>
                    <td></td>
                    <td style="padding-left: 15px; padding-bottom: 3px; position: absolute;text-align: center;">
                    </td>
                    <td></td>
                    <td></td>
                    <td style="padding-left: 5px; padding-bottom: 3px; position: absolute; padding-top:-50px;text-align: center;">
                    </td>
                    <td></td>
                </tr> -->
                <h1></h1>
                <tr>
               <th width="15%"></th>
               <th width="25%" style="padding-left: 5px;
                 padding-bottom: 3px;">
                <strong style="font-size: 13px;"></strong>
                </th>
               <th></th>
               <th width="15%" style="padding-left: -40px;">  </th>
               <th width="25%" style="padding-left: 5px;
                 padding-bottom: 3px;">
                <strong style="font-size: 13px;"></strong>
                </th>
               <th></th>
            </tr>
            <tr class>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="text-align: center; padding-left: -100px; "><br>
                    <!-- <br>
                    <br>
                    <br>
                    <br>
                    <strong style="font-size: 13px;">
                    (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; )
                    <br>Client</strong> -->
                    </td>
                    <td></td>
                </tr>
                <!-- <tr>
                    <td></td>
                    <td style="padding-left: 5px; padding-bottom: 3px;  position: absolute;padding-top:-35px; text-align: center;">
                    </td>
                    <td></td>
                    <td></td>
                    <td style="padding-left: 5px; padding-bottom: 3px; position: absolute; padding-top:-50px; text-align: center; ">
                    </td>
                    <td></td>
                </tr> -->
         </thead>
         <tbody>
         </tbody>
         </table>


         <!-- <table style="margin-top:400px;">
         <thead>
         <tr>
         <th>Contact Person</th>
         <th></th>
         <th></th>
         </tr>
         </thead>
         <tbody>
         <tr>
         <td>&nbsp; Vita &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(082132022323)</td>
         <td></td>
         <td></td>
         </tr>
         <tr>
         <td>&nbsp; Shabila (081252679774)</td>
         <td></td>
         <td></td>
         </tr>
         </tbody>
         </table> -->
      
   </div>
</div>
