<?php 
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;
use mdm\admin\components\Helper;
use app\models\DetailOrder;
$sum_total = DetailOrder::find()
    ->where(['id_order'=>$model->id])
    ->sum('total');
$detail = (new \yii\db\Query())
    ->select(['a.deskripsi_barang as deskripsi_barang','a.quantity as qty','a.keterangan as ket'])
    ->from('detail_order a')
    ->leftJoin('order b', 'a.id_order=b.id')
    ->Where(['b.id'=>$model->id])
    ->all();
    
?>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h4 style="text-align: center;">PURCHASE ORDER</h4>
<p><strong>&nbsp;</strong></p>
<p><strong>&nbsp;</strong></p>
<p>Kepada Yth:</p>
<p><?= $model->ditagihkan_kepada?></p>
<p>Di</p>
<p>Tempat</p>
<p>&nbsp;</p>

<p>Dengan Hormat.</p>
<p>Dengan ini kami mengirimkan daftar pesanan kami untuk <?=$model->tujuan ?>, sebagai berikut:</p>
<p>&nbsp;</p>
<?php
 if ($providerOrder->totalCount) {
    $gridColumnInvoice = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'detailorder.deskripsi_barang',
            'label' => 'Deskripsi Barang',
            'value' => function($model) {
                return $model->deskripsi_barang;
            }
        ],
        [
            'attribute' => 'detailorder.price',
            'label' => 'Price',
            'format'=>'raw',
            'value' => function($model) {
                return \app\components\Angka::toReadableHarga($model->price);
            }
        ],
        [
            'attribute' => 'detailorder.quantity',
            'label' => 'Qty',
            'value' => function($model) {
                return $model->quantity;
            }
        ],
        [
            'attribute' => 'detailorder.keterangan',
            'label' => 'Keterangan',
            'value' => function($model) {
                return $model->keterangan;
            }
        ],
        [
            'attribute' => 'detailorder.total',
            'label' => 'Total Price ',
            'footer' => "IDR  " . number_format($sum_total, 2, ",", "."),
            'format' =>'raw',
            'value' => function($model) {
                return \app\components\Angka::toReadableHarga($model->total);
            }
        ],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerOrder,
        'pjax' => true,
        'showFooter' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-invoice']],
        'panel' => [
            //'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Invoice'),
        ],
        'export' => false,
        'columns' => $gridColumnInvoice,
    ]);
}
?>
<p style="text-align: center;">&nbsp;</p>
<p>Demikian PO dari kami apabila ada hal yang sekiranya masih belum jelas, bisa menghubungi sdri Admin Bagian Purchase Order</p>
<p>Atas perhatian dan kerjasamanya yang baik kami ucapkan terimakasih</p>

<p style="text-align: right;">Hormat kami,</p>
<p style="text-align: right;">&nbsp;</p>
<p style="text-align: right;"><?= Html::img(["uploads/".$model->issuedBy->tanda_tangan], ["width"=>"150px"]); ?></p>
<p style="text-align: right;">&nbsp;</p>
<p style="text-align: right;">&nbsp;</p>
<p style="text-align: right;">(<?=$model->issuedBy->name ?>)</p>