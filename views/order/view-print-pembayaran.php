<?php 
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;
use mdm\admin\components\Helper;
use app\models\DetailOrder;
$sum_total = DetailOrder::find()
    ->where(['id_order'=>$model->id])
    ->sum('total');
$pajak=$sum_total*10/100;
$sum_bayar=$sum_total + $pajak; 
$detail = (new \yii\db\Query())
    ->select(['a.deskripsi_barang as deskripsi_barang','a.quantity as qty','a.keterangan as ket'])
    ->from('detail_order a')
    ->leftJoin('order b', 'a.id_order=b.id')
    ->Where(['b.id'=>$model->id])
    ->all();
    
?>
<p style="text-align: right;"><strong></strong><?= Html::img(["uploads/bt19.png"], ["width"=>"100px"]); ?> </p>
<p style="text-align: left;"><strong>Bukti Pembelian(RECEIPT)</strong></p>
<p style="text-align: left;">Tanggal : <?= date('d F Y , H:i:sa , l')  ?></p>
<!-- <p style="text-align: left;">Tanggal Order : <?= $model->tanggal_order ?></p> -->
<table style="margin-left: auto; margin-right: auto;">
<tbody>
<tr>
<!-- <td width="140">&nbsp;NOMOR INVOICE: </td>
<td width="215"><b><?= $model->order_no ?></b>&nbsp;</td>
<td width="26">TANGGAL PENAGIHAN</td>
<td width="215">&nbsp;</td>
<td width="21">BATAS AKHIR PEMBAYARAN</td>
<td width="214">&nbsp;</td> -->
</tr>

</tbody>
</table>
<p style="text-align: center;">&nbsp;</p>
<p style="text-align: center;">&nbsp;</p>
<p style="text-align: center;">&nbsp;</p>
<p style="text-align: center;">&nbsp;</p>
<table style="margin-left: auto; margin-right: auto;">
<tbody>
<tr>
<td width="171">
<p>DATA PEMESANAN</p>
</td>
<td width="162">
<p>&nbsp;</p>
</td>
<td width="202">
<p>DETAIL PEMBAYARAN</p>
<p>&nbsp;</p>
</td>
<td width="136">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="171">
<p>Nama Lengkap :</p>
</td>
<td width="162">
<p><?= $model->cust_name ?></p>
</td>
<td width="202">
<p>P.O. NUMBER  :</p>
</td>
<td width="136">
<p><?= $model->order_no ?></p>
</td>
</tr>
<tr>
<td width="171">
<p>Nama Perusahaan :</p>
</td>
<td width="162">
<p><?= $model->ditagihkan_kepada ?></p>
</td>
<td width="202">
<p>DETAIL TRANSAKSI :</p>
</td>
<td width="136">
<p><strong>LUNAS</strong></p>
</td>
</tr>
<tr>
<td width="171">
<p>Alamat Lengkap :</p>
</td>
<td width="162">
<p><?= $model->address ?></p>
<!-- </td>
<td width="202">
<p>Alamat Lengkap :</p>
</td>
<td width="136">
<p><?= $model->ditagihkanOleh->alamat ?></p>
</td> -->
</tr>
<tr>
<td width="171">
</td>
<td width="162">
<p>&nbsp;</p>
</td>
<td width="202">

</td>
<td width="136">
<p>&nbsp;</p>
</td>
</tr>
</tbody>
</table>
<!-- <p style="text-align: center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
<p style="text-align: center;">&nbsp;</p> -->
<table style="margin-left: auto; margin-right: auto;">
<tbody>

<tr>
<td colspan="4" rowspan="3" width="501">
</td>
<td width="114">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="114">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td width="114">
<p>&nbsp;</p>
</td>
</tr>
</tbody>
</table>
 <div class='row'>
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-body">
                    <div class="table-responsive">
                    <?=GridView::widget([
                            'layout' => '{summary}{pager}{items}{pager}',
                            'pjax' => true,
                            'showFooter' => true,
                            'dataProvider' => $providerOrder,
                            'pager' => [
                                'class' => yii\widgets\LinkPager::class,
                                'firstPageLabel' => 'First',
                                'lastPageLabel' => 'Last'],
                            'filterModel' => $searchModel,
                            'tableOptions' => ['class' => 'table table-striped table-bordered table-hover'],
                            'headerRowOptions' => ['class' => 'x'],
                            'columns' => [  [
                                'attribute' => 'detailorder.deskripsi_barang',
                                'label' => 'Deskripsi Barang',
                                'value' => function($model) {
                                    return $model->deskripsi_barang;
                                }
                            ],
                            [
                                'attribute' => 'detailorder.price',
                                'label' => 'Harga Satuan',
                                'format'=>'raw',
                                'value' => function($model) {
                                    return \app\components\Angka::toReadableHarga($model->price);
                                }
                            ],
                            [
                                'attribute' => 'detailorder.quantity',
                                'label' => 'Jumlah',
                                'value' => function($model) {
                                    return $model->quantity;
                                }
                            ],
                            [
                                'attribute' => 'detailorder.keterangan',
                                'label' => 'Keterangan',
                                'footer' => "Total Tagihan",
                                'value' => function($model) {
                                    return $model->keterangan;
                                }
                            ],
                            [
                                'attribute' => 'detailorder.total',
                                'label' => 'Total Price ',
                                'footer' => "IDR  " . number_format($sum_total, 2, ",", "."),
                                'format' =>'raw',
                                'value' => function($model) {
                                    return \app\components\Angka::toReadableHarga($model->total);
                                }
                            ],
                               
                            ],
                        ]);?>
                    </div>
                </div>
            </div>
        </div>
    </div> 
<p style="text-align: center;">&nbsp;</p>
<!-- <p style="text-align: right;">TOTAL TAGIHAN</p>
<p style="text-align: right;"><?= "IDR  " . number_format($sum_bayar, 2, ",", ".") ?></p> -->
<p style="text-align: right;">&nbsp;</p>
<p style="text-align: right;">&nbsp;</p>
<p style="text-align: right;"></p>
<p style="text-align: right;">&nbsp;</p>
<p style="text-align: left;"><?= Html::img(["uploads/bt19.png"], ["width"=>"175px"]); ?></p>
<p style="text-align: right;">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $model->ditagihkan_kepada ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; )</p>
<p>&nbsp;</p>