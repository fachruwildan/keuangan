<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var app\models\search\OrderSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="order-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'ditagihkan_kepada') ?>

		<?= $form->field($model, 'ditagihkan_oleh') ?>

		<?= $form->field($model, 'order_no') ?>

		<?= $form->field($model, 'cust_name') ?>

		<?php // echo $form->field($model, 'address') ?>

		<?php // echo $form->field($model, 'tanggal_order') ?>

		<?php // echo $form->field($model, 'tanggal_approve') ?>

		<?php // echo $form->field($model, 'tanggal_delivery') ?>

		<?php // echo $form->field($model, 'tanggal_received') ?>

		<?php // echo $form->field($model, 'issued_by') ?>

		<?php // echo $form->field($model, 'approve_by') ?>

		<?php // echo $form->field($model, 'delivery_by') ?>

		<?php // echo $form->field($model, 'received_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
