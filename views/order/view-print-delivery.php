<?php 
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;
use mdm\admin\components\Helper;
use app\models\DetailOrder;
$sum_total = DetailOrder::find()
    ->where(['id_order'=>$model->id])
    ->sum('total');
$detail = (new \yii\db\Query())
    ->select(['a.deskripsi_barang as deskripsi_barang','a.quantity as qty','a.keterangan as ket'])
    ->from('detail_order a')
    ->leftJoin('order b', 'a.id_order=b.id')
    ->Where(['b.id'=>$model->id])
    ->all();
    
?>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h4 style="text-align: center;"><strong>DELIVERY ORDER</strong></h4>
<p><strong>Jalan Hasanudin No 22&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;Do No&nbsp; :</p></strong>
<p><strong>Kota Wisata Batu 65313&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  Do Date :</p></strong>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table>
<tbody>
<tr style="height: 33px;">
<td style="height: 33px;" width="86">
<p>Office</p>
</td>
<td style="height: 33px;" width="430">
<p>: 0341- 3061627</p>
</td>
<td style="height: 33px;" width="406">
<p>SO No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</p>
</td>
</tr>
<tr style="height: 33px;">
<td style="height: 33px;" width="86">
<p>Phone</p>
</td>
<td style="height: 33px;" width="430">
<p>: 0851 0160 6092</p>
</td>
<td style="height: 33px;" width="406">
<p>SO D ate&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</p>
</td>
</tr>
<tr style="height: 33.7604px;">
<td style="height: 33.7604px;" width="86">
<p>Cust Name</p>
</td>
<td style="height: 33.7604px;" width="430">
<p>: <?= $model->cust_name ?></p>
</td>
<td style="height: 33.7604px;" width="406">
<p>PO No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <?= $model->order_no ?></p>
</td>
</tr>
<tr style="height: 33px;">
<td style="height: 33px;" width="86">
<p>Address</p>
</td>
<td style="height: 33px;" width="430">
<p>: <?= $model->address ?></p>
</td>
<td style="height: 33px;" width="406">
<p>&nbsp;</p>
</td>
</tr>
</tbody>
</table>
<?php
 if ($providerOrder->totalCount) {
    $gridColumnInvoice = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'detailorder.deskripsi_barang',
            'label' => 'Deskripsi Barang',
            'value' => function($model) {
                return $model->deskripsi_barang;
            }
        ],
        [
            'attribute' => 'detailorder.price',
            'label' => 'Price',
            'format'=>'raw',
            'value' => function($model) {
                return \app\components\Angka::toReadableHarga($model->price);
            }
        ],
        [
            'attribute' => 'detailorder.quantity',
            'label' => 'Qty',
            'value' => function($model) {
                return $model->quantity;
            }
        ],
        [
            'attribute' => 'detailorder.keterangan',
            'label' => 'Keterangan',
            'value' => function($model) {
                return $model->keterangan;
            }
        ],
        [
            'attribute' => 'detailorder.total',
            'label' => 'Total Price ',
            'footer' => "IDR  " . number_format($sum_total, 2, ",", "."),
            'format' =>'raw',
            'value' => function($model) {
                return \app\components\Angka::toReadableHarga($model->total);
            }
        ],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerOrder,
        'pjax' => true,
        'showFooter' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-invoice']],
        'panel' => [
            //'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Invoice'),
        ],
        'export' => false,
        'columns' => $gridColumnInvoice,
    ]);
}
?>
<table>
<tbody>
<tr style="height: 33px;">
<td style="height: 33px;" width="198">
<p>Issued By</p>
</td>
<td style="height: 33px;" width="199">
<p>Aproved By</p>
</td>
<td style="height: 33px;" width="217">
<p>Delivery By</p>
</td>
<td style="height: 33px;" width="227">
<p>Received By</p>
</td>
</tr>
<tr style="height: 86px;">
<td style="height: 86px;" width="198">
<p>&nbsp;</p>
<div>
<div><?=Html::img(["uploads/".$model->issuedBy->tanda_tangan],["width"=>"150px"])?></div>
</div>
</td>
<td style="height: 86px;" width="199">
<p><?=Html::img(["uploads/".$model->approveBy->tanda_tangan],["width"=>"150px"])?></p>
</td>
<td style="height: 86px;" width="217">
<p>&nbsp;</p>
</td>
<td style="height: 86px;" width="227">
<p>&nbsp;</p>
</td>
</tr>
<tr style="height: 33.8264px;">
<td style="height: 33.8264px;" width="198">
<div>
<div><?=$model->issuedBy->name?></div>
</div>
</td>
<td style="height: 33.8264px;" width="199">
<p><?=$model->approveBy->name?></p>
</td>
<td style="height: 33.8264px;" width="217">
<p><?= $model->delivery_by ?></p>
</td>
<td style="height: 33.8264px;" width="227">
<p><?= $model->received_by ?></p>
</td>
</tr>
<tr style="height: 33px;">
<td style="height: 33px;" width="198">
<p><?= $model->tanggal_order ?></p>
</td>
<td style="height: 33px;" width="199">
<p><?= $model->tanggal_approve ?></p>
</td>
<td style="height: 33px;" width="217">
<p><?= $model->tanggal_delivery ?></p>
</td>
<td style="height: 33px;" width="227">
<p><?= $model->tanggal_received ?></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>