<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;

/**
* @var yii\web\View $this
* @var app\models\LogPresence $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="box box-info">
    <div class="box-body">
        <?php $form = ActiveForm::begin([
        'id' => 'LogPresence',
        'layout' => 'horizontal',
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error'
        ]
        );
        ?>
        
			<?= $form->field($model, 'id')->textInput() ?>
			<?= $form->field($model, 'qr_code')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'user_id')->textInput() ?>
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'temperature')->textInput() ?>
			<?= $form->field($model, 'photo')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'is_success')->textInput() ?>
			<?= $form->field($model, 'is_guest')->textInput() ?>
			<?= $form->field($model, 'is_late')->textInput() ?>
			<?= $form->field($model, 'date')->textInput() ?>
			<?= $form->field($model, 'time')->textInput() ?>
			<?= $form->field($model, 'timestamp')->textInput() ?>        <hr/>
        <?php echo $form->errorSummary($model); ?>
        <div class="row">
            <div class="col-md-offset-3 col-md-10">
                <?=  Html::submitButton('<i class="fa fa-save"></i> Simpan', ['class' => 'btn btn-success']); ?>
                <?=  Html::a('<i class="fa fa-chevron-left"></i> Kembali', ['index'], ['class' => 'btn btn-default']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>