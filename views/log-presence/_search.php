<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var app\models\search\LogPresenceSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="log-presence-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'qr_code') ?>

		<?= $form->field($model, 'user_id') ?>

		<?= $form->field($model, 'name') ?>

		<?= $form->field($model, 'temperature') ?>

		<?php // echo $form->field($model, 'photo') ?>

		<?php // echo $form->field($model, 'is_success') ?>

		<?php // echo $form->field($model, 'is_guest') ?>

		<?php // echo $form->field($model, 'is_late') ?>

		<?php // echo $form->field($model, 'date') ?>

		<?php // echo $form->field($model, 'time') ?>

		<?php // echo $form->field($model, 'timestamp') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
