<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use kartik\date\DatePicker;

/**
* @var yii\web\View $this
* @var app\models\Aset $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="box box-info">
    <div class="box-body">
        <?php $form = ActiveForm::begin([
        'id' => 'Aset',
        'layout' => 'horizontal',
        'enableClientValidation' => true,
        'errorSummaryCssClass' => 'error-summary alert alert-error'
        ]
        );
        ?>
        
			
			<?= $form->field($model, 'jenis_peralatan')->textarea(['rows' => 6])?>
			<?= $form->field($model, 'no_bukti')->textInput(['maxlength' => true]) ?>
			<?=$form->field($model, 'tanggal_perolehan')->widget(DatePicker::classname(), [
    'options' => ['placeholder' => 'Pilih Tanggal Perolehan'],
    'readonly' => true,
    'pluginOptions' => [
        'format' => 'yyyy-m-d',
        'autocomplete' => "off",
    ],
]);?>
			<?= $form->field($model, 'nilai_peralatan')->widget(\yii\widgets\MaskedInput::className(),[
            'name' => 'input-3',
            'mask' => '9',
            'clientOptions' => ['repeat' => 20, 'greedy' => false]
        ]) ?>
			<?= $form->field($model, 'metode_penyusutan')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'umur')->widget(\yii\widgets\MaskedInput::className(),[
            'name' => 'input-3',
            'mask' => '9',
            'clientOptions' => ['repeat' => 20, 'greedy' => false]
        ]) ?>
			<?= $form->field($model, 'beban_penyusutan_tahun')->widget(\yii\widgets\MaskedInput::className(),[
            'name' => 'input-3',
            'mask' => '9',
            'clientOptions' => ['repeat' => 20, 'greedy' => false]
        ]) ?>
			<?= $form->field($model, 'beban_penyusutan_bulan')->widget(\yii\widgets\MaskedInput::className(),[
            'name' => 'input-3',
            'mask' => '9',
            'clientOptions' => ['repeat' => 20, 'greedy' => false]
        ]) ?>        <hr/>
        <?php echo $form->errorSummary($model); ?>
        <div class="row">
            <div class="col-md-offset-3 col-md-10">
                <?=  Html::submitButton('<i class="fa fa-save"></i> Simpan', ['class' => 'btn btn-success']); ?>
                <?=  Html::a('<i class="fa fa-chevron-left"></i> Kembali', ['index'], ['class' => 'btn btn-default']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>