<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var app\models\search\AsetSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="aset-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'jenis_peralatan') ?>

		<?= $form->field($model, 'no_bukti') ?>

		<?= $form->field($model, 'tanggal_perolehan') ?>

		<?= $form->field($model, 'nilai_peralatan') ?>

		<?php // echo $form->field($model, 'metode_penyusutan') ?>

		<?php // echo $form->field($model, 'umur') ?>

		<?php // echo $form->field($model, 'beban_penyusutan_tahun') ?>

		<?php // echo $form->field($model, 'beban_penyusutan_bulan') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
