<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var app\models\search\InvoiceSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="invoice-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'id_pengajuan') ?>

		<?= $form->field($model, 'keterangan') ?>

		<?= $form->field($model, 'quantity') ?>

		<?= $form->field($model, 'price') ?>

		<?php // echo $form->field($model, 'total') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
