<div class="form-group" id="add-invoice">
<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use kartik\grid\GridView;
/**
* @var yii\web\View $this
* @var app\models\Invoice $model
* @var yii\widgets\ActiveForm $form
*/

?>
<div class="box box-info">
    <div class="box-body">
    <?php
    $dataProvider = new ArrayDataProvider([
        'allModels' => $row,
        'pagination' => [
            'pageSize' => -1
        ]
    ]);
    echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Invoice',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'id' => [
            'type' => TabularForm::INPUT_HIDDEN,
            'columnOptions'=>['hidden'=>true]
        ],
        'keterangan' => ['type' => TabularForm::INPUT_TEXTAREA],
        'quantity' => ['type' => TabularForm::INPUT_TEXTAREA],
        'price' => ['type' => TabularForm::INPUT_TEXTAREA],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowInvoice(' . $key . '); return false;', 'id' => 'invoice-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Invoice', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowInvoice()']),
        ]
    ]
]);
?>
<div class="form-group">
        <!-- ?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?> -->
        <?=  Html::submitButton('<i class="fa fa-save"></i> Simpan', ['class' => 'btn btn-success']); ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    </div>
</div>
<script>
    function addRowInvoice() {
        var data = $('#add-invoice :input').serializeArray();
        data.push({name: '_action', value : 'add'});
        $.ajax({
            type: 'POST',
            url: '/keuangans/web/invoice/add-invoice',
            data: data,
            success: function (data) {
                $('#add-invoice').html(data);
            }
        });
    }
    function delRowInvoice(id) {
        $('#add-invoice tr[data-key=' + id + ']').remove();
    }
</script>