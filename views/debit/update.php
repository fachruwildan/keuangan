<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\Debit $model
*/

$this->title = 'Debit ' . $model->id_debit . ', ' . 'Edit';
$this->params['breadcrumbs'][] = ['label' => 'Debit', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id_debit, 'url' => ['view', 'id_debit' => $model->id_debit]];
$this->params['breadcrumbs'][] = 'Edit';
?>

<p>
    <?= Html::a('<i class="fa fa-eye-open"></i> Lihat', ['view', 'id_debit' => $model->id_debit], ['class' => 'btn btn-default']) ?>
</p>

<?php echo $this->render('_form', [
'model' => $model,
]); ?>
