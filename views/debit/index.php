<?php

/* @var $this yii\web\View */
/* @var $searchModel common\models\AbsensiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = 'Debit';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
    $('.search-form').toggle(1000);
    return false;
});";
$this->registerJs($search);
?>
<div class="absensi-index">

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <p>
                        <?=Html::a('Create Absensi', ['create'], ['class' => 'btn btn-success'])?>
                        <?=Html::a('Advance Search', '#', ['class' => 'btn btn-info search-button'])?>
                    </p>
                    <div class="search-form" style="display:none">
                        <?=$this->render('_search', ['model' => $searchModel]);?>
                    </div>
                </div>
                <div class="box-body">
                    <?php
$gridColumn = [
    ['class' => 'yii\grid\SerialColumn'],
    \app\components\ActionButtonDebit::getButtons(),
    [
        'class' => yii\grid\DataColumn::className(),
        'attribute' => 'id_user',
        'filter' => false,
        'value' => function ($model) {
            if ($rel = $model->user) {
                return Html::a($rel->name, ['user/view', 'id' => $rel->id], ['data-pjax' => 0]);
            } else {
                return '';
            }
        },
        'format' => 'raw',
    ],
    [
        'attribute' => 'nominal',
        'label' => 'Nominal',
        'filter' => false,
        'value' =>function ($model) {
            return "Rp ".$model->nominal;

        },
    ],
    [
        'attribute' => 'tanggal',
        'filter' => false,
    ],
    [
        'attribute' => 'keterangan',
        'filter' => false,
    ],

];
?>
                    <?=GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridColumn,
    'pjax' => true,
    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-absensi']],
    'panel' => [
        //'type' => GridView::TYPE_PRIMARY,
        'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
    ],
    'export' => false,
    // your toolbar can include the additional full export menu
    'toolbar' => [
        '{export}',
        ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumn,
            'target' => ExportMenu::TARGET_BLANK,
            'fontAwesome' => true,
            'dropdownOptions' => [
                'label' => 'Full',
                'class' => 'btn btn-default',
                'itemsBefore' => [
                    '<li class="dropdown-header">Export All Data</li>',
                ],
            ],
            'exportConfig' => [
                ExportMenu::FORMAT_HTML => false,
                ExportMenu::FORMAT_TEXT => false,
            ],
        ]),
    ],
]);?>
                </div>
            </div>
        </div>
    </div>

</div>