<?php

namespace app\models;

use Yii;
use \app\models\base\TransaksiUang as BaseTransaksiUang;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "transaksi_uang".
 */
class TransaksiUang extends BaseTransaksiUang
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
