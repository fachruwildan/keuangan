<?php

namespace app\models;

use app\models\Debit;
use app\models\Kredit;
use yii\helpers\ArrayHelper;
use \app\models\base\LogActivity as BaseLogActivity;

/**
 * This is the model class for table "log_activity".
 */
class LogActivity extends BaseLogActivity
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }

    // public function sumDebit()
    // {
    //     $month = date('m');
    //     $year = date('Y');
    //     $day = date('d');
    //     $datenow = date('Y-m-d');
    //     $debit = LogActivity::find()
    //     ->select(['sum(debit)','sum()'])
    //     ->where(['and', ['>=', 'tanggal', "$year-$month-01"], ['<=', 'tanggal', "$datenow"]])
    //     ->groupBy('tanggal');
    //     var_dump($debit->createCommand()->getRawSql());
    //     die;
    //     return $debit;
    // }
    // public function sumKredit()
    // {
    //     $month = date('m');
    //     $year = date('Y');
    //     $day = date('d');
    //     $datenow = date('Y-m-d');
    //     $kredit = Kredit::find()->where(['and', ['>=', 'tanggal', "$year-$month-01"], ['<=', 'tanggal', "$datenow"]])->sum('nominal');
    //     return $kredit;
    // }
}
