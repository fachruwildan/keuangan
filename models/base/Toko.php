<?php
// This class was automatically generated by a giiant build task
// You should not change it manually as it will be overwritten on next build

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "toko".
 *
 * @property integer $id
 * @property integer $id_perusahaan
 * @property string $nama_toko
 *
 * @property \app\models\Perusahaan $perusahaan
 * @property string $aliasModel
 */
abstract class Toko extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'toko';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_perusahaan', 'nama_toko'], 'required'],
            [['id_perusahaan'], 'integer'],
            [['nama_toko'], 'string', 'max' => 250],
            [['id_perusahaan'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\Perusahaan::className(), 'targetAttribute' => ['id_perusahaan' => 'id_perusahaan']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_perusahaan' => 'Perusahaan',
            'nama_toko' => 'Nama Toko',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerusahaan()
    {
        return $this->hasOne(\app\models\Perusahaan::className(), ['id_perusahaan' => 'id_perusahaan']);
    }




}
