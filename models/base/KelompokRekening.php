<?php
// This class was automatically generated by a giiant build task
// You should not change it manually as it will be overwritten on next build

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "kelompok_rekening".
 *
 * @property integer $id
 * @property string $nama
 * @property string $kelompok
 * @property string $jenis
 *
 * @property \app\models\TransaksiUang[] $transaksiUangs
 * @property string $aliasModel
 */
abstract class KelompokRekening extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kelompok_rekening';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'kelompok', 'jenis'], 'required'],
            [['nama', 'kelompok', 'jenis'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'No Rekening',
            'nama' => 'Nama',
            'kelompok' => 'Kelompok',
            'jenis' => 'Jenis',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaksiUangs()
    {
        return $this->hasMany(\app\models\TransaksiUang::className(), ['id_kelompok_rekening' => 'id']);
    }




}
