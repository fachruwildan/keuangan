<?php

namespace app\models;

use Yii;
use \app\models\base\Kredit as BaseKredit;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "kredit".
 */
class Kredit extends BaseKredit
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
