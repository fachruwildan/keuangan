<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DailySchedule;

/**
* DailyScheduleSearch represents the model behind the search form about `app\models\DailySchedule`.
*/
class DailyScheduleSearch extends DailySchedule
{
    public $start_date;
    public $end_date;
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'id_time', 'status','executor','assign_information_to'], 'integer'],
            [['date','description','start_date','end_date','note'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
    $admin = Yii::$app->user->identity->role_id == 1;
    if($admin){
        $query = DailySchedule::find()->orderBy([
            'date' => SORT_ASC
          ]);        
    }else{
        $month = date('m');
        $year = date('Y');
        $datenow = date('Y-m-d');
      
        $query = DailySchedule::find()->orderBy([
            'date' => SORT_ASC
          ])->where(['date'=>date('Y-m-d')])->andWhere(['executor' => Yii::$app->user->identity->id]);
    }

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            // 'id' => $this->id,
            // 'id_time' => $this->id_time,
            // 'status' => $this->status,
            // 'date'=>$this->date,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);
        // ->andFilterWhere(['like', 'date', $this->date]);

        if($this->start_date != null){
            $query->andWhere(['>=', 'date', $this->start_date]);
            if($this->end_date != null){
                $query->andWhere(['<=', 'date', $this->end_date]);
            }
        }
       
return $dataProvider;
}
}