<?php

namespace app\models\search;

use app\models\Presence;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PresenceSearch represents the model behind the search form about `app\models\Presence`.
 */
class PresenceSearch extends Presence
{
/**
 * @inheritdoc
 */
    public function rules()
    {
        return [
            [['id', 'user_id', 'is_guest', 'is_late', 'time_late'], 'integer'],
            [['qr_code', 'name', 'photo', 'date', 'action', 'time_in', 'time_out'], 'safe'],
            [['temperature'], 'number'],
        ];
    }

/**
 * @inheritdoc
 */
    public function scenarios()
    {
// bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

/**
 * Creates data provider instance with search query applied
 *
 * @param array $params
 *
 * @return ActiveDataProvider
 */
    public function search($params)
    {
        $query = Presence::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'temperature' => $this->temperature,
            'is_guest' => $this->is_guest,
            'is_late' => $this->is_late,
            'date' => $this->date,
            'time_late' => $this->time_late,
        ]);

        $query->andFilterWhere(['like', 'qr_code', $this->qr_code])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'action', $this->action])
            ->andFilterWhere(['like', 'time_in', $this->time_in])
            ->andFilterWhere(['like', 'time_out', $this->time_out]);

        return $dataProvider;
    }
}
