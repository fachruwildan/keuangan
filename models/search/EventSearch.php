<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Event;

/**
* EventSearch represents the model behind the search form about `app\models\Event`.
*/
class EventSearch extends Event
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'id_time', 'status','assign_information_to','maker'], 'integer'],
            [['date', 'description','name'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
    $admin = Yii::$app->user->identity->role_id == 1;
    if($admin){
$query = Event::find()->orderBy([
    'date' => SORT_ASC
  ]);
}else{
    $query = Event::find()->orderBy([
        'date' => SORT_ASC
      ])->where(['maker'=>\Yii::$app->user->identity->id]);
}

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'id_time' => $this->id_time,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
        ->andFilterWhere(['like', 'name', $this->name]);

return $dataProvider;
}
}