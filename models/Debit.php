<?php

namespace app\models;

use Yii;
use \app\models\base\Debit as BaseDebit;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "debit".
 */
class Debit extends BaseDebit
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
