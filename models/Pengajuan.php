<?php

namespace app\models;

use Yii;
use \app\models\base\Pengajuan as BasePengajuan;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "pengajuan".
 */
class Pengajuan extends BasePengajuan
{
    public function fields()
    {
        // return [
        //     "status" => function(){
        //         if($this->status == 0){

        //         }
        //     },
        // ];
    }
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
