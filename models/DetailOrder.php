<?php

namespace app\models;

use Yii;
use \app\models\base\DetailOrder as BaseDetailOrder;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "detail_order".
 */
class DetailOrder extends BaseDetailOrder
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
