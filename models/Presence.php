<?php

namespace app\models;

use Yii;
use \app\models\base\Presence as BasePresence;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "presence".
 */
class Presence extends BasePresence
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
