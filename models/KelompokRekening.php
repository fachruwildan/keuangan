<?php

namespace app\models;

use Yii;
use \app\models\base\KelompokRekening as BaseKelompokRekening;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "kelompok_rekening".
 */
class KelompokRekening extends BaseKelompokRekening
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
