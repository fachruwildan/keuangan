<?php

namespace app\models;

use Yii;
use \app\models\base\Times as BaseTimes;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "times".
 */
class Times extends BaseTimes
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
