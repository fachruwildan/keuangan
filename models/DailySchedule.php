<?php

namespace app\models;

use Yii;
use \app\models\base\DailySchedule as BaseDailySchedule;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "daily_schedule".
 */
class DailySchedule extends BaseDailySchedule
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
