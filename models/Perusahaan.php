<?php

namespace app\models;

use Yii;
use \app\models\base\Perusahaan as BasePerusahaan;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "perusahaan".
 */
class Perusahaan extends BasePerusahaan
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }
}
