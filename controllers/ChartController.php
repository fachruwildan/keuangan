<?php

namespace app\controllers;

use app\models\TransaksiUang;
use app\models\search\PelatihanSearch;
use Yii;
use yii\web\Controller;

class ChartController extends Controller
{
    public function actionPelatihanTingkat()
    {
        $tahun = isset($_GET['tahun']) ? $_GET['tahun'] : date('Y');

        $models = Pelatihan::find()
            ->leftJoin("pelatihan_tingkat", 'pelatihan_tingkat.id = pelatihan.tingkat_id')
            ->select('pelatihan_tingkat.nama label, COUNT(pelatihan.id) AS value')
            ->where(['pelatihan.flag' => 1])
            ->andWhere(['like', 'tanggal_mulai', $tahun])
            ->groupBy(['pelatihan_tingkat.nama'])
            ->asArray()
            ->all();

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return [
            "result" => true,
            "data" => [
                "label" => array_column($models, 'label'),
                "value" => array_column($models, 'value')
            ]
        ];
    }

    public function actionPelatihanUserPembuat()
    {
        $tahun = isset($_GET['tahun']) ? $_GET['tahun'] : date('Y');

        $models = Pelatihan::find()
            ->leftJoin("user", 'user.id = pelatihan.created_by')
            ->select('user.name label, COUNT(pelatihan.id) AS value')
            ->where(['pelatihan.flag' => 1])
            ->andWhere(['like', 'tanggal_mulai', $tahun])
            ->groupBy(['user.name'])
            ->asArray()
            ->all();

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            "result" => true,
            "data" => [
                "label" => array_column($models, 'label'),
                "value" => array_column($models, 'value')
            ]
        ];
    }

    public function actionTransaksiMasuk()
    {
        $dateY=date('Y');
        // $tahun = isset($_GET['tahun']) ? $_GET['tahun'] : date('Y');

//         $query = "
//         SELECT b.nama as label,a.debit AS value FROM `transaksi_uang` `a` 
// LEFT JOIN `kelompok_rekening` `b` ON a.id_kelompok_rekening=b.id
// WHERE (`b`.`kelompok`='Debit') AND (SUBSTRING(a.tanggal,1,4)='$dateY')
//         ";
//         $connection = Yii::$app->getDb();
//         $command = $connection->createCommand($query);
//         $models = $command->queryAll();
        $models= TransaksiUang::find()
        ->leftJoin("kelompok_rekening", 'kelompok_rekening.id = transaksi_uang.id_kelompok_rekening')
        ->select('kelompok_rekening.nama as label, transaksi_uang.debit AS value')
        ->where(['kelompok_rekening.kelompok' => 'Debit'])
        ->andWhere(['SUBSTRING(transaksi_uang.tanggal,1,4)'=>date('Y')])
        ->asArray()
        ->all();

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            "result" => true,
            "data" => [
                "label" => array_column($models, 'label'),
                "value" => array_column($models, 'value')
            ]
        ];
    }
    public function actionPelatihanBulan()
    {

        $tahun = isset($_GET['tahun']) ? $_GET['tahun'] : date('Y');

        $query = "
        SELECT master_bulan.nama label, count(pelatihan.id) as value
        FROM master_bulan 
        LEFT JOIN pelatihan 
            ON MONTH(pelatihan.tanggal_mulai) = master_bulan.id 
            AND YEAR(pelatihan.tanggal_mulai) = :tahun
        GROUP BY master_bulan.nama
        ORDER BY master_bulan.id
        ";
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($query, [':tahun' => $tahun]);
        $models = $command->queryAll();

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            "result" => true,
            "data" => [
                "label" => array_column($models, 'label'),
                "value" => array_column($models, 'value')
            ]
        ];
    }
}
