<?php
// This class was automatically generated by a giiant build task
// You should not change it manually as it will be overwritten on next build

namespace app\controllers\base;

use app\models\Order;
use app\models\DetailOrder;
use app\models\search\OrderSearch;
use Yii;
use app\models\Pengajuan;
use app\models\Action;
use app\models\base\Model;
use app\models\search\PengajuanSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\helpers\Url;
use yii\filters\AccessControl;
use dmstr\bootstrap\Tabs;
use kartik\mpdf\Pdf;
/**
* OrderController implements the CRUD actions for Order model.
*/
class OrderController extends Controller
{


/**
* @var boolean whether to enable CSRF validation for the actions in this controller.
* CSRF validation is enabled only when both this property and [[Request::enableCsrfValidation]] are true.
*/
public $enableCsrfValidation = false;
public function behaviors()
{
//NodeLogger::sendLog(Action::getAccess($this->id));
//apply role_action table for privilege (doesn't apply to super admin)
return Action::getAccess($this->id);
}

/**
* Lists all Order models.
* @return mixed
*/
public function actionIndex()
{
    $searchModel  = new OrderSearch;
    $dataProvider = $searchModel->search($_GET);

Tabs::clearLocalStorage();

Url::remember();
\Yii::$app->session['__crudReturnUrl'] = null;

return $this->render('index', [
'dataProvider' => $dataProvider,
    'searchModel' => $searchModel,
]);
}

/**
* Displays a single Order model.
* @param integer $id
*
* @return mixed
*/
public function actionView($id)
{
\Yii::$app->session['__crudReturnUrl'] = Url::previous();
Url::remember();
Tabs::rememberActiveState();

return $this->render('view', [
'model' => $this->findModel($id),
]);
}

/**
* Creates a new Order model.
* If creation is successful, the browser will be redirected to the 'view' page.
* @return mixed
*/
public function actionCreate()
{
    $model = new Order;
    $modelsDetailOrders= [new DetailOrder];
    
    
    
    // var_dump($no_perusahaan);
    // die;
    try {
        $model->issued_by=\Yii::$app->user->identity->id;
        $model->tanggal_order=date('Y-m-d H:i:s');
    if ($model->load($_POST)) {
        $cek_max =  Order::find()->max('order_no');
        $cek=(int) substr($cek_max,3);
        $cek++;
    
        $no_pengajuan = sprintf("%05s",$cek);
        // $model->order_no="OR".$no_pengajuan;
        $model->order_no=$no_pengajuan;
        if($model->save()){
    //         $nowa='6285604845437';
    // $deskripsi='testing send Pesan wa Pengajuan';
    // $data=[
    //     'phone'=>$nowa,
    //     'body'=>$deskripsi
    // ];
    // $json=json_encode($data);
    // $token='vxvx21g42mfmemnm';
    // $instanceld='194573';
    // $url='https://eu204.chat-api.com/instance'.$instanceld.'/sendMessage?token=' .$token;
    // $options=stream_context_create(['http'=>[
    //     'method'=>'POST',
    //     'header'=>'Content-type:application/json',
    //     'content'=>$json
    // ]]);
    // $result=file_get_contents($url,false,$options);
       $modelsDetailOrders = Model::createMultiple(DetailOrder::classname());
       Model::loadMultiple($modelsDetailOrders, Yii::$app->request->post());
    
       // // ajax validation
       if (Yii::$app->request->isAjax) {
           Yii::$app->response->format = Response::FORMAT_JSON;
           return ArrayHelper::merge(
               ActiveForm::validateMultiple($modelsDetailOrders),
               ActiveForm::validate($model)
           );
       }
    
       // validate all models
       $valid = $model->validate();
       $valid = Model::validateMultiple($modelsDetailOrders) && $valid;
       
       if ($valid) {
           $transaction = \Yii::$app->db->beginTransaction();
           try {
               if ($flag = $model->save(false)) {
                   foreach ($modelsDetailOrders as $modelsDetailOrder) {
                       $modelsDetailOrder->id_order = $model->id;
                       $totals=$modelsDetailOrder->price * $modelsDetailOrder->quantity;
                       $diskon=$totals * $modelsDetailOrder->diskon;
                       $akhir=$totals - $diskon;
                       $modelsDetailOrder->total=$akhir;
                       if (! ($flag = $modelsDetailOrder->save(false))) {
                           $transaction->rollBack();
                           break;
                       }
                   }
               }
               if ($flag) {
                   $transaction->commit();
                //    var_dump($transaction);
                //    die;
                //    return $this->redirect(['view', 'id' => $model->id]);
               }
           } catch (Exception $e) {
               $transaction->rollBack();
           }
       }
    //   var_dump($modelsDetailOrders);
    //   die;
    return $this->redirect(['view', 'id' => $model->id]);
    }
    } elseif (!\Yii::$app->request->isPost) {
    $model->load($_GET);
    }
    } catch (\Exception $e) {
    $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
    $model->addError('_exception', $msg);
    }
    return $this->render('create', [
       'model' => $model,
       'modelsDetailOrders' => (empty($modelsDetailOrders)) ? [new DetailOrder] : $modelsDetailOrders]);
    }

/**
* Updates an existing Order model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id
* @return mixed
*/
public function actionUpdate($id)
{
    $model = $this->findModel($id);
    $modelsDetailOrders = $model->detailOrders;
    if ($model->load($_POST) && $model->save()) {
       $oldIDs = ArrayHelper::map($modelsDetailOrders, 'id', 'id_order');
       $modelsDetailOrders = Model::createMultiple(DetailOrder::classname(), $modelsDetailOrders);
       Model::loadMultiple($modelsDetailOrders, Yii::$app->request->post());
       $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsDetailOrders, 'id', 'id_order')));
    
       // ajax validation
       if (Yii::$app->request->isAjax) {
           Yii::$app->response->format = Response::FORMAT_JSON;
           return ArrayHelper::merge(
               ActiveForm::validateMultiple($modelsDetailOrders),
               ActiveForm::validate($model)
           );
       }
    
       // validate all models
       $valid = $model->validate();
       $valid = Model::validateMultiple($modelsDetailOrders) && $valid;
    
       if ($valid) {
           $transaction = \Yii::$app->db->beginTransaction();
           try {
               if ($flag = $model->save(false)) {
                   if (! empty($deletedIDs)) {
                       DetailOrder::deleteAll(['id' => $deletedIDs]);
                   }
                   foreach ($modelsDetailOrders as $modelsDetailOrder) {
                    $modelsDetailOrder->id_order = $model->id;
                    $modelsDetailOrder->total=$modelsDetailOrder->price * $modelsDetailOrder->quantity;
                       if (! ($flag = $modelsDetailOrder->save(false))) {
                           $transaction->rollBack();
                           break;
                       }
                   }
               }
               if ($flag) {
                   $transaction->commit();
                   return $this->redirect(['view', 'id' => $model->id]);
               }
           } catch (Exception $e) {
               $transaction->rollBack();
           }
       }
    return $this->redirect(Url::previous());
    } else {
    return $this->render('update', [
    'model' => $model,
    'modelsDetailOrders' => (empty($modelsDetailOrders)) ? [new DetailOrder] : $modelsDetailOrders
    ]);
    }
}

/**
* Deletes an existing Order model.
* If deletion is successful, the browser will be redirected to the 'index' page.
* @param integer $id
* @return mixed
*/
public function actionDelete($id)
{
try {
$this->findModel($id)->delete();
} catch (\Exception $e) {
$msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
\Yii::$app->getSession()->addFlash('error', $msg);
return $this->redirect(Url::previous());
}

// TODO: improve detection
$isPivot = strstr('$id',',');
if ($isPivot == true) {
return $this->redirect(Url::previous());
} elseif (isset(\Yii::$app->session['__crudReturnUrl']) && \Yii::$app->session['__crudReturnUrl'] != '/') {
Url::remember(null);
$url = \Yii::$app->session['__crudReturnUrl'];
\Yii::$app->session['__crudReturnUrl'] = null;

return $this->redirect($url);
} else {
return $this->redirect(['index']);
}
}
public function actionApprove($id){
    $model = $this->findModel($_GET['id']);
      //return print_r($model);
      if ($model) {
         $model->status = 2;
         $model->approve_by=\Yii::$app->user->identity->id;
         $model->tanggal_approve=date('Y-m-d H:i:s');
         if ($model->save()){
           
           

            \Yii::$app->getSession()->setFlash(
               'success', 'Order Telah Disetujui!'
            );
         } else {
            \Yii::$app->getSession()->setFlash(
               'danger', 'Order Gagal Disetujui!'
            );
         }
         return $this->redirect(['index']);
      }
}

public function actionCancel($id){
    $model = $this->findModel($_GET['id']);
    //return print_r($model);
    if ($model) {
       $model->status = 0;
       $model->approve_by=\Yii::$app->user->identity->id;
       $model->tanggal_approve=date('Y-m-d H:i:s');
       if ($model->save()){
         
         

          \Yii::$app->getSession()->setFlash(
             'success', 'Order Telah Anda Tolak!'
          );
       } else {
          \Yii::$app->getSession()->setFlash(
             'danger', 'Order Gagal untuk Anda Tolak!'
          );
       }
       return $this->redirect(['index']);
    }
}
public function actionDelivery($id){
    $model = $this->findModel($id);
$model->status=3;
$model->tanggal_delivery=date('Y-m-d H:i:s');
if ($model->load($_POST) && $model->save()) {
return $this->redirect(Url::previous());
} else {
return $this->render('delivery', [
'model' => $model,
]);
}
}
public function actionReceived($id)
{
$model = $this->findModel($id);
$model->status=4;
$model->tanggal_received=date('Y-m-d H:i:s');
if ($model->load($_POST) && $model->save()) {
return $this->redirect(Url::previous());
} else {
return $this->render('received', [
'model' => $model,
]);
}
}

public function actionCetakPurchase($id) {
    $formatter = \Yii::$app->formatter;
    $model=$this->findModel($id);
    $providerOrder = new \yii\data\ArrayDataProvider([
        'allModels' => (array) $model->detailOrders,
     ]);
    $content = $this->renderPartial('view-print-purchase',[
        'model' => $model,
        'providerOrder' => $providerOrder,
]);
    
    // setup kartik\mpdf\Pdf component
    $pdf = new Pdf([
        // set to use core fonts only
        'mode' => Pdf::MODE_CORE, 
        //Name file
        'filename' => 'Download Purchase Order  '."'".$model->tujuan."'.pdf",
        // LEGAL paper format
        'format' => Pdf::FORMAT_FOLIO, 
        // portrait orientation
        'orientation' => Pdf::ORIENT_PORTRAIT, 
        // stream to browser inline
        'destination' => Pdf::DEST_BROWSER, 
        // your html content input
        'content' => $content,
        'marginHeader' => 0,
        'marginFooter' => 5,
        'marginTop' => 40,
        'marginBottom' => 5,
        'marginLeft' => 0,
        'marginRight' => 0,  
        // format content from your own css file if needed or use the
        // enhanced bootstrap css built by Krajee for mPDF formatting 
        'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
        // any css to be embedded if required
        // 'cssInline' => '.kv-heading-1{font-size:25px}', 
        'cssInline' => 'body, p { font-family: irannastaliq; font-size: 17px; };
        .kv-heading-1{font-size:17px}table{width: 100%;line-height: inherit;text-align: left; border-collapse: collapse;}table, td, th {}', 
        // set mPDF properties on the fly
        'options' => [               
            'defaultheaderline' => 0,  //for header
             'defaultfooterline' => 0,  //for footer
        ],
         // call mPDF methods on the fly
        'methods' => [
            'SetTitle'=>'Print', 
            'SetHeader' => $this->renderPartial('header_gambar'),
            // 'SetHeader'=>['AMONG TANI FOUNDATION'],
            'SetFooter'=>$this->renderPartial('footer_gambar'),
            
        ]
    ]);
    return $pdf->render(); 
}
public function actionCetakDelivery($id) {
    $formatter = \Yii::$app->formatter;
    $model=$this->findModel($id);
    $providerOrder = new \yii\data\ArrayDataProvider([
        'allModels' => (array) $model->detailOrders,
     ]);
    $content = $this->renderPartial('view-print-delivery',[
        'model' => $model,
        'providerOrder' => $providerOrder,
]);
    
    // setup kartik\mpdf\Pdf component
    $pdf = new Pdf([
        // set to use core fonts only
        'mode' => Pdf::MODE_CORE, 
        //Name file
        'filename' => 'Download Delivery Order  '."'".$model->tujuan."'.pdf",
        // LEGAL paper format
        'format' => Pdf::FORMAT_LEGAL, 
        // portrait orientation
        'orientation' => Pdf::ORIENT_LANDSCAPE, 
        // stream to browser inline
        'destination' => Pdf::DEST_BROWSER, 
        // your html content input
        'content' => $content,  
        'marginHeader' => 0,
        'marginFooter' => 5,
        'marginTop' => 40,
        'marginBottom' => 5,
        'marginLeft' => 5,
        'marginRight' => 5,
        // format content from your own css file if needed or use the
        // enhanced bootstrap css built by Krajee for mPDF formatting 
        'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
        // any css to be embedded if required
        // 'cssInline' => '.kv-heading-1{font-size:25px}', 
        'cssInline' => 'body, p { font-family: irannastaliq; font-size: 15px; };
        .kv-heading-1{font-size:15px}table{width: 100%;line-height: inherit;text-align: left; border-collapse: collapse;}table, td, th {margin-left:50px;margin-right:50px;}', 
        // set mPDF properties on the fly
        'options' => [               
            'defaultheaderline' => 0,  //for header
             'defaultfooterline' => 0,  //for footer
        ],
         // call mPDF methods on the fly
        'methods' => [
            'SetTitle'=>'Print', 
            'SetHeader' => $this->renderPartial('header_gambar'),
            // 'SetHeader'=>['AMONG TANI FOUNDATION'],
            'SetFooter'=>$this->renderPartial('footer_gambar'),
            
        ]
    ]);
    return $pdf->render(); 
}
public function actionCetakInvoice($id) {
    
    $model=$this->findModel($id);
    $ttd_pembuat= (new \yii\db\Query())
     ->select(['a.tanda_tangan as tanda_tangan','a.name as name','b.name as role'])
     ->from('user a')
     ->leftJoin('role b', 'a.role_id=b.id')
    ->Where(['a.id'=>$model->issued_by])
    ->all();
    $formatter = \Yii::$app->formatter;
    $model=$this->findModel($id);
    $providerOrder = new \yii\data\ArrayDataProvider([
        'allModels' => (array) $model->detailOrders,
     ]);
    $content = $this->renderPartial('view-print-invoice',[
        'model' => $model,
        'ttd_pembuat' => $ttd_pembuat,
        'providerOrder' => $providerOrder,
]);
    
    // setup kartik\mpdf\Pdf component
    $pdf = new Pdf([
        // set to use core fonts only
        'mode' => Pdf::MODE_CORE, 
        //Name file
        'filename' => 'Download Invoice Order  '."'".$model->tujuan."'.pdf",
        // LEGAL paper format
        'format' => Pdf::FORMAT_FOLIO, 
        // portrait orientation
        'orientation' => Pdf::ORIENT_PORTRAIT, 
        // stream to browser inline
        'destination' => Pdf::DEST_BROWSER, 
        // your html content input
        'content' => $content,  
        'marginHeader' => 0,
        'marginFooter' => 5,
        'marginTop' => 40,
        'marginBottom' => 5,
        'marginLeft' => 0,
        'marginRight' => 0,
        // format content from your own css file if needed or use the
        // enhanced bootstrap css built by Krajee for mPDF formatting 
        'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
        // any css to be embedded if required
        // 'cssInline' => '.kv-heading-1{font-size:25px}', 
        'cssInline' => 'body, p { font-family: irannastaliq; font-size: 15px; };
        .kv-heading-1{font-size:15px}table{width: 100%;line-height: inherit;text-align: left; border-collapse: collapse;}table, td, th {margin-left:50px;margin-right:50px;}', 
        // set mPDF properties on the fly
        'options' => [               
            'defaultheaderline' => 0,  //for header
             'defaultfooterline' => 0,  //for footer
        ],
         // call mPDF methods on the fly
        'methods' => [
            'SetTitle'=>'Print', 
            'SetHeader' => $this->renderPartial('header_gambar'),
            // 'SetHeader'=>['AMONG TANI FOUNDATION'],
            'SetFooter'=>$this->renderPartial('footer_gambar'),
        ]
    ]);
    return $pdf->render(); 
}
public function actionCetakPembayaran($id) {
    $formatter = \Yii::$app->formatter;
    $model=$this->findModel($id);
    $providerOrder = new \yii\data\ArrayDataProvider([
        'allModels' => (array) $model->detailOrders,
     ]);
    $content = $this->renderPartial('view-print-pembayaran',[
        'model' => $model,
        'providerOrder' => $providerOrder,
]);
    
    // setup kartik\mpdf\Pdf component
    $pdf = new Pdf([
        // set to use core fonts only
        'mode' => Pdf::MODE_CORE, 
        //Name file
        'filename' => 'Cetak Pembayaran dengan nomor Order  '."'".$model->order_no."'.pdf",
        // LEGAL paper format
        'format' => Pdf::FORMAT_LEGAL, 
        // portrait orientation
        'orientation' => Pdf::ORIENT_PORTRAIT, 
        // stream to browser inline
        'destination' => Pdf::DEST_BROWSER, 
        // your html content input
        'content' => $content,  
        // format content from your own css file if needed or use the
        // enhanced bootstrap css built by Krajee for mPDF formatting 
        'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
        // any css to be embedded if required
        // 'cssInline' => '.kv-heading-1{font-size:25px}', 
        'cssInline' => 'body, p { font-family: irannastaliq; font-size: 15px; };
        .kv-heading-1{font-size:15px}table{width: 100%;line-height: inherit;text-align: left; border-collapse: collapse;}table, td, th {}', 
        // set mPDF properties on the fly
        'options' => [               
            'defaultheaderline' => 0,  //for header
             'defaulfooterline' => 0  //for footer
        ],
         // call mPDF methods on the fly
        'methods' => [
            'SetTitle'=>'Print', 
            'SetHeader' => ['Batu Tracking 19: ' . $formatter->asDate(date("r"))],
            // 'SetHeader'=>['Batu Tracking 19'],
            'SetFooter'=>['{PAGENO}'],
            
        ]
    ]);
    return $pdf->render(); 
}
public function actionCetakKwitansi($id) {
    $formatter = \Yii::$app->formatter;
    $model=$this->findModel($id);
    $ttd_pembuat= (new \yii\db\Query())
    ->select(['a.tanda_tangan as tanda_tangan','a.name as name','b.name as role'])
    ->from('user a')
    ->leftJoin('role b', 'a.role_id=b.id')
   ->Where(['a.id'=>$model->issued_by])
   ->all();
    $providerInvoice = new \yii\data\ArrayDataProvider([
        'allModels' => (array) $model->detailOrders,
     ]);
    $content = $this->renderPartial('print-kwitansi',[
        'model' => $model,
        'providerInvoice' => $providerInvoice,
        'ttd_pembuat' => $ttd_pembuat,
]);
    
    // setup kartik\mpdf\Pdf component
    $pdf = new Pdf([
        // set to use core fonts only
        'mode' => Pdf::MODE_CORE, 
        //Name file
        'filename' => 'Kwitansi  '."'".$model->tujuan."'.pdf",
        // LEGAL paper format
        'format' => Pdf::FORMAT_LEGAL, 
        // portrait orientation
        'orientation' => Pdf::ORIENT_LANDSCAPE, 
        // stream to browser inline
        'destination' => Pdf::DEST_BROWSER, 
        // your html content input
        'content' => $content,  
        'marginHeader' => 0,
        'marginFooter' => 5,
        'marginTop' => 40,
        'marginBottom' => 5,
        'marginLeft' => 0,
        'marginRight' => 0,
        // format content from your own css file if needed or use the
        // enhanced bootstrap css built by Krajee for mPDF formatting 
        'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
        // any css to be embedded if required
        // 'cssInline' => '.kv-heading-1{font-size:25px}', 
        'cssInline' => 'body, p { font-family: irannastaliq; font-size: 15px; };
        .kv-heading-1{font-size:15px}table{width: 100%;line-height: inherit;text-align: left; border-collapse: collapse;}table, td, th {margin-left:50px;margin-right:50px;font-size:20px;}', 
        // set mPDF properties on the fly
        'options' => [               
            'defaultheaderline' => 0,  //for header
             'defaultfooterline' => 0,  //for footer
        ],
         // call mPDF methods on the fly
        'methods' => [
            'SetTitle'=>'Print', 
            'SetHeader' => $this->renderPartial('header_gambar'),
            // 'SetHeader'=>['AMONG TANI FOUNDATION'],
            'SetFooter'=>$this->renderPartial('footer_gambar'),
            
        ]
    ]);
    return $pdf->render(); 
}

/**
* Finds the Order model based on its primary key value.
* If the model is not found, a 404 HTTP exception will be thrown.
* @param integer $id
* @return Order the loaded model
* @throws HttpException if the model cannot be found
*/
protected function findModel($id)
{
if (($model = Order::findOne($id)) !== null) {
return $model;
} else {
throw new HttpException(404, 'The requested page does not exist.');
}
}
}
