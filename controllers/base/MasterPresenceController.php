<?php
namespace app\controllers\base;

use app\models\Action;
use app\models\MasterPresence;
use app\models\search\MasterPresenceSearch;
use dmstr\bootstrap\Tabs;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

class MasterPresenceController extends Controller
{

    public $enableCsrfValidation = false;
    public function behaviors()
    {
        return Action::getAccess($this->id);
    }

    public function actionIndex()
    {
        return $this->redirect(['view', 'id' => 1]);
        // $searchModel = new MasterPresenceSearch;
        // $dataProvider = $searchModel->search($_GET);

        // Tabs::clearLocalStorage();

        // Url::remember();
        // \Yii::$app->session['__crudReturnUrl'] = null;

        // return $this->render('index', [
        //     'dataProvider' => $dataProvider,
        //     'searchModel' => $searchModel,
        // ]);
    }

    public function actionView($id)
    {
        $d = 1;
        \Yii::$app->session['__crudReturnUrl'] = Url::previous();
        Url::remember();
        Tabs::rememberActiveState();

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        throw new NotFoundHttpException();
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        throw new NotFoundHttpException();
    }

    protected function findModel($id)
    {
        if (($model = MasterPresence::findOne($id)) !== null) {
            return $model;
        } else {
            throw new HttpException(404, 'The requested page does not exist.');
        }
    }
}
