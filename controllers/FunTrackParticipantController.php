<?php

namespace app\controllers;

use app\components\Constant;
use app\components\WA;
use app\models\FunTrackParticipant;
use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\QrCode;
use Yii;

/**
 * This is the class for controller "FunTrackParticipantController".
 */
class FunTrackParticipantController extends \app\controllers\base\FunTrackParticipantController
{

    public function actionConfirmation()
    {
        $user_id = Yii::$app->user->id;
        $id = $_POST['FunTrackParticipant']['id'];

        $model = FunTrackParticipant::findOne(['id' => $id]);

        if ($model == []) {
            Yii::$app->session->setFlash("error", "Model tidak ditemukan");
            return $this->redirect(['view', 'id' => $id]);
        }

        if ($model->confirmed_by != null) {
            Yii::$app->session->setFlash("error", "Telah dikonfirmasi sebelumnya");
            return $this->redirect(['view', 'id' => $id]);
        }

        $model->confirmed_at = date("Y-m-d H:i:s");
        $model->confirmed_by = $user_id;
        $model->qr_code = Yii::$app->security->generateRandomString(50);
        $model->is_confirmed = 1;
        try {
            $qrCode = new QrCode("{$model->qr_code}");

            $qrCode->setWriterByName('png');
            $qrCode->setEncoding('UTF-8');
            $qrCode->setErrorCorrectionLevel(ErrorCorrectionLevel::HIGH());
            $qrCode->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0]);
            $qrCode->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0]);
            $qrCode->setLabel('Scan the code', 16);
            $qrCode->setLogoPath(Yii::getAlias("@webroot/uploads/logo/bt19-white.png"));
            $qrCode->setLogoWidth(80);
            $qrCode->setValidateResult(false);

            $img = $qrCode->writeString();
            $imgWa = 'data:image/png;base64,' . base64_encode($img);
            $qrCode->writeFile(Yii::getAlias("@webroot/uploads/fun-track/qr-code") . "/{$model->id}{$model->name}.png");
            $imgEmail = Yii::$app->request->baseUrl . "/uploads/fun-track/qr-code/{$model->id}{$model->name}.png";

            $event_name = $model->funTrackPack->funTrack->name;
            $participant_name = $model->user->name;
            $year = date('Y');

            $contentWa = ($model->funTrack->is_action == "track")
            ? "Halo {$participant_name},\n\nSelamat Pendaftaran anda pada The Next Trip SS ({$event_name}) {$year} berhasil dikonfirmasi oleh admin.\n\nGunakan Qr Code terlampirkan untuk mengambil starter kit.\nBerikut adalah No. Registrasi anda : {$model->registration_number}"
            : "Halo {$participant_name},\n\nSelamat Pendaftaran anda pada The Next Trip SS ({$event_name}) {$year} berhasil dikonfirmasi oleh admin.";
            $contentEmail = "
            <img src='$imgEmail' alt='qrcode'>
            <br>
            Halo {$participant_name},
            <br>
            Selamat Pendaftaran anda pada event Next Trip SS ({$event_name}) {$year} berhasil di konfirmasi oleh admin.
            <br>
            Gunakan Qr Code terlampiran untuk mengambil starter kit.
            ";

            $no_hp = Constant::purifyPhone($model->user->no_hp);
            WA::send($no_hp, $contentWa, $imgWa);
            $caption_pdf = "Berikut adalah petunjuk teknis atau SOP untuk kegiatan THE NEXT TRIP SS. \nSOP ini akan menjelaskan bagaimana teknis kegiatan dan juga pembagian waktu keberangkatan.\n\nDiwajibkan untuk dibaca SOP terlebih dahulu.";
            WA::send($no_hp, $caption_pdf,
                "https://pagar.batutracking19.id/web/uploads/fun-track/sop.pdf", "SOP THE NEXT TRIP SS.pdf");
            WA::send($no_hp, $caption_pdf);
            $model->save();
            Yii::$app->session->setFlash("success", "berhasil dikonfirmasi");
        } catch (\Exception $e) {
            Yii::$app->session->setFlash("error", "Telah terjadi kesalahan" . $e);
        }

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionConfirmationFile()
    {
        $user_id = Yii::$app->user->id;
        $id = $_POST['FunTrackParticipant']['id'];
        $transaction = Yii::$app->db->beginTransaction();

        $model = FunTrackParticipant::findOne(['id' => $id]);
        if ($model->funTrack->is_action != "paint" && $model->is_confirmed == 1) {
            Yii::$app->session->setFlash("error", "Data tidak ditemukan");
            return $this->redirect(Yii::$app->request->referrer);
        }
        $model->is_confirmed++;
        try {
            $model->save();
            $contentWa = "Halo {$model->user->name},\n\Dokumen anda untuk event Next Trip SS ({$model->funTrack->name}) " . date("Y") . " telah diterima oleh pihak Among Tani Foundation";
            $no_hp = Constant::purifyPhone($model->user->no_hp);
            if ($no_hp) {
                WA::send($no_hp, $contentWa);
            }
            $transaction->commit();
            return $this->redirect(Yii::$app->request->referrer);
        } catch (\Throwable $th) {
            Yii::$app->session->setFlash("error", "Telah Terjadi kesalahan");
            $transaction->rollBack();
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    public function actionMajuBabakFinal($id)
    {
        $model = FunTrackParticipant::findOne(["id" => $id]);
        if ($model->funTrack->is_action == "track") {
            Yii::$app->session->setFlash("error", "Khusus untuk lomba");
            return $this->redirect(Yii::$app->request->referrer);
        }

        $model->qr_code = Yii::$app->security->generateRandomString(50);
        try {
            $qrCode = new QrCode("{$model->qr_code}");

            $qrCode->setWriterByName('png');
            $qrCode->setEncoding('UTF-8');
            $qrCode->setErrorCorrectionLevel(ErrorCorrectionLevel::HIGH());
            $qrCode->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0]);
            $qrCode->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0]);
            $qrCode->setLabel('Scan the code', 16);
            $qrCode->setLogoPath(Yii::getAlias("@webroot/uploads/logo/bt19-white.png"));
            $qrCode->setLogoWidth(80);
            $qrCode->setValidateResult(false);

            $img = $qrCode->writeString();
            $imgWa = 'data:image/png;base64,' . base64_encode($img);
            $qrCode->writeFile(Yii::getAlias("@webroot/uploads/fun-track/qr-code") . "/{$model->id}{$model->name}.png");

            $contentWa = "Halo para Finalis Lomba Melukis dan Pidato Bahasa Inggris - Bahasa Jawa !\nUntuk menghindari adanya penumpukan masa di Area Lomba serta tetap menerapkan protokol kesehatan, khususnya Physical Distancing, maka berikut Barcode yang dapat anda gunakan untuk masuk di Area Lomba.\n(Tunjukkan barcode pada petugas Batu Tracking 19 untuk discan)\n\nJangan lupa pakai masker, cuci tangan/gunakan hand sanitizer, dan jaga jarak.\n\nStay safe, stay healthy.\nSelamat berjuang!\n\nUntuk ketentuan Lomba dapat dilihat pada link di bawah ini:\nhttp://gg.gg/KETENTUAN-LOMBA";
            $no_hp = Constant::purifyPhone($model->user->no_hp);
            WA::send($no_hp, $contentWa, $imgWa);

            $model->save();
            Yii::$app->session->setFlash("success", "QR code berhasil dikirim");
            return $this->redirect(Yii::$app->request->referrer);
        } catch (\Throwable $th) {
            Yii::$app->session->setFlash("error", "Telah Terjadi kesalahan");
            return $this->redirect(Yii::$app->request->referrer);
        }
    }
}
