<?php

namespace app\controllers\api;

/**
* This is the class for REST controller "TimesController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class TimesController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\Times';
}
