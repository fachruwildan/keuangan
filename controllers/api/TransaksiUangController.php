<?php

namespace app\controllers\api;

/**
* This is the class for REST controller "TransaksiUangController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class TransaksiUangController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\TransaksiUang';
}
