<?php

namespace app\controllers\api;

/**
* This is the class for REST controller "PerusahaanController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class PerusahaanController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\Perusahaan';
}
