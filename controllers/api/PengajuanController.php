<?php

namespace app\controllers\api;

/**
* This is the class for REST controller "PengajuanController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class PengajuanController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\Pengajuan';
}
