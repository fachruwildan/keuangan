<?php

namespace app\controllers\api;

/**
* This is the class for REST controller "LogActivityController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class LogActivityController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\LogActivity';
}
