<?php

namespace app\controllers\api;

use app\components\Log;
use app\components\Tanggal;
use app\models\MasterPresence;
use app\models\Presence;
use app\models\User;
use Yii;
use yii\web\Response;

/**
 * This is the class for REST controller "PresenceController".
 */

class PresenceController extends \yii\rest\ActiveController
{
    public $modelClass = 'app\models\Presence';
    public function actions()
    {
        $parent = parent::actions();
        unset($parent['index']);
        unset($parent['create']);
        unset($parent['update']);
        unset($parent['view']);
        unset($parent['delete']);
        return $parent;
    }

    public function actionCreate()
    {
        // status
        // 0 : gagal
        // 1 : berhasil
        // 2 : belum waktu mulai
        // 3 : telat
        // 4 : suhu terlalu tinggi
        // 5 : duplikasi data

        Yii::$app->response->format = Response::FORMAT_JSON;

        $masterPresence = MasterPresence::findOne(["id" => 1]);
        $date = date("Y-m-d");
        $time = date("H:i:s");
        // $time = "17:45:12";
        $now = "{$date} {$time}";
        $time_in_range1 = "{$date} {$masterPresence->time_in_min}";
        $time_in_range2 = "{$date} {$masterPresence->time_in_max}";
        $time_out_range1 = "{$date} {$masterPresence->time_out_min}";
        // $time_out_range2 = "{$date} {$masterPresence->time_out_max}";
        $link_photo = $this->uploadImage();
        if ($link_photo->success == false) {
            return [
                "status" => 0,
                "message" => "Foto gagal diupload",
            ];
        }
        $additional = [
            "photo" => $link_photo->data,
            "temperature" => $_POST['temperature'],
            "is_success" => false,
        ];
        $check_time = Tanggal::isInsideRange($now, $time_out_range1,
            date("Y-m-d H:i:s", strtotime("{$time_in_range1} +1 day"))
        );

        if ($check_time) {
            $model = Presence::find()->where([
                'and',
                [
                    'and',
                    ['date' => $date],
                    ['is', 'time_out', null],
                ],
                [
                    'qr_code' => $_POST['qr_code'],
                ],
            ])->one();
            if ($model == []) {
                $message = "Data tidak ditemukan";
                Log::presence($model, $additional, $message);
                return [
                    "status" => 0,
                    "message" => $message,
                ];
            }
        } else {
            $model = new Presence();
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            if ($model->load(Yii::$app->request->bodyParams, '')) {
                $user = $this->getQrCode($model->qr_code);
                if ($user->success == false) {
                    $transaction->rollBack();
                    $message = "User tidak ditemukan";
                    Log::presence($model, $additional, $message);
                    return [
                        "status" => 0,
                        "message" => $message,
                    ];
                }
                $model->user_id = $user->data->id;
                $model->name = $user->data->name;

                $presence = Presence::find()->where([
                    'and',
                    [
                        'date' => $date,
                    ],
                    [
                        'user_id' => $user->data->id,
                        'qr_code' => $user->data->qr_code,
                    ],
                ])->one();

                if (strtotime($now) < strtotime($time_in_range1)) {
                    $transaction->rollBack();
                    $message = "Belum waktu presensi";
                    Log::presence($model, $additional, $message);
                    return [
                        "status" => 2,
                        "message" => $message,
                    ];
                } else if (Tanggal::isInsideRange($now, $time_in_range1, $time_in_range2)) {
                    $opts = [
                        "date" => $date,
                        "time" => $time,
                        "now" => $now,
                        "time_in_range2" => $time_in_range2,
                        "time_out_range1" => $time_out_range1,
                    ];
                    $return = $this->in($model, $presence, $link_photo, $opts, false);
                    if ($return != null) {
                        $transaction->rollBack();
                        Log::presence($model, $additional, $return['message']);
                        return $return;
                    }
                } else if (Tanggal::isInsideRange($now, $time_in_range2, $time_out_range1)) {
                    $opts = [
                        "date" => $date,
                        "time" => $time,
                        "now" => $now,
                        "time_in_range2" => $time_in_range2,
                        "time_out_range1" => $time_out_range1,
                    ];
                    $return = $this->in($model, $presence, $link_photo, $opts, true);
                    if ($return != null) {
                        $transaction->rollBack();
                        Log::presence($model, $additional, $return['message']);
                        return $return;
                    }
                } else if (
                    Tanggal::isInsideRange($now, $time_out_range1,
                        date("Y-m-d H:i:s", strtotime("{$time_in_range1} +1 day"))
                    )
                ) {
                    if ($presence == []) {
                        $message = "Data tidak ditemukan";
                        $transaction->rollBack();
                        Log::presence($model, $additional, $message);
                        return [
                            "status" => 0,
                            "message" => $message,
                        ];
                    }

                    $model->photo_out = $link_photo->data;

                    $temperature = 0;
                    $err = $this->checkTemperature($temperature, $_POST['temperature']);
                    if ($err) {
                        return $err;
                    }
                    $model->temperature_in = $temperature;
                    $model->time_out = $time;
                } else {
                    $transaction->rollBack();
                    $message = "Tidak masuk kedalam waktu yang telah ditentukan";
                    Log::presence($model, $additional, $message);
                    return [
                        "status" => 2,
                        "message" => $message,
                    ];
                }

                // sementara hanya untuk pegawai - tamu belum dibuat
                $model->is_guest = 0; // bukan tamu
                $model->date = $date;
                if ($model->validate()) {
                    $model->save();
                    $transaction->commit();
                    $message = "berhasil absen";
                    $additional["is_success"] = true;
                    Log::presence($model, $additional, $message);
                    return [
                        "status" => 1,
                        "message" => $message,
                    ];
                } else {
                    $transaction->rollBack();
                    $message = "Validasi gagal.";
                    Log::presence($model, $additional, $message);
                    return [
                        "status" => 0,
                        "message" => $message,
                    ];
                }
            }
        } catch (\Throwable $th) {
            $transaction->rollBack();
            $message = "Terjadi kesalahan.";
            Log::presence($model, $additional, $message);
            return [
                "status" => 0,
                "message" => $message,
            ];
        }
    }

    public function checkTemperature(&$data, $temperature)
    {
        if ($temperature > 37.3) {
            $message = "Suhu diatas normal";
            return [
                "status" => 4,
                "message" => $message,
            ];
        }
        $data = $temperature;
    }

    public function in(&$model, $presence, $link_photo, $options, $is_late)
    {
        $model->photo_in = $link_photo->data;
        $temperature = 0;
        $err = $this->checkTemperature($temperature, $_POST['temperature']);
        if ($err) {
            return $err;
        }
        $model->temperature_in = $temperature;

        $presenceBlankTimeout = Presence::find([
            'and',
            ['<', 'date', $options['date']],
            [
                'and',
                ['!=', 'time_in', null],
                ['=', 'time_out', null],
            ],
        ])->all();

        foreach ($presenceBlankTimeout as $item) {
            $item->time_out = "-";
            $item->save();
        }

        if ($presence) {
            $message = "Duplikasi data.";
            return [
                "status" => 5,
                "message" => $message,
            ];
        }

        $model->time_in = $options['time'];
        $model->is_late = ($is_late == false) ? 0 : 1;
        $model->time_late = ($is_late == false) ? 0 : (strtotime($options['now']) - strtotime($options['time_in_range2']));
        return null;
    }

    public function getQrCode($qr_code)
    {
        if ($qr_code == "") {
            return [
                "success" => false,
                "message" => "Qr cant be blank",
            ];
        }
        $user = User::findOne(['qr_code' => $qr_code]);
        if ($user) {
            return (object) [
                "success" => true,
                "data" => $user,
            ];
        }
        return (object) [
            "success" => false,
            "data" => [],
        ];
    }

    public function uploadImage()
    {
        $photo = $_POST['photo'];

        $date = date("Y-m-d");
        $filename = Yii::$app->security->generateRandomString(9) . ".png";

        $url_path = Yii::getAlias("@web/uploads/presence/$date/");
        $real_path = Yii::getAlias("@webroot/uploads/presence/$date/");

        if (file_exists($real_path) == false) {
            mkdir($real_path, 0777, true);
        }

        $binary_image = base64_decode(str_replace("data:image/png;base64,", "", $photo));
        $allow = [
            "\xFF\xD8\xFF",
            "GIF",
            "\x89\x50\x4e\x47\x0d\x0a\x1a\x0a",
            "BM",
            "8BPS",
            "FWS",
        ];

        $flag = 0;

        foreach ($allow as $item) {
            if (substr($binary_image, 0, strlen($item)) == $item) {
                $flag = 1;
            }
        }

        if ($flag == 1) {
            file_put_contents("{$real_path}{$filename}", $binary_image);
            return (object) [
                "success" => true,
                "data" => "{$url_path}{$filename}",
            ];
        } else {
            return (object) [
                "success" => false,
                "data" => "",
            ];
        }
    }
}
