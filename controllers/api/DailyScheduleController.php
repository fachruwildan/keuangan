<?php

namespace app\controllers\api;

/**
* This is the class for REST controller "DailyScheduleController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class DailyScheduleController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\DailySchedule';
}
