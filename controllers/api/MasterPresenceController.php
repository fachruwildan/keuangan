<?php

namespace app\controllers\api;

/**
 * This is the class for REST controller "MasterPresenceController".
 */

class MasterPresenceController extends \yii\rest\ActiveController
{
    public $modelClass = 'app\models\MasterPresence';
    public function actions()
    {
        $parent = parent::actions();
        unset($parent['index']);
        unset($parent['create']);
        unset($parent['update']);
        unset($parent['view']);
        unset($parent['delete']);
        return $parent;
    }
}
