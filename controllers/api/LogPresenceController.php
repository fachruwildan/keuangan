<?php

namespace app\controllers\api;

/**
* This is the class for REST controller "LogPresenceController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class LogPresenceController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\LogPresence';
}
