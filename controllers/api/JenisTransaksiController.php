<?php

namespace app\controllers\api;

/**
* This is the class for REST controller "JenisTransaksiController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class JenisTransaksiController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\JenisTransaksi';
}
