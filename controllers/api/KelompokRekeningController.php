<?php

namespace app\controllers\api;

/**
* This is the class for REST controller "KelompokRekeningController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class KelompokRekeningController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\KelompokRekening';
}
