<?php

namespace app\controllers\api;

/**
* This is the class for REST controller "DetailOrderController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class DetailOrderController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\DetailOrder';
}
