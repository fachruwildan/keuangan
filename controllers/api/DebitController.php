<?php

namespace app\controllers\api;

/**
* This is the class for REST controller "DebitController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class DebitController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\Debit';
}
