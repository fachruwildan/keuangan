<?php

namespace app\controllers\api;

/**
* This is the class for REST controller "KreditController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class KreditController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\Kredit';
}
