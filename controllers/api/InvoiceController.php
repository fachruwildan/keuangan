<?php

namespace app\controllers\api;

/**
* This is the class for REST controller "InvoiceController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class InvoiceController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\Invoice';
}
