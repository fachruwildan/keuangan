<?php

namespace app\controllers\api;

/**
* This is the class for REST controller "HutangPiutangController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class HutangPiutangController extends \yii\rest\ActiveController
{
public $modelClass = 'app\models\HutangPiutang';
}
